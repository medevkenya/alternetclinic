<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@signin');
Route::get('/signin', 'HomeController@signin');
Route::post('dosignin', 'HomeController@dosignin');
Route::get('login', [ 'as' => 'login', 'uses' => 'HomeController@signin']);
Route::get('/register', 'HomeController@register');
Route::post('doRegister', 'HomeController@doRegister');
Route::get('/logout', 'HomeController@logout');
Route::get('/activateAccount', 'HomeController@activateAccount');

Route::get('ResetPasswordLink/{PasswordConfirmation}/{email}', 'ForgotpasswordController@resetPasswordLink');
Route::get('forgotPassword', 'ForgotpasswordController@forgotPassword');
Route::post('resetPassword', 'ForgotpasswordController@resetPassword');
Route::post('DoResetMYPassword', 'ForgotpasswordController@doResetPassword');

Route::get('resendRecoveryEmail/{email}', 'ForgotpasswordController@resendRecoveryEmail');

Route::get('/terms', 'SiteController@terms');
Route::get('/privacy', 'SiteController@privacy');
Route::get('/help', 'SiteController@help');

Route::group(['middleware' => 'auth'], function () {

Route::get('dashboard', 'AccountController@dashboard');

Route::get('profile', 'AccountController@profile');
Route::post('updateProfile', 'AccountController@updateProfile');
Route::get('changepassword', 'AccountController@changepassword');
Route::post('doChangePass', 'AccountController@doChangePass');
Route::get('changePic', 'AccountController@changePic');
Route::post('UpdatePic', 'AccountController@UpdatePic')->name('UpdatePic');

Route::get('patients', 'PatientsController@patients');
Route::get('addpatient', 'PatientsController@addpatient');
Route::post('doaddpatient', 'PatientsController@doaddpatient');
Route::post('deletepatient', 'PatientsController@deletepatient');
Route::get('editPatient/{id}', 'PatientsController@editPatient');
Route::post('doeditpatient', 'PatientsController@doeditpatient');
Route::get('viewPatient/{id}', 'PatientsController@viewPatient');

Route::post('printCard', 'PatientsController@printCard');

Route::post('updateCompanyPhoto', 'SettingsController@updateCompanyPhoto')->name('updateCompanyPhoto');
Route::post('updateCompanyDetails', 'SettingsController@updateCompanyDetails');

Route::get('activities', 'AccountController@activities');

Route::get('roles', 'RolesController@roles');
Route::post('addrole', 'RolesController@addrole');
Route::post('deleterole', 'RolesController@deleterole');
Route::post('editrole', 'RolesController@editrole');

Route::get('appointments', 'AppointmentsController@appointments');
Route::post('addappointment', 'AppointmentsController@addappointment');
Route::post('deleteappointment', 'AppointmentsController@deleteappointment');
Route::post('editappointment', 'AppointmentsController@editappointment');
Route::get('viewAppointment/{id}', 'AppointmentsController@viewAppointment');
Route::get('appointmentsReport', 'AppointmentsController@appointmentsReport');
Route::post('postappointmentsreport', 'AppointmentsController@postappointmentsreport');
Route::get('postappointmentsreport', 'AppointmentsController@appointmentsReport');

Route::post('edittreatment', 'AppointmentsController@edittreatment');
Route::post('editprescription', 'AppointmentsController@editprescription');
Route::post('editdiagnosis', 'AppointmentsController@editdiagnosis');
Route::post('editlabtest', 'AppointmentsController@editlabtest');
Route::post('editmedicines', 'AppointmentsController@editmedicines');

Route::get('doctors', 'DoctorsController@doctors');
Route::get('viewDoctor/{id}', 'DoctorsController@viewDoctor');

Route::get('pharmacy', 'PharmacyController@pharmacy');
Route::get('pharmacyReport', 'PharmacyController@pharmacyReport');
Route::post('postpharmacyReport', 'PharmacyController@postpharmacyReport');

Route::get('fees', 'FeesController@fees');
Route::post('addfee', 'FeesController@addfee');
Route::post('deletefee', 'FeesController@deletefee');
Route::post('editfee', 'FeesController@editfee');

Route::get('colors', 'ColorsController@colors');
Route::post('addcolor', 'ColorsController@addcolor');
Route::post('deletecolor', 'ColorsController@deletecolor');
Route::post('editcolor', 'ColorsController@editcolor');

Route::get('sizes', 'SizesController@sizes');
Route::post('addsize', 'SizesController@addsize');
Route::post('deletesize', 'SizesController@deletesize');
Route::post('editsize', 'SizesController@editsize');

Route::get('brands', 'BrandsController@brands');
Route::post('addbrand', 'BrandsController@addbrand');
Route::post('deletebrand', 'BrandsController@deletebrand');
Route::post('editbrand', 'BrandsController@editbrand');

Route::get('batches', 'BatchesController@batches');
Route::post('addbatch', 'BatchesController@addbatch');
Route::post('deletebatch', 'BatchesController@deletebatch');
Route::post('editbatch', 'BatchesController@editbatch');

Route::get('getBrands', 'BrandsController@getBrands')->name('getBrands');
Route::post('addproduct', 'ProductsController@addproduct')->name('addproduct');
Route::post('editproduct', 'ProductsController@editproduct')->name('editproduct');

Route::get('getProducts', 'ProductsController@getProducts')->name('getProducts');

Route::get('products', 'ProductsController@products');
Route::post('deleteproduct', 'ProductsController@deleteproduct');

Route::get('salesReport', 'OrdersController@salesReport');
Route::post('postSalesReport', 'OrdersController@postSalesReport');
Route::get('postSalesReport', 'OrdersController@salesReport');
Route::post('deleteorder', 'OrdersController@deleteorder');

Route::get('categories', 'CategoriesController@categories');
Route::post('addcategory', 'CategoriesController@addcategory');
Route::post('deletecategory', 'CategoriesController@deletecategory');
Route::post('editcategory', 'CategoriesController@editcategory');

Route::post('editcost', 'FeesController@editcost');

Route::get('users', 'UsersController@users');
Route::post('adduser', 'UsersController@adduser');
Route::post('deleteuser', 'UsersController@deleteuser');
Route::post('edituser', 'UsersController@edituser');

Route::get('receiveCash', 'CashierController@receiveCash');
Route::get('cashier', 'CashierController@cashier');
Route::get('cashierReport', 'CashierController@cashierReport');
Route::post('postcashierReport', 'CashierController@postcashierReport');
Route::get('postcashierReport', 'CashierController@cashierReport');

Route::get('payments', 'PaymentsController@payments');
Route::post('paymentsReport', 'PaymentsController@paymentsReport');
Route::get('paymentsReport', 'PaymentsController@payments');

Route::get('expensetypes', 'ExpensetypesController@expensetypes');
Route::post('addexpensetype', 'ExpensetypesController@addexpensetype');
Route::post('deleteexpensetype', 'ExpensetypesController@deleteexpensetype');
Route::post('editexpensetype', 'ExpensetypesController@editexpensetype');

Route::get('referralhospitals', 'ReferralhospitalsController@referralhospitals');
Route::post('addreferralhospital', 'ReferralhospitalsController@addreferralhospital');
Route::post('deletereferralhospital', 'ReferralhospitalsController@deletereferralhospital');
Route::post('editreferralhospital', 'ReferralhospitalsController@editreferralhospital');

Route::get('wards', 'WardsController@wards');
Route::post('addward', 'WardsController@addward');
Route::post('deleteward', 'WardsController@deleteward');
Route::post('editward', 'WardsController@editward');

Route::get('services', 'ServicesController@services');
Route::post('addservice', 'ServicesController@addservice');
Route::post('deleteservice', 'ServicesController@deleteservice');
Route::post('editservice', 'ServicesController@editservice');

Route::get('expenses', 'ExpensesController@expenses');
Route::post('addexpense', 'ExpensesController@addexpense');
Route::post('deleteexpense', 'ExpensesController@deleteexpense');
Route::post('editexpense', 'ExpensesController@editexpense');
Route::post('postexpensesReport', 'ExpensesController@postexpensesReport');
Route::get('postexpensesReport', 'ExpensesController@expenses');

Route::get('permissions', 'PermissionsController@permissions');
Route::get('updatepermission', 'PermissionsController@updatepermission');

Route::post('admitpatient', 'AppointmentsController@admitpatient');
Route::post('dischargepatient', 'AppointmentsController@dischargepatient');
Route::post('referpatient', 'AppointmentsController@referpatient');

Route::post('flutterwaveverify', 'FlutterwaveController@flutterwaveverify')->name('flutterwaveverify');
Route::post('flutterwaveinitiate', 'FlutterwaveController@flutterwaveinitiate')->name('flutterwaveinitiate');

Route::get('notifications', 'NotificationsController@notifications');
Route::post('addnotification', 'NotificationsController@addnotification');

Route::get('admissions', 'AdmissionsController@admissions');
Route::post('editadmission', 'AdmissionsController@editadmission');
Route::post('admissionsReport', 'AdmissionsController@admissionsReport');
Route::get('admissionsReport', 'AdmissionsController@admissions');

Route::get('referrals', 'ReferralsController@referrals');
Route::post('editreferral', 'ReferralsController@editreferral');
Route::post('referralsReport', 'ReferralsController@referralsReport');
Route::get('referralsReport', 'ReferralsController@referrals');

Route::get('sales', 'SalesController@sales');
Route::post('savecart', 'SalesController@savecart')->name('savecart');
Route::get('getCart', 'SalesController@getCart')->name('getCart');
Route::post('increaseCart', 'SalesController@increaseCart')->name('increaseCart');
Route::post('decreaseCart', 'SalesController@decreaseCart')->name('decreaseCart');
Route::get('completePrint', 'SalesController@completePrint')->name('completePrint');
Route::post('search', 'SalesController@search');

});

//Cron
Route::get('cronsms', 'CronsController@cronsms');
