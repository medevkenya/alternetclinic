<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Hash;
use\App\Activities;

class Patients extends Model
{
    protected $table = 'users';

    public static function latestPatients()
    {
      return User::select('users.created_at','users.firstName','users.lastName','users.gender','users.mobileNo','users.location','users.profilePic','appointments.appointmentNo')
      ->leftJoin('appointments','users.id','=','appointments.patientId')
      ->where('users.roleId',0)
      ->where('users.adminId',Auth::user()->adminId)
      ->where('users.isDeleted',0)
      ->orderBy('users.id','DESC')
      ->take(6)
      ->get();
    }

    public static function storeone($patientNo,$firstName,$middleName,$lastName,$mobileNo,$email,$password,$roleId,$location,$gender,$dob)
    {
        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Patients;
        $model->firstName = $firstName;
        $model->lastName = $lastName;
        $model->middleName = $middleName;
        $model->password = $password;
        $model->email = $email;
        $model->mobileNo = $mobileNo;
        $model->dob = $dob;
        $model->gender = $gender;
        $model->location = $location;
        $model->roleId = $roleId;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model) {
            Activities::saveLog("Added patient [".$firstName." ".$lastName." - ".$mobileNo."]");
            return $model->id;
        }
        return false;
    }

    public static function updateone($id,$firstName,$middleName,$lastName,$mobileNo,$email,$location,$gender,$dob)
    {
        $model = Patients::find($id);
        $model->firstName = $firstName;
        $model->lastName = $lastName;
        $model->middleName = $middleName;
        $model->email = $email;
        $model->mobileNo = $mobileNo;
        $model->dob = $dob;
        $model->gender = $gender;
        $model->location = $location;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited patient [".$firstName." ".$lastName." - ".$mobileNo."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Patients::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted user [".$id."]");
            return true;
        }
        return false;
    }

}
