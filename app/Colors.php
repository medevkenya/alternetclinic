<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Colors extends Model
{
    protected $table = 'colors';

    public static function getAll() {
      return $list = Colors::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($colorName)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Colors;
        $model->colorName = $colorName;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new color [".$colorName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $colorName)
    {
        $model = Colors::find($id);
        $model->colorName = $colorName;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited color [".$colorName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Colors::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted color [".$id."]");
            return true;
        }
        return false;
    }

}
