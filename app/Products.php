<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Products extends Model
{
    protected $table = 'products';

    public static function getAll() {
      return $list = Products::select('products.*','categories.categoryName','brands.brandName','colors.colorName','sizes.sizeName')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->leftJoin('brands','products.brandId','=','brands.id')
      ->leftJoin('colors','products.colorId','=','colors.id')
      ->leftJoin('sizes','products.sizeId','=','sizes.id')
      ->where('products.adminId',Auth::user()->adminId)->where('products.isDeleted',0)->orderBy('products.id','DESC')->get();
    }

    public static function storeone($productName,$cost,$categoryId,$brandId,$sizeId,$colorId,$description,$photoUrl)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Products;
        $model->productName = $productName;
        $model->cost = $cost;
        $model->categoryId = $categoryId;
        $model->brandId = $brandId;
        $model->colorId = $colorId;
        $model->sizeId = $sizeId;
        $model->description = $description;
        $model->photoUrl = $photoUrl;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new product [".$productName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $productName,$cost,$categoryId,$brandId,$sizeId,$colorId,$description,$photoUrl)
    {
        $model = Products::find($id);
        $model->productName = $productName;
        $model->cost = $cost;
        $model->categoryId = $categoryId;
        $model->brandId = $brandId;
        $model->colorId = $colorId;
        $model->sizeId = $sizeId;
        $model->description = $description;
        $model->photoUrl = $photoUrl;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited product [".$productName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Products::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted product [".$id."]");
            return true;
        }
        return false;
    }

}
