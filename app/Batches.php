<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Cart;
use App\Notifications;
use Log;

class Batches extends Model
{
    protected $table = 'batches';

    public static function getAll() {
      return $list = Batches::select('batches.*','products.productName','categories.categoryName')
      ->leftJoin('products','batches.productId','=','products.id')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->where('batches.adminId',Auth::user()->adminId)
      ->where('batches.status','In Stock')
      ->where('batches.isDeleted',0)
      ->orderBy('batches.id','DESC')->get();
    }

    public static function getProductQuantity($productId) {
      $quantity = Batches::where('productId',$productId)->where('status','In Stock')->where('isDeleted',0)->sum('quantity');
      $sold = Batches::where('productId',$productId)->where('status','In Stock')->where('isDeleted',0)->sum('sold');
      $bal = $quantity - $sold;
      if($bal > 0) {
        return $bal;
      }
      else {
        return 0;
      }
    }

    public static function storeone($batchNo,$productId,$quantity,$expiryDate)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Batches;
        $model->batchNo = $batchNo;
        $model->productId = $productId;
        $model->expiryDate = $expiryDate;
        $model->quantity = $quantity;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new batch [".$batchNo."]");
          return true;
        }

        return false;

    }

    public static function updateone($id,$batchNo,$productId,$quantity,$expiryDate)
    {
        $model = Batches::find($id);
        $model->batchNo = $batchNo;
        $model->productId = $productId;
        $model->expiryDate = $expiryDate;
        $model->quantity = $quantity;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited batch [".$batchNo."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Batches::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted batch [".$id."]");
            return true;
        }
        return false;
    }

    public static function updateProduct($orderId)
    {

      $list = Cart::where('orderId',$orderId)->where('isDeleted',0)->get();
      foreach ($list as $key) {

        $check = Batches::where('productId',$key->productId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','ASC')->first();
        if($check)
        {
          log::info("batch id--".$check->id);
          $checkdetails = Batches::where('id',$check->id)->first();
            $sold = $checkdetails->sold + $key->quantity;
            log::info("cart quantity--".$key->quantity);
            log::info("batch new sold--".$sold);
            Batches::where('id',$checkdetails->id)->update(['sold'=>$sold]);
            if($sold >= $checkdetails->quantity)
            {
                Batches::where('id',$checkdetails->id)->update(['status'=>'Out of Stock']);
                $message = $key->itemName." has run out of stock";
                Notifications::storeone($message);
            }
        }

      }

    }

}
