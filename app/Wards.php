<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Wards extends Model
{
    protected $table = 'wards';

    public static function getAll() {
      return $list = Wards::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($wardName,$spacesavailable,$spacesoccupied)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Wards;
        $model->wardName = $wardName;
        $model->spacesavailable = $spacesavailable;
        $model->spacesoccupied = $spacesoccupied;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new ward [".$wardName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $wardName,$spacesavailable,$spacesoccupied)
    {
        $model = Wards::find($id);
        $model->wardName = $wardName;
        $model->spacesavailable = $spacesavailable;
        $model->spacesoccupied = $spacesoccupied;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited ward [".$wardName."]");
            return true;
        }
        return false;
    }

    public static function occupy($id) {
      $check = Wards::where('id',$id)->first();
      $spacesoccupied = $check->spacesoccupied + 1;
      $spacesavailable = $check->spacesavailable - 1;
      $model = Wards::find($id);
      $model->spacesavailable = $spacesavailable;
      $model->spacesoccupied = $spacesoccupied;
      if($spacesavailable == 0) {
        $model->status = "Not Available";
      }
      $model->save();
      if ($model) {
          return true;
      }
      return false;
    }

    public static function discharge($id) {
      $check = Wards::where('id',$id)->first();
      $spacesoccupied = $check->spacesoccupied - 1;
      $spacesavailable = $check->spacesavailable + 1;
      $model = Wards::find($id);
      $model->spacesavailable = $spacesavailable;
      $model->spacesoccupied = $spacesoccupied;
      $model->status = "Available";
      $model->save();
      if ($model) {
          return true;
      }
      return false;
    }

    public static function deleteone($id)
    {
        $model = Wards::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted ward [".$id."]");
            return true;
        }
        return false;
    }

    }
