<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Treatments;
use App\Services;
use App\Costs;
use App\Fees;

class Appointments extends Model
{
    protected $table = 'appointments';

    public static function storeone($patientId,$doctorId,$serviceId,$appointmentDate) {
      $adminId	= Auth::user()->adminId;
      $created_by	= Auth::user()->id;

      $count = Appointments::where('adminId',$adminId)->where('isDeleted', 0)->whereRaw('Date(created_at) = CURDATE()')->count();
			//if($checklast) {
        $cc = $count + 1;
				$appointmentNo = $cc."APT/".date('Ymd');
			// }
			// else {
			// 	$appointmentNo = "1APT/".date('Ymd');
			// }

      $model = new Appointments;
      $model->patientId = $patientId;
      $model->doctorId = $doctorId;
      $model->serviceId = $serviceId;
      $model->appointmentNo = $appointmentNo;
      $model->appointmentDate = $appointmentDate;
      $model->adminId = $adminId;
      $model->created_by = $created_by;
      $model->save();
      if ($model)
      {
          Treatments::storeone($patientId,$model->id);
          Costs::storeone($patientId,$model->id,$serviceId);
          Activities::saveLog("Added appointment for patient [".$patientId.", appointment no. - ".$appointmentNo."]");
          return true;
      }
      return false;
    }

    public static function updateone($id, $doctorId,$serviceId,$appointmentDate,$status) {
      $model = Appointments::find($id);
      $model->doctorId = $doctorId;
      $model->serviceId = $serviceId;
      $model->appointmentDate = $appointmentDate;
      $model->status = $status;
      $model->save();
      if ($model) {
          Activities::saveLog("Edited appointment ".$id.", No. [".$model->appointmentNo."]");
          return true;
      }
      return false;
    }

    public static function deleteone($id)
    {
        $model = Appointments::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Costs::where('appointmentId',$id)->delete();
            Fees::where('appointmentId',$id)->delete();
            Activities::saveLog("Deleted appointment [".$model->appointmentNo."]");
        }
        return false;
    }

    public static function countForPatient($patientId) {
      return Appointments::where('patientId',$patientId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
    }

    public static function countForDoctor($patientId) {
      return Appointments::where('doctorId',$patientId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
    }

    public static function getForPatient($patientId)
    {
      return Appointments::select('appointments.*','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','appointments.doctorId','=','users.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('appointments.patientId',$patientId)
      ->where('appointments.adminId',Auth::user()->adminId)
      ->where('appointments.isDeleted',0)
      ->orderBy('appointments.id','DESC')
      ->get();
    }

    public static function getForCashier()
    {
      return Appointments::select('appointments.*','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','appointments.doctorId','=','users.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('appointments.status','Pending')
      ->where('appointments.adminId',Auth::user()->adminId)
      ->where('appointments.isDeleted',0)
      ->orderBy('appointments.id','DESC')
      ->get();
    }

    public static function getForDoctor($doctorId)
    {
      return Appointments::select('appointments.*','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','appointments.doctorId','=','users.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('appointments.doctorId',$doctorId)
      ->where('appointments.adminId',Auth::user()->adminId)
      ->where('appointments.isDeleted',0)
      ->orderBy('appointments.id','DESC')
      ->get();
    }

    public static function getForPharmacy()
    {
      return Appointments::select('appointments.*','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','appointments.doctorId','=','users.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('appointments.giveMedicine','Yes')
      ->where('appointments.adminId',Auth::user()->adminId)
      ->where('appointments.isDeleted',0)
      ->orderBy('appointments.id','DESC')
      ->get();
    }

    public static function getAll()
    {
      return Appointments::select('appointments.*','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','appointments.doctorId','=','users.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('appointments.adminId',Auth::user()->adminId)
      ->where('appointments.isDeleted',0)
      ->orderBy('appointments.id','DESC')
      ->get();
    }

}
