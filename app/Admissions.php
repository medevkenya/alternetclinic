<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Wards;

class Admissions extends Model
{
    protected $table = 'admissions';

    public static function getAll() {
      return Admissions::select('admissions.*','appointments.appointmentNo','users.firstName','users.lastName','services.name as serviceName','wards.wardName')
  					->leftJoin('users','admissions.patientId','=','users.id')
  					->leftJoin('appointments','admissions.appointmentId','=','appointments.id')
  					->leftJoin('services','appointments.serviceId','=','services.id')
  					->leftJoin('wards','admissions.wardId','=','wards.id')
  					->where('admissions.adminId',Auth::user()->adminId)
  					->whereRaw('Date(admissions.created_at) = CURDATE()')
  					->where('admissions.isDeleted',0)
  					->orderBy('admissions.id','DESC')
  					->get();
    }

    public static function getForPatient($patientId) {
      return Admissions::select('admissions.*','appointments.appointmentNo','users.firstName','users.lastName','services.name as serviceName','wards.wardName')
            ->leftJoin('users','admissions.patientId','=','users.id')
            ->leftJoin('appointments','admissions.appointmentId','=','appointments.id')
            ->leftJoin('services','appointments.serviceId','=','services.id')
            ->leftJoin('wards','admissions.wardId','=','wards.id')
            ->where('admissions.adminId',Auth::user()->adminId)
            ->where('admissions.patientId',$patientId)
            ->where('admissions.isDeleted',0)
            ->orderBy('admissions.id','DESC')
            ->get();
    }

    public static function getForAppointment($appointmentId) {
      return Admissions::select('admissions.*','appointments.appointmentNo','users.firstName','users.lastName','services.name as serviceName','wards.wardName')
            ->leftJoin('users','admissions.patientId','=','users.id')
            ->leftJoin('appointments','admissions.appointmentId','=','appointments.id')
            ->leftJoin('services','appointments.serviceId','=','services.id')
            ->leftJoin('wards','admissions.wardId','=','wards.id')
            ->where('admissions.adminId',Auth::user()->adminId)
            ->where('admissions.appointmentId',$appointmentId)
            ->where('admissions.isDeleted',0)
            ->orderBy('admissions.id','DESC')
            ->get();
    }

    public static function checkPatient($patientId,$appointmentId) {
      return Admissions::where('patientId',$patientId)->where('appointmentId',$appointmentId)->where('isDeleted',0)->first();
    }

    public static function editadmission($id,$bedNo,$admitReason) {
      $model = Admissions::find($id);
      $model->bedNo = $bedNo;
      $model->admitReason = $admitReason;
      $model->save();
      if ($model)
      {
          return true;
      }
      return false;
    }

    public static function admitpatient($patientId,$bedNo,$wardId,$admitReason,$appointmentId)
    {
      $adminId	= Auth::user()->adminId;
      $created_by	= Auth::user()->id;
      $model = new Admissions;
      $model->patientId = $patientId;
      $model->bedNo = $bedNo;
      $model->wardId = $wardId;
      $model->admitReason = $admitReason;
      $model->admissionDate = date('Y-m-d');
      $model->appointmentId = $appointmentId;
      $model->adminId = $adminId;
      $model->created_by = $created_by;
      $model->save();
      if ($model)
      {
        Activities::saveLog("Admitted new patient [".$patientId."], ward [".$wardId."]");
        return true;
      }
      return false;
    }

    public static function dischargepatient($patientId,$dischargeReason,$appointmentId)
    {
        $check = Admissions::where('patientId',$patientId)->where('appointmentId',$appointmentId)->first();
        $model = Admissions::find($check->id);
        $model->dischargeDate = date('Y-m-d');
        $model->dischargeReason = $dischargeReason;
        $model->save();
        if ($model)
        {
            Wards::discharge($check->id);
            Activities::saveLog("Discharged patient [".$patientId."]");
            return true;
        }
        return false;
    }

}
