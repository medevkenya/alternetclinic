<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Cart;
use Session;
use Log;
use App\Batches;

class Cart extends Model
{
    protected $table = 'cart';

    public static function getAll($orderId)
    {
      return Cart::where('orderId',$orderId)->where('adminId',Auth::user()->adminId)->where('completed',0)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function getCart()
    {
      return Cart::select('cart.id','cart.itemName','cart.cost','cart.totalCost','cart.quantity','cart.productId','products.photoUrl')
      ->leftJoin('products','cart.productId','=','products.id')
      ->where('cart.sessionId',Session::getId())
      ->where('cart.completed',0)
      ->where('cart.isDeleted',0)
      ->orderBy('cart.id','DESC')
      ->get();
    }

    public static function saveNew($productId)
    {

      $productdetails = Products::where('id',$productId)->first();

      if(Auth::user())
      {
        $userId = Auth::user()->id;
      }
      else
      {
        $userId = 0;
      }

        $checkcart = Cart::where('productId',$productId)->where('sessionId',Session::getId())->where('completed',0)->first();
        if(!$checkcart)
        {

          $model = new Cart;
          $model->userId = $userId;
          $model->productId = $productId;
          $model->quantity = 1;
          $model->cost = $productdetails->cost;
          $model->totalCost = $productdetails->cost;
          $model->itemName = $productdetails->productName;
          $model->sessionId = Session::getId();
          $model->orderId = 0;
          $model->adminId = Auth::user()->adminId;
          $model->created_by = Auth::user()->id;
          $model->save();
          if($model)
          {
            return true;
          }
          return false;
    }
    else
    {
      $modele = Cart::find($checkcart->id);
      $modele->quantity = $checkcart->quantity + 1;
      $modele->totalCost = $checkcart->totalCost + $productdetails->cost;
      $modele->save();
      if($modele)
      {
        return true;
      }
      return false;
    }

    }

    public static function deleteone($id)
    {
        $model = Cart::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted cart [".$id."]");
            return true;
        }
        return false;
    }

    public static function increaseCart($productId) {
      $productdetails = Products::where('id',$productId)->first();

      if(Auth::check())
      {
        $userId	= Auth::user()->id;
        log::info("logged in user increaseMyCart pid--".$productId."---uid--".$userId);

        $checkcart = Cart::where('productId',$productId)->where('userId',$userId)->where('completed',0)->orderBy('id','DESC')->limit(1)->first();
      }
      else
      { log::info("use session increaseMyCart");
        $checkcart = Cart::where('productId',$productId)->where('sessionId',session()->getId())->where('completed',0)->orderBy('id','DESC')->limit(1)->first();
      }

      // $newquantity = $checkdetails->quantity + 1;
      // $price = $details->cost;
      // $model = Cart::where('id', $checkdetails->id)->update(['quantity'=>$newquantity,'price'=>$price]);
      $modele = Cart::find($checkcart->id);
      $modele->quantity = $checkcart->quantity + 1;
      $modele->totalCost = $checkcart->totalCost + $productdetails->cost;
      $modele->save();
      if($modele) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function decreaseCart($productId) {
      $productdetails = Products::where('id',$productId)->first();

      if(Auth::check())
      {
        $userId	= Auth::user()->id;
        $checkcart = Cart::where('productId',$productId)->where('userId',$userId)->where('completed',0)->orderBy('id','DESC')->limit(1)->first();
      }
      else
      {
        $checkcart = Cart::where('productId',$productId)->where('sessionId',session()->getId())->where('completed',0)->orderBy('id','DESC')->limit(1)->first();
      }

      // $newquantity = $checkdetails->quantity - 1;
      // $price = $details->cost;
      // $model = Cart::where('id', $checkdetails->id)->update(['quantity'=>$newquantity,'price'=>$price]);
      $modele = Cart::find($checkcart->id);
      $modele->quantity = $checkcart->quantity - 1;
      $modele->totalCost = $checkcart->totalCost - $productdetails->cost;
      $modele->save();
      if($modele) {
        if($modele->quantity==0) {
          Cart::where('id', $checkcart->id)->update(['isDeleted'=>1,'completed'=>1]);
        }
        return true;
      }
      else {
        return false;
      }
    }

    public static function completesale($subtotal,$total,$discount,$tax,$taxamount) {
      $orderId = Orders::saveNew($subtotal,$total,$discount,$tax,$taxamount);
        if($orderId) {
          $u = Cart::where('sessionId',Session::getId())->where('completed',0)->update(['completed'=>1,'orderId'=>$orderId]);
          if($u) {
            Batches::updateProduct($orderId);
            return true;
          }
          return false;
        }
        return false;
    }

}
