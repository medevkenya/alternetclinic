<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Services;
use App\Activities;
use App\Treatments;

class Costs extends Model
{
    protected $table = 'costs';

    public static function storeone($patientId,$appointmentId,$serviceId) {
      $adminId	= Auth::user()->adminId;
      $created_by	= Auth::user()->id;

      $checkservice = Services::where('id',$serviceId)->first();
      $amount = $checkservice->appointmentFee;

      $model = new Costs;
      $model->patientId = $patientId;
      $model->appointmentId = $appointmentId;
      $model->amount = $amount;
      $model->description = "Appointment";
      $model->adminId = $adminId;
      $model->created_by = $created_by;
      $model->save();
      if ($model)
      {
          Activities::saveLog("Added cost for patient [".$patientId.", appointment id. - ".$appointmentId."]");
          return true;
      }
      return false;
    }

    public static function saveone($id,$amount,$description) {
      $adminId	= Auth::user()->adminId;
      $created_by	= Auth::user()->id;

      $checkservice = Treatments::where('id',$id)->first();
      $appointmentId = $checkservice->appointmentId;
      $patientId = $checkservice->patientId;

      $checkcost = Costs::where('appointmentId',$appointmentId)->where('patientId',$patientId)->first();

      $model = Costs::find($checkcost->id);
      if($description == "General Treatment") {
        $model->treatmentCost = $amount;
      }
      if($description == "Diagnosis") {
        $model->diagnosisCost = $amount;
      }
      if($description == "Prescriptions") {
        $model->prescriptionsCost = $amount;
      }
      if($description == "Lab Tests") {
        $model->labtestCost = $amount;
      }
      if($description == "Medicines") {
        $model->medicinesCost = $amount;
      }
      $model->save();
      if ($model)
      {
          Activities::saveLog("Updated ".$description." for patient [".$patientId.", appointment id. - ".$appointmentId."]");
          return true;
      }
      return false;
    }

    public static function editcost($id, $amountPaid,$status)
    {
      $model = Costs::find($id);
      $model->amountPaid = $amountPaid;
      $model->status = $status;
      $model->save();
      if ($model)
      {
          Activities::saveLog("Updated cost for patient [".$model->patientId.", appointment id. - ".$model->appointmentId."]");
          return true;
      }
      return false;
    }

    public static function getAll()
    {
      return Costs::select('costs.*','appointments.appointmentNo','appointments.appointmentDate','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','costs.patientId','=','users.id')
      ->leftJoin('appointments','costs.appointmentId','=','appointments.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('costs.adminId',Auth::user()->adminId)
      ->where('costs.isDeleted',0)
      ->orderBy('costs.id','DESC')
      ->get();
    }

    public static function getAllForPatient($patientId)
    {
      return Costs::select('costs.*','appointments.appointmentNo','appointments.appointmentDate','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','costs.patientId','=','users.id')
      ->leftJoin('appointments','costs.appointmentId','=','appointments.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('costs.adminId',Auth::user()->adminId)
      ->where('costs.patientId',$patientId)
      ->where('costs.isDeleted',0)
      ->orderBy('costs.id','DESC')
      ->get();
    }

    public static function getAllForCashier()
    {
      return Costs::select('costs.*','appointments.appointmentNo','appointments.appointmentDate','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','costs.patientId','=','users.id')
      ->leftJoin('appointments','costs.appointmentId','=','appointments.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('costs.adminId',Auth::user()->adminId)
      ->where('costs.isDeleted',0)
      ->orderBy('costs.id','DESC')
      ->get();
    }

    public static function getAllForAppointment($appointmentId)
    {
      return Costs::select('costs.*','appointments.appointmentNo','appointments.appointmentDate','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','costs.patientId','=','users.id')
      ->leftJoin('appointments','costs.appointmentId','=','appointments.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('costs.adminId',Auth::user()->adminId)
      ->where('costs.appointmentId',$appointmentId)
      ->where('costs.isDeleted',0)
      ->orderBy('costs.id','DESC')
      ->get();
    }

    public static function addamount($appointmentId,$patientId,$amount) {
      $check = Costs::where('appointmentId',$appointmentId)->where('patientId',$patientId)->first();
      $newPaid = $check->amountPaid + $amount;
      Costs::where('id',$check->id)->update(['amountPaid'=>$newPaid]);
    }

    public static function removeamount($appointmentId,$patientId,$amount) {
      $check = Costs::where('appointmentId',$appointmentId)->where('patientId',$patientId)->first();
      $newPaid = $check->amountPaid - $amount;
      Costs::where('id',$check->id)->update(['amountPaid'=>$newPaid]);
    }

}
