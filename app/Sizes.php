<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Sizes extends Model
{
    protected $table = 'sizes';

    public static function getAll() {
      return $list = Sizes::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($sizeName)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Sizes;
        $model->sizeName = $sizeName;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new size [".$sizeName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $sizeName)
    {
        $model = Sizes::find($id);
        $model->sizeName = $sizeName;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited size [".$sizeName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Sizes::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted size [".$id."]");
            return true;
        }
        return false;
    }

}
