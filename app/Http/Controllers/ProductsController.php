<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Products;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image as Image;

class ProductsController extends Controller {

	public function getProducts(Request $request)
	{
		if (!$request->categoryId) {
		$products = '<option value="">Select Category First</option>';
		}
		else {

		$products = '<option value="">Select</option>';
		$data = Products::select('id','productName')->where('categoryId',$request->categoryId)->where('isDeleted',0)->get();
		foreach ($data as $dt) {
				$products .= '<option value="'.$dt->id.'">'.$dt->productName.'</option>';
		}

		}
		log::info("products--".$products);
		return response()->json(['products'=>$products]);
	}

	public function products()
	{
		$list = Products::getAll();
		return view('products',['list'=>$list]);
	}

	public function addproduct(Request $request) {
	if($request->hasFile('photoUrl')) {


		$request->validate([

					'photoUrl' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

			]);

			//get filename with extension
			$filenamewithextension = $request->file('photoUrl')->getClientOriginalName();

			//get filename without extension
			$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

			//get file extension
			$extension = $request->file('photoUrl')->getClientOriginalExtension();

			//filename to store
			$filenametostore = $filename.'_'.time().'.'.$extension;

			//Upload File
			$destinationPath = public_path('/photos');
			$request->photoUrl->move($destinationPath, $filenametostore); // uploading file to given path

			$thumbnailpath = public_path('/photos'.'/'.$filenametostore);
			$img = Image::make($thumbnailpath)->resize(400, 500, function($constraint) {
					$constraint->aspectRatio();
			});
			$img->save($thumbnailpath);

			// $imgwatermark = Image::make(public_path('/photos'.'/'.$filenametostore));
			// /* insert watermark at bottom-right corner with 10px offset */
			// $imgwatermark->insert(public_path('images/logo.png'), 'bottom-right', 10, 10);
			// $imgwatermark->save(public_path('/photos'.'/'.$filenametostore));

			$productName = $request->productName;
			$cost = $request->cost;
			$categoryId = $request->categoryId;
			$brandId = $request->brandId;
			$sizeId = $request->sizeId;
			$colorId = $request->colorId;
			$description = $request->description;

			$saveimg = Products::storeone($productName,$cost,$categoryId,$brandId,$sizeId,$colorId,$description,$filenametostore);
			if($saveimg) {
				return redirect()->back()->with(['success' => "Product was added successfully"]);
			}
			else {
				return redirect()->back()->with(['error' => "Failed to add product"]);
			}

	}
	else {
		return redirect()->back()->withInput($request->input())->with('error', 'Failed to upload photo');
	}
}

public function editproduct(Request $request) {

$details = Products::where('id',$request->id)->first();

if($request->hasFile('photoUrl')) {

	$request->validate([

				'photoUrl' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

		]);

		//get filename with extension
		$filenamewithextension = $request->file('photoUrl')->getClientOriginalName();

		//get filename without extension
		$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

		//get file extension
		$extension = $request->file('photoUrl')->getClientOriginalExtension();

		//filename to store
		$filenametostore = $filename.'_'.time().'.'.$extension;

		//Upload File
		$destinationPath = public_path('/photos');
		$request->photoUrl->move($destinationPath, $filenametostore); // uploading file to given path

		$thumbnailpath = public_path('/photos'.'/'.$filenametostore);
		$img = Image::make($thumbnailpath)->resize(400, 500, function($constraint) {
				$constraint->aspectRatio();
		});
		$img->save($thumbnailpath);

		// $imgwatermark = Image::make(public_path('/photos'.'/'.$filenametostore));
		// /* insert watermark at bottom-right corner with 10px offset */
		// $imgwatermark->insert(public_path('images/logo.png'), 'bottom-right', 10, 10);
		// $imgwatermark->save(public_path('/photos'.'/'.$filenametostore));

	}
	else {
		$filenametostore = $details->photoUrl;
	}

		$id = $request->id;
		$productName = $request->productName;
		$cost = $request->cost;
		$categoryId = $request->categoryId;
		$brandId = $request->brandId;
		$sizeId = $request->sizeId;
		$colorId = $request->colorId;
		$description = $request->description;

		$saveimg = Products::updateone($id,$productName,$cost,$categoryId,$brandId,$sizeId,$colorId,$description,$filenametostore);
		if($saveimg) {
			return redirect()->back()->with(['success' => "Product was updated successfully"]);
		}
		else {
			return redirect()->back()->with(['error' => "Failed to update product"]);
		}
}

	public function deleteproduct(Request $request)
	{
			$id = $request->id;
			$delete = Products::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
