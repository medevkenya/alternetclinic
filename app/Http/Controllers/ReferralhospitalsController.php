<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Referralhospitals;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class ReferralhospitalsController extends Controller {

	public function referralhospitals()
	{
		$list = Referralhospitals::getAll();
		return view('referralhospitals',['list'=>$list]);
	}

	public function addreferralhospital(Request $request)
	{
			$name = $request->name;
			$location = $request->location;
			$specialities = $request->specialities;
			$contacts = $request->contacts;

			$check = Referralhospitals::where('name', $name)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Record already exists.']);
			} else {

			$add = Referralhospitals::storeone($name,$location,$specialities,$contacts);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New referralhospital was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating referralhospital.']);
			}

		}

	}


	public function editreferralhospital(Request $request)
	{
			$id = $request->id;
			$name = $request->name;
			$location = $request->location;
			$specialities = $request->specialities;
			$contacts = $request->contacts;

			$update = Referralhospitals::updateone($id, $name,$location,$specialities,$contacts);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The referralhospital was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating referralhospital']);
			}

	}

	public function deletereferralhospital(Request $request)
	{
			$id = $request->id;
			$delete = Referralhospitals::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
