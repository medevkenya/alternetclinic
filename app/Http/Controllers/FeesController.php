<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Fees;
use App\Costs;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class FeesController extends Controller {

	public function fees()
	{
		$list = Fees::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
		return view('fees',['list'=>$list]);
	}

	public function addfee(Request $request)
	{
			$feeName = $request->feeName;
			$amount = $request->amount;
			$appointmentId = $request->appointmentId;
			$patientId = $request->patientId;
			$adminId	= Auth::user()->adminId;

			$add = Fees::storeone($feeName,$amount,$appointmentId,$patientId);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New fee was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating fee.']);
			}

	}


	public function editfee(Request $request)
	{
			$id = $request->id;
			$feeName = $request->feeName;
			$amount = $request->amount;
			$status = $request->status;

			$update = Fees::updateone($id, $feeName,$amount,$status);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The fee was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating fee']);
			}

	}

	public function editcost(Request $request)
	{
			$id = $request->id;
			$amountPaid = $request->amountPaid;
			$status = $request->status;

			$app = Costs::where('id',$id)->first();
			$otherFees = \App\Fees::totalForAppointment($app->appointmentId);
			$totalCosts = $otherFees + $app->treatmentCost + $app->prescriptionsCost + $app->diagnosisCost + $app->labtestCost + $app->medicinesCost;
			$balance = $totalCosts - $app->amountPaid;

			if($amountPaid > $totalCosts) {
				return Redirect::back()->with(['status0'=>'Amount paid cannot be more than total costs']);
			}

			$update = Costs::editcost($id, $amountPaid,$status);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The cost was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating cost']);
			}

	}

	public function deletefee(Request $request)
	{
			$id = $request->id;
			$delete = Fees::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
