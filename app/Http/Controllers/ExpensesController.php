<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Expenses;
use App\Expensetypes;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class ExpensesController extends Controller {

	public function expenses()
	{
		$list = Expenses::select('expenses.*','expensetypes.expensetypeName')
		->leftJoin('expensetypes','expenses.expenseTypeId','=','expensetypes.id')
		->where('expenses.adminId',Auth::user()->adminId)->where('expenses.isDeleted',0)->orderBy('expenses.id','DESC')->get();
		$expensetypes = Expensetypes::getAll();
		return view('expenses',['list'=>$list,'expensetypes'=>$expensetypes]);
	}

	public function postexpensesReport(Request $request)
	{
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

			$list = Expenses::select('expenses.*','expensetypes.expensetypeName')
			->leftJoin('expensetypes','expenses.expenseTypeId','=','expensetypes.id')
			->where('expenses.adminId',Auth::user()->adminId)
			->whereBetween('expenses.created_at', [$fromdate, $todate])
			->where('expenses.isDeleted',0)
			->orderBy('expenses.id','DESC')->get();
			$expensetypes = Expensetypes::getAll();
			return view('expenses',['list'=>$list,'expensetypes'=>$expensetypes,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

	public function addexpense(Request $request)
	{
			$expenseTypeId = $request->expenseTypeId;
			$description = $request->description;
			$amount = $request->amount;

			$add = Expenses::storeone($expenseTypeId,$description,$amount);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New expense was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating expense.']);
			}

	}


	public function editexpense(Request $request)
	{
			$id = $request->id;
			$expenseTypeId = $request->expenseTypeId;
			$description = $request->description;
			$amount = $request->amount;

			$update = Expenses::updateone($id, $expenseTypeId,$description,$amount);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The expense was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating expense']);
			}

	}

	public function deleteexpense(Request $request)
	{
			$id = $request->id;
			$delete = Expenses::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
