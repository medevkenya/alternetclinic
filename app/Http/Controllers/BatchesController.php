<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Batches;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image as Image;

class BatchesController extends Controller {

	public function batches()
	{
		$list = Batches::getAll();
		return view('batches',['list'=>$list]);
	}

	public function addbatch(Request $request)
	{
			$batchNo = $request->batchNo;
			$quantity = $request->quantity;
			$expiryDate = $request->expiryDate;
			$productId = $request->productId;

			$newDate = date("Y-m-d", strtotime($expiryDate));

			if($newDate < date('Y-m-d')) {
				return Redirect::back()->with(['status0'=>'Expiry date must be equal or greater than today']);
			}

			$add = Batches::storeone($batchNo,$productId,$quantity,$expiryDate);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New batch was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating batch.']);
			}
	}

	public function editbatch(Request $request)
	{
			$id = $request->id;
			$batchNo = $request->batchNo;
			$quantity = $request->quantity;
			$expiryDate = $request->expiryDate;
			$productId = $request->productId;
			$update = Batches::updateone($id, $batchNo,$productId,$quantity,$expiryDate);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The batch was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating batch']);
			}
	}

	public function deletebatch(Request $request)
	{
			$id = $request->id;
			$delete = Batches::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
