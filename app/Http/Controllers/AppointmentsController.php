<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Appointments;
use App\Treatments;
use App\Patients;
use App\Doctors;
use App\Services;
use App\Admissions;
use App\Wards;
use App\Costs;
use App\Referrals;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class AppointmentsController extends Controller {

	public function appointments()
	{
		$doctors = Doctors::select('id','firstName','lastName')->where('roleId',2)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
		$services = Services::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
		$list = Appointments::getAll();
		return view('appointments',['list'=>$list,'doctors'=>$doctors,'services'=>$services]);
	}

	public function appointmentsReport()
	{
		$list = Appointments::getAll();
		return view('appointmentsreport',['list'=>$list]);
	}

	public function postappointmentsreport(Request $request)
{
		$fromdate = $request->fromdate;
		$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
		$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
		$todate = $request->todate;
		$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
		$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

			$list = Appointments::select('appointments.*','users.firstName','users.lastName','services.name as serviceName')
      ->leftJoin('users','appointments.doctorId','=','users.id')
      ->leftJoin('services','appointments.serviceId','=','services.id')
      ->where('appointments.adminId',Auth::user()->adminId)
			->whereBetween('appointments.appointmentDate', [$fromdate, $todate])
      ->where('appointments.isDeleted',0)
      ->orderBy('appointments.id','DESC')
      ->get();

		return view('appointmentsreport',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
}

	public function viewAppointment($id) {
		$details = Appointments::select('appointments.*','users.firstName','users.lastName','services.name as serviceName')
		->leftJoin('users','appointments.doctorId','=','users.id')
		->leftJoin('services','appointments.serviceId','=','services.id')
		->where('appointments.id',$id)
		->where('appointments.adminId',Auth::user()->adminId)
		->where('appointments.isDeleted',0)
		->first();
		if(!$details)
		{
			return Redirect::back()->with(['status0'=>'Appointment does not exists.']);
		}
		$patientDetails = Patients::where('id',$details->patientId)->first();
		$treatmentdetails = Treatments::where('appointmentId',$id)->first();
		return view('viewAppointment',['treatmentdetails'=>$treatmentdetails,'details'=>$details,'patientDetails'=>$patientDetails]);
	}

	public function addappointment(Request $request)
	{
			$patientId = $request->patientId;
			$appointmentDate = $request->appointmentDate;
			$doctorId = $request->doctorId;
			$serviceId = $request->serviceId;

					$add = Appointments::storeone($patientId,$doctorId,$serviceId,$appointmentDate);
					if ($add) {
							return Redirect::back()->with(['status1'=>'New appointment was created successfully.']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while creating appointment.']);
					}

	}


	public function editappointment(Request $request)
	{
			$id = $request->id;
			$appointmentDate = $request->appointmentDate;
			$doctorId = $request->doctorId;
			$serviceId = $request->serviceId;
			$status = $request->status;

					$update = Appointments::updateone($id, $doctorId,$serviceId,$appointmentDate,$status);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The appointment was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating appointment']);
					}

	}


	public function deleteappointment(Request $request)
	{
			$id = $request->id;
			$delete = Appointments::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Appointment was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting appointment']);
			}
	}

	public function edittreatment(Request $request)
	{
			$id = $request->id;
			$temperature = $request->temperature;
			$bp = $request->bp;
			$bloodSugar = $request->bloodSugar;
			$cholestrol = $request->cholestrol;
			$giveMedicine = $request->giveMedicine;
			$treatmentCost = $request->treatmentCost;

					$update = Treatments::edittreatment($id, $bp,$bloodSugar,$temperature,$cholestrol,$giveMedicine,$treatmentCost);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The treatment was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating treatment']);
					}

	}

	public function editprescription(Request $request)
	{
			$id = $request->id;
			$prescriptions = $request->prescriptions;
			$prescriptionsCost = $request->prescriptionsCost;

					$update = Treatments::editprescription($id,$prescriptions,$prescriptionsCost);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The prescriptions was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating prescriptions']);
					}

	}

	public function editmedicines(Request $request)
	{
			$id = $request->id;
			$medicines = $request->medicines;
			$medicinesCost = $request->medicinesCost;

					$update = Treatments::editmedicines($id, $medicines,$medicinesCost);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The medicines were updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating medicines']);
					}

	}

	public function editdiagnosis(Request $request)
	{
			$id = $request->id;
			$diagnosis = $request->diagnosis;
			$diagnosisCost = $request->diagnosisCost;

					$update = Treatments::editdiagnosis($id, $diagnosis,$diagnosisCost);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The diagnosis was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating diagnosis']);
					}

	}

	public function editlabtest(Request $request)
	{
			$id = $request->id;
			$labtest = $request->labtest;
			$labtestCost = $request->labtestCost;

					$update = Treatments::editlabtest($id, $labtest,$labtestCost);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The lab test was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating lab test']);
					}

	}

	public function admitpatient(Request $request)
	{
			$patientId = $request->patientId;
			$appointmentId = $request->appointmentId;
			$admitReason = $request->admitReason;
			$wardId = $request->wardId;
			$bedNo = $request->bedNo;

			$check = Wards::where('id',$wardId)->where('spacesavailable','>',0)->where('status','Available')->first();
			if(!$check) {
				return Redirect::back()->with(['status0'=>'Ward is not available for admitting patients.']);
			}

			$add = Admissions::admitpatient($patientId,$bedNo,$wardId,$admitReason,$appointmentId);
			if ($add) {
					Wards::occupy($wardId);
					return Redirect::back()->with(['status1'=>'Patient was admitted successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while admitting patient.']);
			}

	}

	public function referpatient(Request $request)
	{
			$patientId = $request->patientId;
			$appointmentId = $request->appointmentId;
			$reason = $request->reason;
			$referralHospitalId = $request->referralHospitalId;

			$check = Costs::where('patientId',$patientId)->where('appointmentId',$appointmentId)->where('status','Paid')->where('isDeleted',0)->first();
			if(!$check) {
				return Redirect::back()->with(['status0'=>'Patient has pending bills, cannot be referred. Please check with cashier.']);
			}

			$add = Referrals::referpatient($patientId,$referralHospitalId,$reason,$appointmentId);
			if ($add) {
					return Redirect::back()->with(['status1'=>'Patient was referred successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while referring patient.']);
			}

	}

	public function dischargepatient(Request $request)
	{
			$patientId = $request->patientId;
			$appointmentId = $request->appointmentId;
			$dischargeReason = $request->dischargeReason;

			$check = Costs::where('patientId',$patientId)->where('appointmentId',$appointmentId)->where('status','Paid')->where('isDeleted',0)->first();
			if(!$check) {
				return Redirect::back()->with(['status0'=>'Patient has pending bills, cannot be discharged. Please check with cashier.']);
			}

			$add = Admissions::dischargepatient($patientId,$dischargeReason,$appointmentId);
			if ($add) {
					return Redirect::back()->with(['status1'=>'Patient was discharged successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while discharging patient.']);
			}

	}

}
