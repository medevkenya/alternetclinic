<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Wards;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class WardsController extends Controller {

	public function wards()
	{
		$list = Wards::getAll();
		return view('wards',['list'=>$list]);
	}

	public function addward(Request $request)
	{
			$wardName = $request->wardName;
			$spacesavailable = $request->spacesavailable;
			$spacesoccupied = $request->spacesoccupied;
			$adminId	= Auth::user()->adminId;

			$check = Wards::where('wardName', $wardName)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Record already exists.']);
			} else {

			$add = Wards::storeone($wardName,$spacesavailable,$spacesoccupied);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New ward was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating ward.']);
			}

		}

	}


	public function editward(Request $request)
	{
			$id = $request->id;
			$wardName = $request->wardName;
			$spacesavailable = $request->spacesavailable;
			$spacesoccupied = $request->spacesoccupied;

			$update = Wards::updateone($id, $wardName,$spacesavailable,$spacesoccupied);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The ward was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating ward']);
			}

	}

	public function deleteward(Request $request)
	{
			$id = $request->id;
			$delete = Wards::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
