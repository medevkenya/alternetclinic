<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Brands;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class BrandsController extends Controller {

	public function getBrands(Request $request)
	{
		if (!$request->categoryId) {
		$models = '<option value="">Select Category First</option>';
		}
		else {

		$models = '<option value="">Select</option>';
		$data = Brands::where('categoryId',$request->categoryId)->where('isDeleted',0)->get();
		foreach ($data as $dt) {
				$models .= '<option value="'.$dt->id.'">'.$dt->brandName.'</option>';
		}

		}
		log::info("models--".$models);
		return response()->json(['models'=>$models]);
	}

	public function brands()
	{
		$list = Brands::getAll();
		return view('brands',['list'=>$list]);
	}

	public function addbrand(Request $request)
	{
			$brandName = $request->brandName;
			$categoryId = $request->categoryId;
			$add = Brands::storeone($brandName,$categoryId);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New brand was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating brand.']);
			}
	}

	public function editbrand(Request $request)
	{
			$id = $request->id;
			$brandName = $request->brandName;
			$categoryId = $request->categoryId;
			$update = Brands::updateone($id, $brandName, $categoryId);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The brand was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating brand']);
			}
	}

	public function deletebrand(Request $request)
	{
			$id = $request->id;
			$delete = Brands::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
