<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Subscriptions;
use App\Packages;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class SubscriptionsController extends Controller {

	public function subscriptions()
	{

		$details = Subscriptions::select('subscriptions.*','packages.name as packageName','packages.amount')
		->leftJoin('packages','subscriptions.packageId','=','packages.id')
		->where('subscriptions.adminId',Auth::user()->adminId)->where('subscriptions.isDeleted',0)->first();

		$profileDetails = User::where('id',Auth::user()->id)->first();

		$now = time(); // or your date as well
		$datediff = $now - strtotime($details->expiryDate);
		$days = round($datediff / (60 * 60 * 24));
		if($days < 0) {
			$days = 0;
		}

		return view('subscriptions',['profileDetails'=>$profileDetails,'details'=>$details,'days'=>$days]);

	}

}
