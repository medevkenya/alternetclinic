<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Guardians;
use App\Doctors;
use App\Services;
use App\Appointments;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class DoctorsController extends Controller {

	public function doctors()
	{
		$list = Doctors::where('roleId',2)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
		return view('doctors',['list'=>$list]);
	}

	public function deletedoctor(Request $request)
	{
			$id = $request->id;
			$delete = Doctors::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

	public function viewDoctor($doctorId)
	{

		$doctors = Doctors::select('id','firstName','lastName')->where('roleId',2)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
		$services = Services::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();

		$details = Doctors::where('id',$doctorId)->where('roleId',2)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
		if(!$details)
		{
			return Redirect::back()->with(['status0'=>'Doctor does not exists.']);
		}
		return view('viewDoctor',['details'=>$details,'doctors'=>$doctors,'services'=>$services]);

	}

}
