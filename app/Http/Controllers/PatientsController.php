<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Patients;
use App\Guardians;
use App\Doctors;
use App\Services;
use App\Appointments;
use App\Settings;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class PatientsController extends Controller {

	public function patients()
	{
		$list = Patients::where('roleId',0)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
		return view('patients',['list'=>$list]);
	}

	public function addpatient()
	{
		$doctors = Doctors::select('id','firstName','lastName')->where('roleId',2)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
		$services = Services::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
		return view('addpatient',['doctors'=>$doctors,'services'=>$services]);
	}

	public function editPatient($patientId)
	{

		$doctors = Doctors::select('id','firstName','lastName')->where('roleId',2)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
		$services = Services::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
		$details = Patients::where('id',$patientId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
		if(!$details)
		{
			return Redirect::back()->with(['status0'=>'Patient does not exists.']);
		}
		$gdetails = Guardians::where('patientId',$patientId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
		return view('editPatient',['details'=>$details,'gdetails'=>$gdetails,'doctors'=>$doctors,'services'=>$services]);

	}

	public function doaddpatient(Request $request)
	{

			$firstName = $request->firstName;
			$lastName = $request->lastName;
			$middleName = $request->middleName;
			$email = $request->email;
			$mobileNo = $request->mobileNo;
			$location = $request->location;
			$gender = $request->gender;
			$dob = $request->dob;
			$roleId = 0;
			$password = $request->firstName;
			$adminId	= Auth::user()->adminId;

			$guardianName = $request->guardianName;
			$guardianEmail = $request->guardianEmail;
			$guardianPhone = $request->guardianPhone;
			$guardianLocation = $request->guardianLocation;
			$guardianGender = $request->guardianGender;

			$doctorId = $request->doctorId;
			$serviceId = $request->serviceId;

			$count = Patients::where('adminId',$adminId)->where('isDeleted', 0)->whereRaw('Date(created_at) = CURDATE()')->count();//->orderBy('id','DESC')->limit(1)->first();
			//if($checklast) {
			  $cc = $count + 1;
				$patientNo = $cc."SHS/".date('Ymd');
			// }
			// else {
			// 	$patientNo = "1SHS/".date('Ymd');
			// }

			$check = Patients::where('firstName', $firstName)->where('lastName', $lastName)->where('middleName', $middleName)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Patient with similar name already exists.']);
			} else {
					$add = Patients::storeone($patientNo,$firstName,$middleName,$lastName,$mobileNo,$email,$password,$roleId,$location,$gender,$dob);
					if ($add) {
						  Guardians::storeone($add,$guardianName,$guardianEmail,$guardianPhone,$guardianLocation,$guardianGender);
							Appointments::storeone($add,$doctorId,$serviceId,date('Y-m-d'));
							return Redirect::back()->with(['status1'=>'New patient was registered successfully.']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while registering patient.']);
					}
			}
	}


	public function doeditpatient(Request $request)
	{
					$id = $request->id;
					$firstName = $request->firstName;
					$lastName = $request->lastName;
					$middleName = $request->middleName;
					$email = $request->email;
					$mobileNo = $request->mobileNo;
					$location = $request->location;
					$gender = $request->gender;
					$dob = $request->dob;

					$guardianId = $request->guardianId;
					$guardianName = $request->guardianName;
					$guardianEmail = $request->guardianEmail;
					$guardianPhone = $request->guardianPhone;
					$guardianLocation = $request->guardianLocation;
					$guardianGender = $request->guardianGender;

					$update = Patients::updateone($id,$firstName,$middleName,$lastName,$mobileNo,$email,$location,$gender,$dob);
					if ($update) {
							Guardians::updateone($guardianId,$guardianName,$guardianEmail,$guardianPhone,$guardianLocation,$guardianGender);
							return Redirect::to('patients')->with('success', 'Patient details were successfully updated');
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating record']);
					}

	}

	public function deletepatient(Request $request)
	{
			$id = $request->id;
			$delete = Patients::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

	public function viewPatient($patientId)
	{

		$doctors = Doctors::select('id','firstName','lastName')->where('roleId',2)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
		$services = Services::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
		$details = Patients::where('id',$patientId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
		if(!$details)
		{
			return Redirect::back()->with(['status0'=>'Patient does not exists.']);
		}
		$gdetails = Guardians::where('patientId',$patientId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->first();
		return view('viewPatient',['details'=>$details,'gdetails'=>$gdetails,'doctors'=>$doctors,'services'=>$services]);

	}

	public function printCard(Request $request)
{

	$patientId = $request->patientId;

$settings = Settings::getDetails();
$details = Patients::where('id',$patientId)->first();

$data['created_at']=$details->created_at;
$data['patientNo']=$details->patientNo;
$data['mobileNo']=$details->mobileNo;
$data['patientName']=strtoupper($details->firstName).' '.strtoupper($details->middleName).' '.strtoupper($details->lastName);
$data['companyName']=$settings->companyName;
$data['companyPhoto']=$settings->companyPhoto;

	$pdf = PDF::loadView('cardpdf', $data,[],['format'=>'A7-L',
	    'title' => 'Patient Card',
	    'orientation' => 'L',
	    'margin_left' => '0',
	    'margin_right' => '0',
	    'margin_top' => '0',
	    'margin_bottom' => '0']);
	return $pdf->download(''.$data['patientName'].'.pdf');

	//$pathimg = public_path('public/profiles/'.$settings->companyPhoto);

	//$data .= '<center><img src="'.$pathimg.'" style="width:100%;height:136px;"></center>';

	// $data .= '<div class="cardcard" id="cardcard">
	//   <header>
	//     <time
	//   datetime="2018-05-15T19:00">'.date("M d - Y", strtotime($details->created_at)).'</time>
	//     <div class="logocard">
	//       <!-- <span>
	//         <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 234.5 53.7"><style>.st0{fill:none;stroke:#FFFFFF;stroke-width:5;stroke-miterlimit:10;}</style><path d="M.6 1.4L116.9 52l117-50.6" class="st0"/></svg>
	//       </span> -->
	//       <img src="'.$pathimg.'" width="50px" height="50px" style="border-radius:50%;" />
	//     </div>
	//     <div class="sponsor">'.ucwords($details->mobileNo).'</div>
	//   </header>
	//   <div class="announcement">
	//     <h3>'.ucwords($details->patientNo).'</h3>
	//     <h1>'.strtoupper($details->firstName).' '.strtoupper($details->middleName).' '.strtoupper($details->lastName).'</h1>
	//     <h3 class="italic">'.$settings->companyName.'</h3>
	//   </div>
	// </div>';

	//$pdf = PDF::loadHTML($data);

	// $pdf = PDF::loadHTML(
	// 		$data,
	// 		[
	// 			'title' => 'Report',
	// 			'format' => 'A4-P',
	// 			'orientation' => 'P'
	// 		]);

	//$pdf->setPaper('A4', 'landscape');
	 //return $pdf->stream();
 //return $pdf->stream(''.$docutype.'_'.$name.'.pdf');
	//$pdf = PDF::loadView('pdf.document', $data);
//$pdf->stream('document.pdf');
//$time = time();
//file_put_contents('public/reports/'.$course_name.'_Payments_Report_'.$startdate.'_'.$enddate.'.pdf', $pdf->output());
//$url = url('/').'/public/reports/'.$course_name.'_Payments_Report_'.$startdate.'_'.$enddate.'.pdf';
//redirect('paymentsreport')->with('status11', $url);
//return $pdf->download('_Payments_Report.pdf');

	/*$pdf = App::make('dompdf.wrapper');
	$pdf->loadHTML($data);
	$time = time();
	return $pdf->download($course_name.'_Payments_Report_'.$time.'.pdf');*/

}

}
