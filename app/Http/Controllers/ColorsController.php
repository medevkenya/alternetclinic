<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Colors;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class ColorsController extends Controller {

	public function colors()
	{
		$list = Colors::getAll();
		return view('colors',['list'=>$list]);
	}

	public function addcolor(Request $request)
	{
			$colorName = $request->colorName;
			$add = Colors::storeone($colorName);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New color was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating color.']);
			}
	}

	public function editcolor(Request $request)
	{
			$id = $request->id;
			$colorName = $request->colorName;
			$update = Colors::updateone($id, $colorName);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The color was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating color']);
			}
	}

	public function deletecolor(Request $request)
	{
			$id = $request->id;
			$delete = Colors::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
