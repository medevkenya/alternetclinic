<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Guardians;
use App\Services;
use App\Appointments;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class PharmacyController extends Controller {

	public function pharmacy()
	{
		return view('pharmacy');
	}

	public function pharmacyReport()
	{
		return view('pharmacyReport');
	}

	public function postpharmacyReport(Request $request)
	{
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

				$list = Appointments::select('appointments.*','users.firstName','users.lastName','services.name as serviceName')
				->leftJoin('users','appointments.doctorId','=','users.id')
				->leftJoin('services','appointments.serviceId','=','services.id')
				->where('appointments.giveMedicine','Yes')
				->where('appointments.adminId',Auth::user()->adminId)
				->whereBetween('appointments.appointmentDate', [new Carbon($fromdate), new Carbon($todate)])
				//->whereBetween('appointments.appointmentDate', [$fromdate, $todate])
				->where('appointments.isDeleted',0)
				->orderBy('appointments.id','DESC')
				->get();

			return view('pharmacyReport',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

}
