<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Costs;
use App\Services;
use App\Appointments;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class CashierController extends Controller {

	public function receiveCash()
	{
		return view('receiveCash');
	}

	public function cashier()
	{
		return view('cashier');
	}

	public function cashierReport()
	{
		//$list = Costs::getAll();
		return view('cashierReport');
	}

	public function postcashierReport(Request $request)
	{
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

			$list = Costs::select('costs.*','users.firstName','users.lastName','services.name as serviceName')
			      ->leftJoin('users','costs.patientId','=','users.id')
			      ->leftJoin('appointments','costs.appointmentId','=','appointments.id')
			      ->leftJoin('services','appointments.serviceId','=','services.id')
			      ->where('costs.adminId',Auth::user()->adminId)
						->whereBetween('costs.created_at', [$fromdate, $todate])
			      ->where('costs.isDeleted',0)
			      ->orderBy('costs.id','DESC')
			      ->get();

			return view('cashierReport',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

}
