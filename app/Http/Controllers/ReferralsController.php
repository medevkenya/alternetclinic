<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Costs;
use App\Referrals;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class ReferralsController extends Controller {

	public function referrals()
	{
		$list = Referrals::getAll();
		return view('referrals',['list'=>$list]);
	}

	public function referralsReport(Request $request)
	{
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

			$list = Referrals::select('referrals.*','appointments.appointmentNo','users.firstName','users.lastName','referralhospitals.name as referralhospitalName')
			            ->leftJoin('users','referrals.patientId','=','users.id')
			            ->leftJoin('appointments','referrals.appointmentId','=','appointments.id')
			            ->leftJoin('referralhospitals','referrals.referralHospitalId','=','referralhospitals.id')
									->where('referrals.adminId',Auth::user()->adminId)
									->whereBetween('referrals.created_at', [$fromdate, $todate])
			            ->where('referrals.isDeleted',0)
			            ->orderBy('referrals.id','DESC')
			            ->get();

			return view('referrals',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

	public function editreferral(Request $request)
	{
			$id = $request->id;
			$referralHospitalId = $request->referralHospitalId;
			$reason = $request->reason;

			$add = Referrals::editreferral($id,$referralHospitalId,$reason);
			if ($add) {
					return Redirect::back()->with(['status1'=>'Referral was updated successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating referral.']);
			}
	}

	public function deletereferral(Request $request)
	{
			$id = $request->id;
			$delete = Referrals::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Referral was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting referral']);
			}
	}

}
