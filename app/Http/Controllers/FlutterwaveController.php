<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Payments;
use App\SMS;
use Auth;
use Log;
use App\mobile\Responseobject;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Response;

class FlutterwaveController extends Controller {

      public function flutterwaveinitiate(Request $request)
      {

          $amount = $request->amount;
          $packageId = $request->packageId;

          $results = Payments::initiatePayment($amount,$packageId);
          if($results != null) {
          return json_encode($results);
        }
        else {
          return null;
        }

      }

      public function flutterwaveverify(Request $request)
      {

          $reference = $request->tx_ref;
          $transaction_id = $request->transaction_id;

          $results = Payments::verifyPayment($reference,$transaction_id);
          return json_encode($results);

      }

      public function webhook()
    {
        // Retrieve the request's body
        $body = @file_get_contents("php://input");

        // retrieve the signature sent in the reques header's.
        $signature = (isset($_SERVER['verif-hash']) ? $_SERVER['verif-hash'] : '');

        /* It is a good idea to log all events received. Add code *
        * here to log the signature and body to db or file       */

        if (!$signature)
        {

          // only a post with rave signature header gets our attention
          log::info("Missing or invalid rave signature header");
          exit();

        }

        // Store the same signature on your server as an env variable and check against what was sent in the headers
        $local_signature = Yii::$app->params['flutterwave']['FLUTTERWAVE_SECKEY'];

        // confirm the event's signature
        if( $signature !== $local_signature )
        {

        // silently forget this ever happened
        log::info("Webhook's event's signature does not match the local signature");
        exit();

        }

        http_response_code(200); // PHP 5.4 or greater
        // parse event (which is json string) as object
        // Give value to your customer but don't give any output
        // Remember that this is a call from rave's servers and
        // Your customer is not seeing the response here at all
        $response = json_decode($body);
        if ($response->body->status == 'successful')
        {

          // TIP: you may still verify the transaction // before giving value.
          log::info("Successful transaction from webhook");
          $txref = $response->body->txRef;
          $amount = $response->body->amount;
          $charged_amount = $response->body->charged_amount;
          $transaction_id = $response->body->id;
          //$type => $response->body->event->type;

          //Proces Flutterwave payment and update transaction accordingly
          $fee = $charged_amount - $amount;
          $response = Payments::verifyPayment($txref,$transaction_id);
          //$response = Payments::processWebhook($txref,$fee,$transaction_id,json_encode($response->body));
          //header("HTTP/1.1 200 OK");
          return $this->asJson($response);

        }
        else
        {

          log::info("Invalid validation fields");

        }

        exit();
    }

}
