<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Outbox;
use App\SMS;
use App\Brandnames;
use Hash;
use Session;
use PDF;
use Log;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class CronsController extends Controller {

	public function cronsms()
	{
		date_default_timezone_set("Africa/Nairobi");

		$date= date('Y-m-d');
		$time = date('H:i');

		$checkbatch = SMS::where('overall_status','0')
		->where('send_date','=',$date)
		->where('send_time','<=',$time)
		->where('isDeleted',0)
		->orderBy('id','DESC')
		->first();
		if(!$checkbatch)
		{
			exit();
		}

		$ref_queue_id = $checkbatch->id;
		$message = $checkbatch->message;
		$senderId = $checkbatch->senderId;

		$checksenderid = Brandnames::where('id',$senderId)->first();
		$brandName = $checksenderid->brandName;

		$batch_limit_size = 1000;

	  $smslist = Outbox::where('ref_queue_id', $ref_queue_id)->where('status','!=','1')->where('status','!>','4')->limit($batch_limit_size)->get();

		$lines = array();
		$count_processed = 0;
		foreach( $smslist as $list ) {
		$phone = $list->mobileNo;
		$newphone =  substr($phone, -9);
		$phone = "254".$newphone;
		$lines[] = $phone;
		$count_processed++;

		//DB::insert('insert into messages_sent (phone,ref_queue_id,status,message) values (?,?,?,?)', [$newphone,$ref_queue_id,1,$message]);
		//DB::table('messages')->where('ref_queue_id', $ref_queue_id)->where('phone',$newphone)->delete();

		}

		if(count($lines)>1){
			$recipients = implode(',', $lines);
		}else{
			$recipients =$lines[0];
		}


		//$results = $gateway->sendMessage($recipients, $message,$brandName);

	// Specify your login credentials
	$username   = $checksenderid->username;
	$apiKey     = $checksenderid->apikey;
	$senderId = $checksenderid->brandName;

	$AT       = new AfricasTalking($username, $apiKey);

	// Get one of the services
	$sms      = $AT->sms();

	// Use the service
	$result   = $sms->send([
		'from'      => $senderId,
		'to'        => $recipients,
		'message'   => $message
	]);

		foreach($results as $result) {

		$msglen=strlen($message);
		$multipl = ceil($msglen/160);
		$costt = $multipl * 1;

		$phone = $result->number;
		$phone =  "254".substr($phone, -9);
	$status = $result->status;
	$messageid = $result->messageId;
	//echo " Cost: "   .$result->cost."\n";

	$getbatchfirst = SMS::where('id', $ref_queue_id)->first();

	if($status == "success") {

	Outbox::where('ref_queue_id', $ref_queue_id)->where('mobileNo', $phone)->update(['status' => 1]);

	//if($status == "Success" || $status == "Sent") {
		//DB::table('messages')->where('phone', $phone)->where('ref_queue_id', $ref_queue_id)->update(['status' => 1]);

		//$addsent = DB::insert('insert into messages_sent (phone,ref_queue_id) values (?,?)', [$phone,$ref_queue_id]);
		//DB::table('messages')->where('ref_queue_id', $ref_queue_id)->where('phone',$phone)->delete();
		$batch_sent = $getbatchfirst->batch_sent + 1;
		SMS::where('id', $ref_queue_id)->update(['batch_sent' => $batch_sent]);

	}
	else {
		//failed sending...queued for retry
		$rescurre = Outbox::where('ref_queue_id', $ref_queue_id)->where('mobileNo', $phone)->where('status','!=', 1)->first();
		if($rescurre) {
			$newstatus = $rescurre->status+2;
		Outbox::where('ref_queue_id', $ref_queue_id)->where('mobileNo', $phone)->update(['status' => $newstatus]);

		if($newstatus >= 4) {
			$fullTrial = $getbatchfirst->fullTrial + 1;
		SMS::where('id', $ref_queue_id)->update(['fullTrial' => $fullTrial]);
	}
	}

	$batch_processed = $getbatchfirst->batch_processed + 1;
	SMS::where('id', $ref_queue_id)->update(['batch_processed' => $batch_processed]);

	}
	//}
	//else {
		//DB::table('messages')->where('phone', $phone)->where('ref_queue_id', $ref_queue_id)->update(['status' => 3]);
		//SMS::where('phone', $phone)->where('ref_queue_id', $ref_queue_id)->update(['status' => 1]);
	//}

		}

		$getbatch = SMS::where('ref_queue_id', $ref_queue_id)->first();
		$totalGoneThrough = $getbatch->batch_sent + $getbatch->fullTrial;
		$batch_size= $getbatch->batch_size;

		//SMS::where('ref_queue_id', $ref_queue_id)->update(['batch_processed' => $batch_processed]);

		if($totalGoneThrough >= $batch_size)
		{
			SMS::where('ref_queue_id', $ref_queue_id)->update(['overall_status' => 1]);
			self::notifyOwner($ref_queue_id);
		}

		// DONE!!!
	}


	public static function notifyOwner($ref_queue_id)
	{

		$check = SMS::where('id',$ref_queue_id)->first();
		$batch_sent = $check->batch_sent;
		$batch_size = $check->batch_size;
		$fullTrial = $check->fullTrial;
		$messageContent = $check->message;

		$checksenderid = Brandnames::where('id',$check->senderId)->first();
		$brandName = $checksenderid->brandName;

		$admindetails = User::where('id',$check->adminId)->where('isDeleted',0)->first();
		$email = $admindetails->email;
		$fullName = $admindetails->firstName." ".$admindetails->lastName;

		$unitBalance = SMS::getBalance($check->senderId);

		Mail::send('emails.notify', $data, function($message) use ($pass,$email,$fullName,$batch_sent,$batch_size,$fullTrial,$messageContent,$unitBalance) {
		$message->to($email)->subject('SMS Sent Status');
		$message->from('mails@alternet.co.ke','AlternetSMS');
		});

	}

}
