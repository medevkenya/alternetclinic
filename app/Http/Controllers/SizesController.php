<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Sizes;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class SizesController extends Controller {

	public function sizes()
	{
		$list = Sizes::getAll();
		return view('sizes',['list'=>$list]);
	}

	public function addsize(Request $request)
	{
			$sizeName = $request->sizeName;
			$add = Sizes::storeone($sizeName);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New size was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating size.']);
			}
	}

	public function editsize(Request $request)
	{
			$id = $request->id;
			$sizeName = $request->sizeName;
			$update = Sizes::updateone($id, $sizeName);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The size was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating size']);
			}
	}

	public function deletesize(Request $request)
	{
			$id = $request->id;
			$delete = Sizes::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
