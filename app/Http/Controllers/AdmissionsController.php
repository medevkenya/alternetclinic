<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Costs;
use App\Admissions;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class AdmissionsController extends Controller {

	public function admissions()
	{
		$list = Admissions::getAll();
		return view('admissions',['list'=>$list]);
	}

	public function admissionsReport(Request $request)
	{
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

			$list = Admissions::select('admissions.*','appointments.appointmentNo','users.firstName','users.lastName','services.name as serviceName','wards.wardName')
			      ->leftJoin('users','admissions.patientId','=','users.id')
			      ->leftJoin('appointments','admissions.appointmentId','=','appointments.id')
			      ->leftJoin('services','appointments.serviceId','=','services.id')
						->leftJoin('wards','admissions.wardId','=','wards.id')
			      ->where('admissions.adminId',Auth::user()->adminId)
						->whereBetween('admissions.created_at', [$fromdate, $todate])
			      ->where('admissions.isDeleted',0)
			      ->orderBy('admissions.id','DESC')
			      ->get();

			return view('admissions',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

	public function editadmission(Request $request)
	{
			$id = $request->id;
			$admitReason = $request->admitReason;
			$bedNo = $request->bedNo;

			$add = Admissions::editadmission($id,$bedNo,$admitReason);
			if ($add) {
					return Redirect::back()->with(['status1'=>'Admission was updated successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating admission.']);
			}
	}

}
