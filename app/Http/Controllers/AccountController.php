<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Activities;
use App\Roles;
use App\Appointments;
use App\Admissions;
use App\Referrals;
use App\Referralhospitals;
use App\Services;
use App\Wards;
use App\Treatments;
use App\Costs;
use App\Doctors;
use Mail;
use Hash;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;
use Carbon\Carbon;

class AccountController extends Controller
{

    public function dashboard()
    {
        $countPatients = User::where('roleId',0)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $countDoctors = Doctors::where('roleId',2)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $countAdmissions = Admissions::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $countAppointments = Appointments::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);

        $lastweek = Costs::whereBetween('created_at', [$start_week, $end_week])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');
        $thisweek = Costs::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');

        if($lastweek > $thisweek) {
          $weeksign = "-";
          $diff = $lastweek - $thisweek;
          $wpercentage = ($diff / $lastweek) * 100;
        }
        else {
          if($thisweek > 0) {
          $weeksign = "+";
          $diff = $thisweek - $lastweek;
          $wpercentage = ($diff / $thisweek) * 100;
        }
          else {
            $weeksign = null;
            $wpercentage = 0;
          }
        }


        $currentMonth = date('m');
        $lastmonth = Costs::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');
        $thismonth = Costs::whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');

        if($lastmonth > $thismonth) {
          $monthsign = "-";
          $mdiff = $lastmonth - $thismonth;
          $mpercentage = ($mdiff / $lastmonth) * 100;
        }
        else {
          if($thismonth > 0) {
          $monthsign = "+";
          $mdiff = $thismonth - $lastmonth;
          $mpercentage = ($mdiff / $thismonth) * 100;
        }
        else {
          $monthsign = null;
          $mpercentage = 0;
        }
        }

        $labtests = Treatments::where('labtest','!=',null)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $services = Services::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $availablewards = Wards::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('spacesavailable');
        $occupiedwards = Wards::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('spacesoccupied');
        $referrals = Referrals::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $hospitals = Referralhospitals::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();

        return view('dashboard',['labtests'=>$labtests,'services'=>$services,'availablewards'=>$availablewards,'occupiedwards'=>$occupiedwards,
        'monthsign'=>$monthsign,'mpercentage'=>$mpercentage,'wpercentage'=>$wpercentage,'referrals'=>$referrals,'hospitals'=>$hospitals,
        'weeksign'=>$weeksign,'thisweek'=>$thisweek,'thismonth'=>$thismonth,'countPatients'=>$countPatients,
        'countDoctors'=>$countDoctors,'countAdmissions'=>$countAdmissions,'countAppointments'=>$countAppointments]);
    }

    public function profile()
    {
      $userid	= Auth::user()->id;
      $details = User::where('id',$userid)->first();
      return view('profile',['details'=>$details]);
    }

    public function activities()
    {
      return view('activities');
    }

    public function changepassword()
    {
        return view('changepassword');
    }

    public function doChangePass(Request $request)
    {
        $oldpassword	= $request->oldpassword;
        $password	= $request->password;
        $cpassword 	= $request->cpassword;
        $userid	= Auth::user()->id;

        $oldhashedPassword = Hash::make($oldpassword);

        $checkold = User::where('id', '=', $userid)->first();
        if (!Hash::check($oldpassword, $checkold->password)) {
            return redirect()->back()->withInput($request->input())->with('error', 'Current password is wrong');
        } elseif (strlen($password) < 6) {
            return redirect()->back()->withInput($request->input())->with('error', 'Password must be at least 6 characters');
        } elseif ($password != $cpassword) {
            return redirect()->back()->withInput($request->input())->with('error', 'Passwords do not match');
        }
        else
        {

            $hashedPassword = Hash::make($password);

            $edituserpass = User::where('id', $userid)->update(['password' => $hashedPassword]);
            if ($edituserpass) {
                return redirect()->back()->withInput($request->input())->with('success', 'Your password was changed successfully');
            } else {
                return redirect()->back()->withInput($request->input())->with('error', 'Failed to change password, try again');
            }

        }
    }


    public function updateProfile(Request $request)
    {
      //https://stackoverflow.com/questions/51962486/datepicker-set-maximum-date-to-18-years-ago-from-current-date
        request()->validate([
            'firstName' => 'required|min:3|max:50',
            'lastName' => 'required|min:3|max:50',
            'mobileNo' => 'required',// input starts with 01 and is followed by 9 numbers
            'email' => 'required',
            // 'dob' => 'required|date_format:d-m-Y|before:18 years',
            //'terms' => 'accepted',
        ], [
            'firstName.required' => 'First Name is required',
            'firstName.min' => 'First Name must be at least 3 characters.',
            'firstName.max' => 'First Name should not be greater than 20 characters.',
            'lastName.required' => 'Last Name is required',
            'lastName.min' => 'Last Name must be at least 3 characters.',
            'lastName.max' => 'Last Name should not be greater than 20 characters.',
            'mobileNo.required' => 'Mobile Number is required',
            'mobileNo.regex' => 'Mobile number should be 10 digits starting with 07',
            //'about.required' => 'Provide brief description about yourself',
            //'terms.required' => 'Please accept our terms of service',
        ]);

        // $input = request()->except('password','confirm_password');
        // $user=new User($input);//
        // $user->password=bcrypt(request()->password);
        // $user->save();
        //request()->firstName,request()->lastName,request()->mobileNo,,request()->password
        $res = User::editMyProfile(request()->all());
        if($res) {
          return redirect()->back()->withInput($request->input())->with('success', 'Your profile was updated successfully');
        }
        else {
          return redirect()->back()->withInput($request->input())->with('error', 'Failed to save. Please try again');
        }

    }

    public function changePic()
    {
      $userid	= Auth::user()->id;
      $details = User::where('id',$userid)->first();
      return view('changePic',['details'=>$details]);
    }

    public function UpdatePic(Request $request)
    {

  // getting all of the post data
        $id	= Auth::user()->id;
        $file = array('image' => $request->profilePic);

        // setting up rules
        //  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000 required|image|mimes:jpeg,png,jpg,gif,svg|max:2048
        $rules = array('image' => 'required|image|mimes:jpeg,png,jpg,gif|max:5048',);
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);

        // checking file is valid.
        if ($request->profilePic->isValid()) {
            //$destinationPath = 'otheruploads'; // upload path
            $destinationPath = public_path('/profiles');

            $extension = $request->profilePic->getClientOriginalExtension(); // getting image extension

            $fileName = rand(11111, 99999).'.'.$extension; // renameing image

            $request->profilePic->move($destinationPath, $fileName); // uploading file to given path

            //file name to take to db
            $profilePic=  $fileName;

            User::where('id', $id)->update(['profilePic' => $profilePic]);

            return Redirect::to('profile')->with(['status1'=>'Your profile picture was updated successfully.']);
        } else {
            return Redirect::to('profile')->with(['status0'=>'Sorry, error occurred while updating your profile picture. Try again.']);
        }
    }

    public function ErrorPage()
    {
        $datee = date('Y-m-d');
        return view('error');
    }

}
