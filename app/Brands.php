<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Brands extends Model
{
    protected $table = 'brands';

    public static function getAll() {
      return $list = Brands::select('brands.*','categories.categoryName')
      ->leftJoin('categories','brands.categoryId','=','categories.id')
      ->where('brands.adminId',Auth::user()->adminId)->where('brands.isDeleted',0)->orderBy('brands.id','DESC')->get();
    }

    public static function storeone($brandName,$categoryId)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Brands;
        $model->brandName = $brandName;
        $model->categoryId = $categoryId;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new brand [".$brandName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $brandName,$categoryId)
    {
        $model = Brands::find($id);
        $model->brandName = $brandName;
        $model->categoryId = $categoryId;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited brand [".$brandName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Brands::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted brand [".$id."]");
            return true;
        }
        return false;
    }

}
