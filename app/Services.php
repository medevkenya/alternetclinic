<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Services extends Model
{
    protected $table = 'services';

    public static function getAll() {
      return $list = Services::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($name,$description,$appointmentFee)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Services;
        $model->name = $name;
        $model->description = $description;
        $model->appointmentFee = $appointmentFee;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new service [".$name."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $name,$description,$appointmentFee)
    {
        $model = Services::find($id);
        $model->name = $name;
        $model->description = $description;
        $model->appointmentFee = $appointmentFee;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited service [".$name."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Services::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted service [".$id."]");
            return true;
        }
        return false;
    }

    }
