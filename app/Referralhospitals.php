<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Referralhospitals extends Model
{
    protected $table = 'referralhospitals';

    public static function getAll() {
      return $list = Referralhospitals::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($name,$location,$specialities,$contacts)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Referralhospitals;
        $model->name = $name;
        $model->location = $location;
        $model->specialities = $specialities;
        $model->contacts = $contacts;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new referral hospital [".$name."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $name,$location,$specialities,$contacts)
    {
        $model = Referralhospitals::find($id);
        $model->name = $name;
        $model->location = $location;
        $model->specialities = $specialities;
        $model->contacts = $contacts;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited referral hospital [".$name."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Referralhospitals::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted referral hospital [".$id."]");
            return true;
        }
        return false;
    }

    }
