<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Costs;
use App\Activities;

class Fees extends Model
{
    protected $table = 'fees';

    public static function getAllForAppointment($appointmentId) {
      return Fees::where('appointmentId',$appointmentId)->where('isDeleted',0)->get();
    }

    public static function totalForAppointment($appointmentId) {
      return Fees::where('appointmentId',$appointmentId)->where('isDeleted',0)->sum('amount');
    }

    public static function storeone($feeName,$amount,$appointmentId,$patientId)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Fees;
        $model->feeName = $feeName;
        $model->amount = $amount;
        $model->patientId = $patientId;
        $model->appointmentId = $appointmentId;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new fee [".$feeName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $feeName, $amount, $status)
    {
        $model = Fees::find($id);
        $model->feeName = $feeName;
        $model->amount = $amount;
        $model->status = $status;
        $model->save();
        if ($model) {

            if($status=="Paid") {
              Costs::addamount($model->appointmentId,$model->patientId,$amount);
            }
            else if($status=="Pending") {
                Costs::removeamount($model->appointmentId,$model->patientId,$amount);
            }

            Activities::saveLog("Edited fee [".$feeName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Fees::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Costs::removeamount($model->appointmentId,$model->patientId,$model->amount);
            Activities::saveLog("Deleted fee [".$id."]");
            return true;
        }
        return false;
    }

}
