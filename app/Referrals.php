<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Referrals extends Model
{
    protected $table = 'referrals';

    public static function getAll() {
      return Referrals::select('referrals.*','appointments.appointmentNo','users.firstName','users.lastName','referralhospitals.name as referralhospitalName')
            ->leftJoin('users','referrals.patientId','=','users.id')
            ->leftJoin('appointments','referrals.appointmentId','=','appointments.id')
            ->leftJoin('referralhospitals','referrals.referralHospitalId','=','referralhospitals.id')
            ->where('referrals.adminId',Auth::user()->adminId)
            ->whereRaw('Date(referrals.created_at) = CURDATE()')
            ->where('referrals.isDeleted',0)
            ->orderBy('referrals.id','DESC')
            ->get();
    }

    public static function countForHospital($referralHospitalId) {
      return Referrals::where('referralHospitalId',$referralHospitalId)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
    }

    public static function checkPatient($patientId,$appointmentId) {
      return Referrals::where('patientId',$patientId)->where('appointmentId',$appointmentId)->where('isDeleted',0)->first();
    }

    public static function referpatient($patientId,$referralHospitalId,$reason,$appointmentId) {
      $adminId	= Auth::user()->adminId;
      $created_by	= Auth::user()->id;
      $model = new Referrals;
      $model->patientId = $patientId;
      $model->referralHospitalId = $referralHospitalId;
      $model->reason = $reason;
      $model->appointmentId = $appointmentId;
      $model->adminId = $adminId;
      $model->created_by = $created_by;
      $model->save();
      if ($model)
      {
        Activities::saveLog("Referred patient [".$patientId."], hospital [".$referralHospitalId."]");
        return true;
      }
      return false;
    }

    public static function editreferral($id,$referralHospitalId,$reason) {
      $model = Referrals::find($id);
      $model->referralHospitalId = $referralHospitalId;
      $model->reason = $reason;
      $model->save();
      if ($model)
      {
          return true;
      }
      return false;
    }

    public static function deleteone($id)
    {
        $model = Referrals::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            return true;
        }
        return false;
    }

}
