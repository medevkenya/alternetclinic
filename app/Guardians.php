<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Guardians extends Model
{
    protected $table = 'guardians';

    public static function storeone($patientId,$name,$email,$phone,$location,$gender) {
      $adminId	= Auth::user()->adminId;
      $created_by	= Auth::user()->id;
      $model = new Guardians;
      $model->patientId = $patientId;
      $model->name = $name;
      $model->email = $email;
      $model->phone = $phone;
      $model->gender = $gender;
      $model->location = $location;
      $model->adminId = $adminId;
      $model->created_by = $created_by;
      $model->save();
      if ($model) {
          Activities::saveLog("Added guaradian [".$name." ".$email." - ".$phone."]");
          return true;
      }
      return false;
    }

    public static function updateone($id,$name,$email,$phone,$location,$gender) {
      $model = Guardians::find($id);
      $model->name = $name;
      $model->email = $email;
      $model->phone = $phone;
      $model->gender = $gender;
      $model->location = $location;
      $model->save();
      if ($model) {
          Activities::saveLog("Updated guaradian [".$name." ".$email." - ".$phone."]");
          return true;
      }
      return false;
    }

}
