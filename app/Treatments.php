<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Costs;
use App\Activities;

class Treatments extends Model
{
    protected $table = 'treatments';

    public static function storeone($patientId,$appointmentId) {
      $adminId	= Auth::user()->adminId;
      $created_by	= Auth::user()->id;

      $model = new Treatments;
      $model->patientId = $patientId;
      $model->appointmentId = $appointmentId;
      $model->adminId = $adminId;
      $model->created_by = $created_by;
      $model->save();
      if ($model)
      {
          return true;
      }
      return false;
    }

    public static function edittreatment($id, $bp,$bloodSugar,$temperature,$cholestrol,$giveMedicine,$treatmentCost) {
      $model = Treatments::find($id);
      $model->bp = $bp;
      $model->bloodSugar = $bloodSugar;
      $model->temperature = $temperature;
      $model->cholestrol = $cholestrol;
      $model->giveMedicine = $giveMedicine;
      $model->treatmentCost = $treatmentCost;
      $model->save();
      if ($model) {
        if($treatmentCost > 0) {
        Costs::saveone($id,$treatmentCost,"General Treatment");
      }
          Appointments::where('id',$model->appointmentId)->update(['giveMedicine'=>$giveMedicine]);
          Activities::saveLog("Edited treatment ".$id."");
          return true;
      }
      return false;
    }

    public static function editmedicines($id, $medicines,$medicinesCost) {
      $model = Treatments::find($id);
      $model->medicines = $medicines;
      $model->medicinesCost = $medicinesCost;
      $model->save();
      if ($model) {
        if($medicinesCost > 0) {
        Costs::saveone($id,$medicinesCost,"Medicines");
      }
          Activities::saveLog("Edited medicines ".$id."");
          return true;
      }
      return false;
    }

    public static function editprescription($id, $prescriptions,$prescriptionsCost) {
      $model = Treatments::find($id);
      $model->prescriptions = $prescriptions;
      $model->prescriptionsCost = $prescriptionsCost;
      $model->save();
      if ($model) {
        if($prescriptionsCost > 0) {
        Costs::saveone($id,$prescriptionsCost,"Prescriptions");
      }
          Activities::saveLog("Edited Prescription ".$id."");
          return true;
      }
      return false;
    }

    public static function editdiagnosis($id, $diagnosis,$diagnosisCost) {
      $model = Treatments::find($id);
      $model->diagnosis = $diagnosis;
      $model->diagnosisCost = $diagnosisCost;
      $model->save();
      if ($model) {
        if($diagnosisCost > 0) {
        Costs::saveone($id,$diagnosisCost,"Diagnosis");
      }
          Activities::saveLog("Edited diagnosis ".$id."");
          return true;
      }
      return false;
    }

    public static function editlabtest($id, $labtest,$labtestCost) {
      $model = Treatments::find($id);
      $model->labtest = $labtest;
      $model->labtestCost = $labtestCost;
      $model->save();
      if ($model) {
        if($labtestCost > 0) {
          Costs::saveone($id,$labtestCost,"Lab Tests");
        }
          Activities::saveLog("Edited labtest ".$id."");
          return true;
      }
      return false;
    }

}
