<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | View Appointments</title>
    @include('headerlink')
    @include('datatables')
    <link href="assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Appointments | <?php echo count($list); ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All appointments</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Appointments</div>
                                <div class="ibox-tools">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addappointment"><i class="fa fa-plus"></i> Create New</button>
                                </div>
                            </div>
                            <div class="ibox-body">

                              @if (count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                             @endforeach
                                         </ul>
                                     </div>
                                    @endif

                                    @if ($message = Session::get('error'))
                                         <div class="alert alert-danger">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if ($message = Session::get('success'))
                                         <div class="alert alert-success">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if (session('status0'))
                                    <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status0') }}
                                    </div>
                                    @endif

                                    @if (session('status1'))
                                    <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status1') }}
                                    </div>
                                    @endif

                      <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Doctor</th>
                              <th>Service</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th>Actions</th>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                              <th>No.</th>
                              <th>Doctor</th>
                              <th>Service</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th>Actions</th>
                          </tr>
                      </tfoot>
                      <tbody>
                        <?php foreach ($list as $app) {
                          ?>
                          <tr>
                            <td><?php echo $app->appointmentNo; ?></td>
                              <td><?php echo $app->firstName; ?> <?php echo $app->lastName; ?></td>
                              <td><?php echo $app->serviceName; ?></td>
                              <td><?php echo $app->appointmentDate; ?></td>
                              <td><?php echo $app->status; ?></td>
                              <td>
                            <a href="<?php $url = URL::to("/viewAppointment/".$app->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editappointment<?php echo $app->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleteappointment<?php echo $app->id; ?>"><i class="fa fa-trash"></i> Delete</button>

                          </td>
                          </tr>

                          <!-- Modal -->
                          <div class="modal fade text-left" id="modal-editappointment<?php echo $app->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              {!! Form::open(['url' => 'editappointment']) !!}
                            <div class="modal-content">
                              <div class="modal-header">
                              <h4 class="modal-title" id="myModalLabel1">Edit Appointment</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                              </div>
                              <div class="modal-body">
                              <div class="row">
                              <div class="col-xl-12 col-lg-12 col-md-12">
                                <input type="hidden" name="id" value="<?php echo $app->id; ?>" class="form-control" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Doctor</label>
                                <select class="form-control" name="doctorId" required>
                                  <option value="<?php echo $app->doctorId; ?>"><?php echo $app->firstName; ?> <?php echo $app->lastName; ?></option>
                                  <?php foreach ($doctors as $keyf) { ?>
                                    <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->firstName; ?> <?php echo $keyf->lastName; ?></option>
                                  <?php } ?>
                                </select>
                            </div>

                            <div class="col-sm-6 form-group">
                                <label>Service</label>
                                <select class="form-control" name="serviceId" required>
                                  <option value="<?php echo $app->serviceId; ?>"><?php echo $app->serviceName; ?></option>
                                  <?php foreach ($services as $keys) { ?>
                                    <option value="<?php echo $keys->id; ?>"><?php echo $keys->name; ?></option>
                                  <?php } ?>
                                </select>
                            </div>

                            <div class="col-sm-6 form-group">
                                <label>Appointment date</label>
                                <input class="form-control" type="date" name="appointmentDate" value="<?php echo $app->appointmentDate; ?>" required>
                            </div>

                            <div class="col-sm-6 form-group">
                                <label>Status</label>
                                <select class="form-control" name="status" required>
                                  <option value="<?php echo $app->status; ?>"><?php echo $app->status; ?></option>
                                  <option value="Pending">Pending</option>
                                  <option value="Done">Done</option>
                                  <option value="Cancelled">Cancelled</option>
                                </select>
                            </div>

                          </div>
                              </div>
                              <div class="modal-footer">
                              <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save Changes</button>
                              </div>
                            </div>
                            {!! Form::close() !!}
                            </div>
                          </div>

                          <!-- Modal -->
                          <div class="modal fade text-left" id="modal-deleteappointment<?php echo $app->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              {!! Form::open(['url' => 'deleteappointment']) !!}
                            <div class="modal-content">
                              <div class="modal-header">
                              <h4 class="modal-title" id="myModalLabel1">Delete Appointment</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                              </div>
                              <div class="modal-body">
                              <div class="row">
                              <div class="col-xl-6 col-lg-6 col-md-6">
                                <input type="hidden" name="id" value="<?php echo $app->id; ?>" class="form-control" required>
                            </div>
                            <h5 style="margin-left:2%;">Confirm that you want to delete this appointment</h5>
                          </div>
                              </div>
                              <div class="modal-footer">
                              <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Delete</button>
                              </div>
                            </div>
                            {!! Form::close() !!}
                            </div>
                          </div>

                        <?php } ?>
                      </tbody>
                  </table>

                            </div>
                          </div>
                        </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    <script src="assets/vendors/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="assets/vendors/jquery-knob/dist/jquery.knob.min.js" type="text/javascript"></script>
    <script src="assets/vendors/moment/min/moment.min.js" type="text/javascript"></script>
    @include('datatablesfooter')
  </body>

  </html>
