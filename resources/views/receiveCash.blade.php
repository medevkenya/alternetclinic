<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Receive Cash</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('headerlink')
    <script src="https://checkout.flutterwave.com/v3.js"></script>
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Receive Cash</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Initiate payments</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">
                <div class="row">
                  <div class="col-lg-6 col-md-6">
                      <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title"><img src="{{ URL::to('/') }}/images/Mpesa-logo.jpg" style="width: 20%;position:absolute;"/></div>
                            <!-- <div class="ibox-tools">
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addnotification"><i class="fa fa-plus"></i> Create New</button>
                            </div> -->
                        </div>
                          <div class="ibox-body">
                              <div class="tab-content">



                                      @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                           <ul>
                                               @foreach ($errors->all() as $error)
                                               <li>{{ $error }}</li>
                                               @endforeach
                                           </ul>
                                       </div>
                                      @endif

                                      @if ($message = Session::get('error'))
                                           <div class="alert alert-danger">
                                               {{ $message }}
                                           </div>
                                      @endif

                                      @if ($message = Session::get('success'))
                                           <div class="alert alert-success">
                                               {{ $message }}
                                           </div>
                                      @endif

                                      @if (session('status0'))
                                      <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      {{ session('status0') }}
                                      </div>
                                      @endif

                                      @if (session('status1'))
                                      <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      {{ session('status1') }}
                                      </div>
                                      @endif

                                      {!! Form::open(['url' => 'stkpush']) !!}
                                      {{ csrf_field() }}
                                          <div class="row">
                                            <p style="margin-left:2%;margin-top:2%;">The recipient will receive an M-PESA prompt to complete payment</p>
                                          <div class="col-sm-6 form-group">
                                              <label>Mobile No.</label>
                                              <input class="form-control" type="number" name="mobileNo" placeholder="Mobile No." required>
                                          </div>
                                          <div class="col-sm-6 form-group">
                                              <label>Amount</label>
                                              <input class="form-control" type="number" name="amount" placeholder="Enter amount" required>
                                          </div>
                                          </div>
                                          <div class="form-group">
                                              <button class="btn btn-primary" type="submit">Request Payment</button>
                                          </div>

                                      </form>

                              </div>
                          </div>
                      </div>
                  </div>
                  <?php $settings = \App\Settings::getDetails(); ?>
                    <div class="col-lg-6 col-md-6">
                        <div class="ibox">
                          <div class="ibox-head">
                              <div class="ibox-title"><img src="{{ URL::to('/') }}/images/creditcards.png" style="width: 40%;position:absolute;"/></div>
                              <!-- <div class="ibox-tools">
                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addnotification"><i class="fa fa-plus"></i> Create New</button>
                              </div> -->
                          </div>
                            <div class="ibox-body">
                                <div class="tab-content">

                                          <div class="alert alert-success alert-dismissible" id="successfulFlutterPayment" style="display:none;margin-top:4%;">
                                           <p>Success! You will get a confirmation email once its confirmed.</p>
                                          </div>

                                          <div class="alert alert-danger" role="alert" id="failedFlutterPayment" style="display:none;margin-top:4%;">
                                           <p>Failed! Your payment was not successful. Please check your details and try again</p>
                                          </div>

                                            <div class="row">
                                              <p style="margin-left:2%;margin-top:2%;">A pop will be displayed, follow the prompts to complete payment</p>
                                            <div class="col-sm-6 form-group">
                                                <label>Email Address</label>
                                                <input class="form-control" id="emailFlutterPayment" type="email" name="email" placeholder="Enter email address" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Mobile No.</label>
                                                <input class="form-control" id="phoneFlutterPayment" type="number" name="mobileNo" placeholder="Enter mobile no." required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Customer Name</label>
                                                <input class="form-control" id="nameFlutterPayment" type="text" name="name" placeholder="Name" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Amount</label>
                                                <input class="form-control" id="amountFlutterPayment" type="number" name="amount" placeholder="Enter amount" required>
                                            </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="button" onclick="initiatePayment()">Request Payment</button>
                                            </div>

                                        </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')

    <script>

  function initiatePayment()
  {
    var amount = document.getElementById("amountFlutterPayment").value;
    initiateFlutterPayment('mastercard',1,amount);

  }

  function initiateFlutterPayment(method,packageId,amount)
  {

			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      jQuery.ajax({
      type: 'POST',
			headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
      url:"{{ route('flutterwaveinitiate') }}",
      data: 'amount='+amount+'&packageId='+packageId,
      cache: false,
      success: function(response){
        //console.log('initiatePayment---'+JSON.stringify(response));
        if(response != null) {
          var results = JSON.parse(response);
          //console.log('initiatePayment parsed---'+JSON.stringify(results));
          //console.log('initiatePayment billRefNumber---'+results.billRefNumber);
          makePayment(amount,results.billRefNumber,results.public_key);
        }
        else {

        }

      }
      });

  }

  function makePayment(amount,billRefNumber,public_key)
  {

    var email = document.getElementById("emailFlutterPayment").value;
    var phone_number = document.getElementById("phoneFlutterPayment").value;
    var name = document.getElementById("nameFlutterPayment").value;

    var x = FlutterwaveCheckout({
      public_key: public_key,
      tx_ref: billRefNumber,//Generate a random id for the transaction reference
      amount: amount,
      currency: "KES",
      country: "KE",
      payment_options: "card, mpesa",
      customer: {
        email: email,
        phone_number: phone_number,
        name: name,
      },
      callback: function(response) {

          //console.log("This is the response returned after a charge", JSON.stringify(response));
          if (response.status == "successful")
          {
              var transaction_id = response.transaction_id; // collect txRef returned and pass to a server page to complete status check.
              var tx_ref = response.tx_ref;

              // redirect to a success page
              document.getElementById("successfulFlutterPayment").style.display = "block";
              document.getElementById("failedFlutterPayment").style.display = "none";
              document.getElementById("cardOptionsText").style.display = "none";

							$("html, body").animate({ scrollTop: 0 }, "slow");

              //verify transaction
              verifyFlutterTransaction(transaction_id,tx_ref);

          }
          else
          {
              // redirect to a failure page
              document.getElementById("failedFlutterPayment").style.display = "block";
              document.getElementById("successfulFlutterPayment").style.display = "none";
              document.getElementById("cardOptionsText").style.display = "none";

							$("html, body").animate({ scrollTop: 0 }, "slow");
          }

          x.close(); // use this to close the modal immediately after payment.
      },
      onclose: function() {
        // close modal
      },
      customizations: {
        title: "<?php echo $settings->companyName; ?>",
        description: "Receive Cash",
        logo: "<?php echo \App\User::getMainURL()."profiles/".$settings->companyPhoto; ?>",
      },
    });
  }

  function verifyFlutterTransaction(transaction_id,tx_ref)
  {

			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      jQuery.ajax({
      type: 'POST',
			headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
      url:"{{ route('flutterwaveverify') }}",
      data: 'transaction_id='+transaction_id+'&tx_ref='+tx_ref,
      cache: false,
      success: function(response){
        console.log('verifyFlutterTransaction---'+JSON.stringify(response));
        if(response != null) {
          var results = JSON.parse(response);
          //console.log('verifyFlutterTransaction parsed---'+JSON.stringify(results));
          //console.log('verifyFlutterTransaction ResultDesc---'+results.ResultDesc);

					$("html, body").animate({ scrollTop: 0 }, "slow");

          if (results.ResultCode == 0)
          {

              // redirect to a success page
              document.getElementById("successfulFlutterPayment").style.display = "block";
              document.getElementById("successfulFlutterPayment").innerHTML = results.ResultDesc;
              document.getElementById("successfulFlutterPayment").style.borderColor = "#FAB348";
              document.getElementById("failedFlutterPayment").style.display = "none";
              document.getElementById("cardOptionsText").style.display = "none";

							location.reload();
							return false;

          }

        }

      }
      });

  }
</script>

  </body>

  </html>
