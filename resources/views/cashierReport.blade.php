<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Cashier</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Cashier</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Costs and fees</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                        <div class="col-md-12">
                            <div class="ibox">
                                <div class="ibox-head">
                                    <div class="ibox-title">Costs
                                      <?php if(isset($fromdate)) { echo "From: ".$fromdate." To: ".$todate; } ?>
                                    </div>

                                </div>
                                <div class="ibox-body">

                                  @if (count($errors) > 0)
                                         <div class="alert alert-danger">
                                             <ul>
                                                 @foreach ($errors->all() as $error)
                                                 <li>{{ $error }}</li>
                                                 @endforeach
                                             </ul>
                                         </div>
                                        @endif

                                        @if ($message = Session::get('error'))
                                             <div class="alert alert-danger">
                                                 {{ $message }}
                                             </div>
                                        @endif

                                        @if ($message = Session::get('success'))
                                             <div class="alert alert-success">
                                                 {{ $message }}
                                             </div>
                                        @endif

                                        @if (session('status0'))
                                        <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ session('status0') }}
                                        </div>
                                        @endif

                                        @if (session('status1'))
                                        <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ session('status1') }}
                                        </div>
                                        @endif

                                        {!! Form::open(['url' => 'postcashierReport']) !!}
                                        {{ csrf_field() }}
                                        <div class="row">

                                        <div class="form-group col-md-3">
                                          <label>From Date</label>
                                          <input type="date" class="form-control" name="fromdate" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                          <label>To Date</label>
                                          <input type="date" class="form-control" name="todate" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                          <label>.</label>
                                          <button type="submit" class="btn btn-block btn-primary">Search</button>
                                        </div>
                                        </div>
                                        {!! Form::close() !!}


                          <?php $costs = \App\Costs::getAllForCashier(); ?>

                          <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>Appointment No.</th>
                                  <th>Treatment</th>
                                  <th>Prescription</th>
                                  <th>Diagnosis</th>
                                  <th>Lab Tests</th>
                                  <th>Medicines</th>
                                  <th>Other Fees</th>
                                  <th>Total</th>
                                  <th>Amount Paid</th>
                                  <th>Balance</th>
                                  <th>Date</th>
                                  <th>Status</th>
                                  <th>Actions</th>
                              </tr>
                          </thead>
                          <tfoot>
                              <tr>
                                <th>Appointment No.</th>
                                <th>Treatment</th>
                                <th>Prescription</th>
                                <th>Diagnosis</th>
                                <th>Lab Tests</th>
                                <th>Medicines</th>
                                <th>Other Fees</th>
                                <th>Total</th>
                                <th>Amount Paid</th>
                                <th>Balance</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Actions</th>
                              </tr>
                          </tfoot>
                          <tbody>
                            <?php $grandTotalCost = 0; foreach ($costs as $app) {
                              $otherFees = \App\Fees::totalForAppointment($app->appointmentId);
                              $totalCosts = $otherFees + $app->treatmentCost + $app->prescriptionsCost + $app->diagnosisCost + $app->labtestCost + $app->medicinesCost;
                              $balance = $totalCosts - $app->amountPaid;
                              $grandTotalCost += $totalCosts;
                              ?>
                              <tr>
                                <td><a href="<?php $url = URL::to("/viewAppointment/".$app->appointmentId); print_r($url); ?>" class="btn btn-primary"><?php echo $app->appointmentNo; ?></a></td>

                                  <td><?php echo $app->treatmentCost; ?></td>
                                  <td><?php echo $app->prescriptionsCost; ?></td>
                                  <td><?php echo $app->diagnosisCost; ?></td>
                                  <td><?php echo $app->labtestCost; ?></td>
                                  <td><?php echo $app->medicinesCost; ?></td>
                                  <td><?php echo $otherFees; ?></td>
                                  <td><?php echo $totalCosts; ?></td>
                                  <td><?php echo $app->amountPaid; ?></td>
                                  <td><?php echo $balance; ?></td>
                                  <td><?php echo $app->created_at; ?></td>
                                  <td><?php echo $app->status; ?></td>
                                  <td>
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editcost<?php echo $app->id; ?>"><i class="fa fa-edit"></i> Update</button>
                              </td>
                              </tr>

                              <!-- Modal -->
                              <div class="modal fade text-left" id="modal-editcost<?php echo $app->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  {!! Form::open(['url' => 'editcost']) !!}
                                <div class="modal-content">
                                  <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Edit Costs</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                                  <div class="modal-body">
                                  <div class="row">
                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                    <input type="hidden" name="id" value="<?php echo $app->id; ?>" class="form-control" required>
                                </div>

                                <div class="col-sm-6 form-group">
                                    <label>Amount Paid</label>
                                    <input class="form-control" type="number" name="amountPaid" value="<?php echo $app->amountPaid; ?>" required>
                                </div>

                                <div class="col-sm-6 form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status" required>
                                      <option value="<?php echo $app->status; ?>"><?php echo $app->status; ?></option>
                                      <option value="Pending">Pending</option>
                                      <option value="Paid">Paid</option>
                                    </select>
                                </div>

                              </div>
                                  </div>
                                  <div class="modal-footer">
                                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Save Changes</button>
                                  </div>
                                </div>
                                {!! Form::close() !!}
                                </div>
                              </div>

                            <?php } ?>
                          </tbody>
                      </table>
                      <p><b><?php echo "Total Ksh. ".number_format($grandTotalCost,0).""; ?></b></p>

                                </div>
                              </div>
                            </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
