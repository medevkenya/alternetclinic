<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Doctors</title>
    @include('headerlink')
    @include('datatables')
</head>

<?php
use Carbon\Carbon;
?>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Doctors</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Manage doctors</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Doctors List</div>
                                <div class="ibox-tools">
                                    <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                                    <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                    <!-- <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item">option 1</a>
                                        <a class="dropdown-item">option 2</a>
                                    </div> -->
                                </div>
                            </div>
                            <div class="ibox-body">
                              @if (count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                             @endforeach
                                         </ul>
                                     </div>
                                    @endif

                                    @if ($message = Session::get('error'))
                                         <div class="alert alert-danger">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if ($message = Session::get('success'))
                                         <div class="alert alert-success">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if (session('status0'))
                                    <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status0') }}
                                    </div>
                                    @endif

                                    @if (session('status1'))
                                    <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status1') }}
                                    </div>
                                    @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile No.</th>
                                    <th>Appointments</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Mobile No.</th>
                                  <th>Appointments</th>
                                  <th>Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                              <?php foreach ($list as $key) {
                                $appointments = \App\Appointments::countForDoctor($key->id);
                                ?>
                                <tr>
                                    <td><?php echo $key->firstName; ?> <?php echo $key->lastName; ?></td>
                                    <td><?php echo $key->email; ?></td>
                                    <td><?php echo $key->mobileNo; ?></td>
                                    <td><?php echo $appointments; ?></td>
                                    <td>
                                  <a href="<?php $url = URL::to("/viewDoctor/".$key->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>

                                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-opendelete<?php echo $key->id; ?>"><i class="fa fa-trash"></i> Delete</button>

                                </td>
                                </tr>

                                <!-- Modal -->
                                <div class="modal fade text-left" id="modal-opendelete<?php echo $key->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    {!! Form::open(['url' => 'deletedoctor']) !!}
                                  <div class="modal-content">
                                    <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel1">Delete Doctor</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                    <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-md-6">
                                      <input type="hidden" name="id" value="<?php echo $key->id; ?>" class="form-control" required>
                                  </div>
                                  <h5 style="margin-left:2%;">Confirm that you want to delete this doctor</h5>
                                </div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Delete</button>
                                    </div>
                                  </div>
                                  {!! Form::close() !!}
                                  </div>
                                </div>

                              <?php } ?>
                            </tbody>
                        </table>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
