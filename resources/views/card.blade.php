<link href="{{ URL::asset('assets/css/card.css')}}" rel="stylesheet">
<?php $settings = \App\Settings::getDetails(); ?>
<div class="cardcard" id="cardcard">
  <header>
    <time
  datetime="2018-05-15T19:00"><?php echo date("M d - Y", strtotime($details->created_at)); ?></time>
    <div class="logocard">
      <!-- <span>
        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 234.5 53.7"><style>.st0{fill:none;stroke:#FFFFFF;stroke-width:5;stroke-miterlimit:10;}</style><path d="M.6 1.4L116.9 52l117-50.6" class="st0"/></svg>
      </span> -->
      <img src="{{ URL::to('/') }}/public/profiles/<?php echo $settings->companyPhoto; ?>" width="50px" height="50px" style="border-radius:50%;" />
    </div>
    <div class="sponsor"><?php echo ucwords($details->mobileNo); ?></div>
  </header>
  <div class="announcement">
    <h3><?php echo ucwords($details->patientNo); ?></h3>
    <h1><?php echo strtoupper($details->firstName); ?> <?php echo strtoupper($details->middleName); ?> <?php echo strtoupper($details->lastName); ?></h1>
    <h3 class="italic"><?php echo $settings->companyName; ?></h3>
  </div>
</div>
