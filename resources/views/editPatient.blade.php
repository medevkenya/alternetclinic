<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Edit Patient</title>
    @include('headerlink')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Edit Patient</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Edit patient details</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Fill all details correctly</div>
                                <div class="ibox-tools">
                                    <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                                    <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                    <!-- <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item">option 1</a>
                                        <a class="dropdown-item">option 2</a>
                                    </div> -->
                                </div>
                            </div>
                            <div class="ibox-body">
                              @if (count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                             @endforeach
                                         </ul>
                                     </div>
                                    @endif

                                    @if ($message = Session::get('error'))
                                         <div class="alert alert-danger">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if ($message = Session::get('success'))
                                         <div class="alert alert-success">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if (session('status0'))
                                    <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status0') }}
                                    </div>
                                    @endif

                                    @if (session('status1'))
                                    <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status1') }}
                                    </div>
                                    @endif

                                   {!! Form::open(['url'=>'doeditpatient','id'=>'login-form']) !!}
                                   {{ csrf_field() }}
                                   <div class="row">
                                     <input class="form-control" value="<?php echo $details->id; ?>" type="hidden" name="id" required>
                                     <input class="form-control" value="<?php echo $gdetails->id; ?>" type="hidden" name="guardianId" required>
                                       <div class="col-sm-4 form-group">
                                           <label>First Name</label>
                                           <input class="form-control" value="<?php echo $details->firstName; ?>" type="text" placeholder="First Name" name="firstName" required>
                                       </div>
                                       <div class="col-sm-4 form-group">
                                           <label>Middle Name (optional)</label>
                                           <input class="form-control" type="text" value="<?php echo $details->middleName; ?>" placeholder="Middle Name" name="middleName">
                                       </div>
                                       <div class="col-sm-4 form-group">
                                           <label>Last Name</label>
                                           <input class="form-control" value="<?php echo $details->lastName; ?>" type="text" placeholder="Last Name" name="lastName" required>
                                       </div>
                                       <div class="col-sm-4 form-group">
                                           <label>Gender</label>
                                           <select class="form-control" name="gender" required>
                                             <option value="<?php echo $details->gender; ?>"><?php echo $details->gender; ?></option>
                                             <option value="Male">Male</option>
                                             <option value="Female">Female</option>
                                           </select>
                                       </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Email  (optional)</label>
                                       <input class="form-control" value="<?php echo $details->email; ?>" type="email" name="email" placeholder="Email address">
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Mobile No.  (optional)</label>
                                       <input class="form-control" value="<?php echo $details->mobileNo; ?>" type="number" name="mobileNo" placeholder="Mobile No.">
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Location</label>
                                       <input class="form-control" value="<?php echo $details->location; ?>" type="location" name="location" placeholder="Location">
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Date of Birth</label>
                                       <input class="form-control" value="<?php echo $details->dob; ?>" type="date" name="dob" placeholder="Date of Birth" required>
                                   </div>

                                 </div>
                                 <div class="row">

                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Name</label>
                                       <input class="form-control" value="<?php echo $gdetails->name; ?>" type="text" name="guardianName" placeholder="Name" required>
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Email  (optional)</label>
                                       <input class="form-control" value="<?php echo $gdetails->email; ?>" type="email" name="guardianEmail" placeholder="Email address">
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Telephone  (optional)</label>
                                       <input class="form-control" value="<?php echo $gdetails->phone; ?>" type="number" name="guardianPhone" placeholder="Telephone">
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Gender</label>
                                       <select class="form-control" name="guardianGender" required>
                                         <option value="<?php echo $gdetails->gender; ?>"><?php echo $gdetails->gender; ?></option>
                                         <option value="Male">Male</option>
                                         <option value="Female">Female</option>
                                       </select>
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Location</label>
                                       <input class="form-control" value="<?php echo $gdetails->location; ?>" type="text" name="guardianLocation" placeholder="Location" required>
                                   </div>

                                 </div>

                                   <div class="form-group">
                                       <button class="btn btn-primary" type="submit">Save Changes</button>
                                   </div>
                               </form>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
  </body>

  </html>
