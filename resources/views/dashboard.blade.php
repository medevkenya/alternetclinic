<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Dashboard</title>
    @include('headerlink')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-success color-white widget-stat">
                          <a href="{{URL::to('/patients')}}" class="color-white">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $countPatients; ?></h2>
                                <div class="m-b-5">PATIENTS</div><i class="ti-shopping-cart widget-stat-icon"></i>
                                <div><i class="fa fa-level-up m-r-5"></i><small>View More</small></div>
                            </div>
                          </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-info color-white widget-stat">
                          <a href="{{URL::to('/doctors')}}" class="color-white">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $countDoctors; ?></h2>
                                <div class="m-b-5">DOCTORS</div><i class="ti-bar-chart widget-stat-icon"></i>
                                <div><i class="fa fa-level-up m-r-5"></i><small>View More</small></div>
                            </div>
                          </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-warning color-white widget-stat">
                          <a href="{{URL::to('/admissions')}}" class="color-white">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $countAdmissions; ?></h2>
                                <div class="m-b-5">ADMISSIONS</div><i class="fa fa-money widget-stat-icon"></i>
                                <div><i class="fa fa-level-up m-r-5"></i><small>View More</small></div>
                            </div>
                          </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-danger color-white widget-stat">
                          <a href="{{URL::to('/appointments')}}" class="color-white">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $countAppointments; ?></h2>
                                <div class="m-b-5">APPOINTMENTS</div><i class="ti-user widget-stat-icon"></i>
                                <div><i class="fa fa-level-down m-r-5"></i><small>View More</small></div>
                            </div>
                          </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="ibox">
                            <div class="ibox-body">
                                <div class="flexbox mb-4">
                                    <div>
                                        <h3 class="m-0">Statistics</h3>
                                        <div>Your <?php echo  env("APP_NAME"); ?> sales analytics</div>
                                    </div>
                                    <div class="d-inline-flex">
                                        <div class="px-3" style="border-right: 1px solid rgba(0,0,0,.1);">
                                            <div class="text-muted">WEEKLY REVENUE</div>
                                            <div>
                                                <span class="h2 m-0">Ksh. <?php echo $thismonth; ?></span>
                                                <span class="text-success ml-2"><i class="fa fa-level-up"></i> <?php echo $weeksign."".$wpercentage; ?>%</span>
                                            </div>
                                        </div>
                                        <div class="px-3">
                                            <div class="text-muted">MONTHLY REVENUE</div>
                                            <div>
                                                <span class="h2 m-0">Ksh. <?php echo $thismonth; ?></span>
                                                <span class="text-warning ml-2"><i class="fa fa-level-down"></i> <?php echo $monthsign."".$mpercentage; ?>%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <canvas id="bar_chart" style="height:260px;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Statistics</div>
                            </div>
                            <div class="ibox-body">
                                <div class="row align-items-center">
                                    <div class="col-md-6">
                                        <canvas id="doughnut_chart" style="height:160px;"></canvas>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="m-b-20 text-success"><i class="fa fa-circle-o m-r-10"></i>Referrals (<?php echo $referrals; ?>)</div>
                                        <div class="m-b-20 text-success"><i class="fa fa-circle-o m-r-10"></i>Hospitals (<?php echo $hospitals; ?>)</div>
                                        <div class="m-b-20 text-success"><i class="fa fa-circle-o m-r-10"></i>Services (<?php echo $services; ?>)</div>
                                    </div>
                                </div>
                                <ul class="list-group list-group-divider list-group-full">
                                    <li class="list-group-item">Available Wards
                                        <span class="float-right text-success"><i class="fa fa-caret-up"></i> <?php echo $availablewards; ?></span>
                                    </li>
                                    <li class="list-group-item">Occupied Wards
                                        <span class="float-right text-success"><i class="fa fa-caret-up"></i> <?php echo $occupiedwards; ?></span>
                                    </li>
                                    <li class="list-group-item">Lab Tests
                                        <span class="float-right text-success"><i class="fa fa-caret-down"></i> <?php echo $labtests; ?></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-lg-8">
                      <div class="ibox">
                          <div class="ibox-head">
                              <div class="ibox-title">Latest Patients</div>
                              <div class="ibox-tools">
                                  <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                                  <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                  <div class="dropdown-menu dropdown-menu-right">
                                      <a class="dropdown-item">option 1</a>
                                      <a class="dropdown-item">option 2</a>
                                  </div>
                              </div>
                          </div>
                          <div class="ibox-body">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th>Patient No.</th>
                                          <th>Name</th>
                                          <th>Mobile No.</th>
                                          <th>Appointment No.</th>
                                          <th width="91px">Date</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <?php $patients = \App\Patients::latestPatients();
                                    foreach ($patients as $key) {
                                      ?>
                                      <tr>
                                          <td>
                                              <a href="<?php $url = URL::to("/viewPatient/".$key->patientNo); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> <?php echo $key->patientNo; ?></a>
                                          </td>
                                          <td><?php echo $key->firstName; ?> <?php echo $key->lastName; ?></td>
                                          <td><?php echo $key->mobileNo; ?></td>
                                          <td>
                                              <a href="<?php $url = URL::to("/viewAppointment/".$key->appointmentNo); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> <?php echo $key->appointmentNo; ?></a>
                                          </td>
                                          <td><?php echo $key->created_at; ?></td>
                                      </tr>
                                    <?php } ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
                    <div class="col-lg-4">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Notifications</div>
                            </div>
                            <div class="ibox-body">
                                <ul class="media-list media-list-divider m-0">
                                  <?php $notifications = \App\Notifications::getLatest(); foreach ($notifications as $keyn) { ?>
                                    <li class="media">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="{{ URL::to('/') }}/profiles/<?php echo $keyn->profilePic; ?>" width="40" />
                                        </a>
                                        <div class="media-body">
                                            <div class="media-heading"><?php echo $keyn->firstName; ?> <?php echo $keyn->lastName; ?><small class="float-right text-muted"><?php echo $keyn->created_at; ?></small></div>
                                            <div class="font-13"><?php echo $keyn->message; ?></div>
                                        </div>
                                    </li>
                                  <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
</body>

</html>
