<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Recover Password</title>
    @include('headerlinks')
</head>

<body class="bg-silver-300">
    <div class="content">
        <div class="brand">
            <a class="link" href="#">
<img src="{{ URL::to('/') }}/images/logo.png" width="80%"/>
            </a>
        </div>

          @if (count($errors) > 0)
                 <div class="alert alert-danger">
                     <ul>
                         @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
                @endif

                @if ($message = Session::get('error'))
                     <div class="alert alert-danger">
                         {{ $message }}
                     </div>
                @endif

                @if ($message = Session::get('success'))
                     <div class="alert alert-success">
                         {{ $message }}
                     </div>
                @endif

               {!! Form::open(['url'=>'resetPassword','id'=>'login-form']) !!}
               {{ csrf_field() }}
            <h2 class="login-title">Recover Password</h2>
            <div class="form-group">
                <div class="input-group-icon right">
                    <div class="input-icon"><i class="fa fa-envelope"></i></div>
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Email" autocomplete="off">
                    @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                      @endif
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block" type="submit">Recover Password</button>
            </div>
            <div class="social-auth-hr">
                <span>Connect with us</span>
            </div>
            <div class="text-center social-auth m-b-20">
                <a class="btn btn-social-icon btn-twitter m-r-5" href="https://twitter.com/Alternetlimited" target="_blank"><i class="fa fa-twitter"></i></a>
                <a class="btn btn-social-icon btn-facebook m-r-5" href="https://facebook.com/Alternetlimited" target="_blank"><i class="fa fa-facebook"></i></a>
                <a class="btn btn-social-icon btn-google m-r-5" href="https://youtube.com/Alternetlimited" target="_blank"><i class="fa fa-youtube"></i></a>
                <a class="btn btn-social-icon btn-linkedin m-r-5" href="https://linkedin.com/Alternetlimited" target="_blank"><i class="fa fa-linkedin"></i></a>
            </div>
            <div class="text-center">If you have your password
                <a class="color-blue" href="{{URL::to('/')}}">Sign In</a>
            </div>
        </form>
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    @include('footerlinks')
</body>

</html>
