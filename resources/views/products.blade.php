<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Products</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Products</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Products</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Products</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                              <form action="{{ route('addproduct') }}" method="post" enctype="multipart/form-data">
                                              {{ csrf_field() }}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Product</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                                  <?php
                                                  $categories = \App\Categories::getAll();
                                                  $colors = \App\Colors::getAll();
                                                  $sizes = \App\Sizes::getAll();
                                                  ?>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Category</label>
                                                      <select class="form-control" name="categoryId" id="categoryId" required>
                                                     <option></option>
                                                     <?php foreach ($categories as $keyf) { ?>
                                                       <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Brand / Sub Category</label>
                                                      <select class="form-control" name="brandId" id="brandId" required>
                                                   </select>
                                                  </div>

                                                </div>
                                                <div class="row">

                                                  <div class="col-sm-6 form-group">
                                                      <label>Color</label>
                                                      <select class="form-control" name="colorId">
                                                     <option></option>
                                                     <?php foreach ($colors as $keyl) { ?>
                                                       <option value="<?php echo $keyl->id; ?>"><?php echo $keyl->colorName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Size</label>
                                                      <select class="form-control" name="sizeId">
                                                     <option></option>
                                                     <?php foreach ($sizes as $keyz) { ?>
                                                       <option value="<?php echo $keyz->id; ?>"><?php echo $keyz->sizeName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>
                                                </div>
                                                <div class="row">

                                              <div class="col-sm-12 form-group">
                                                  <label>Product Name</label>
                                                  <input class="form-control" type="text" name="productName" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                  <label>Cost (Ksh.)</label>
                                                  <input class="form-control" type="number" name="cost" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                  <label>Description</label>
                                                  <textarea class="form-control" name="description" required></textarea>
                                              </div>

                                            </div>
                                            <div class="row">
                                              <div class="col-sm-6 form-group">
                                                  <label>Photo</label>
                                              <input type="file" name="photoUrl" placeholder="Choose photo" id="photoUrl">
                                            </div>
                                            <div class="col-sm-6 form-group">
                                              <img id="preview-image-before-upload" src="{{ URL::asset('photos/medicine.png')}}" alt="preview image" style="max-height: 150px;">
                                          </div>
                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @error('photoUrl')
                                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                            @enderror

                                            @if (count($errors) > 0)
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                     <li>{{ $error }}</li>
                                                     @endforeach
                                                 </ul>
                                             </div>
                                            @endif

                                            @if ($message = Session::get('error'))
                                                 <div class="alert alert-danger">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if ($message = Session::get('success'))
                                                 <div class="alert alert-success">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if (session('status0'))
                                            <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status0') }}
                                            </div>
                                            @endif

                                            @if (session('status1'))
                                            <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status1') }}
                                            </div>
                                            @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                          <th width="12%">Photo</th>
                                            <th>Product Name</th>
                                            <th>Cost (Ksh.)</th>
                                            <th>Category</th>
                                            <th>Size</th>
                                            <th>Color</th>
                                            <th>Description</th>
                                            <th>Qty</th>
                                            <th width="14%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th width="12%">Photo</th>
                                          <th>Product Name</th>
                                          <th>Cost (Ksh.)</th>
                                          <th>Category</th>
                                          <th>Size</th>
                                          <th>Color</th>
                                          <th>Description</th>
                                          <th>Qty</th>
                                          <th width="14%">Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $product) {
                                        $quantity = \App\Batches::getProductQuantity($product->id);
                                        ?>
                                        <tr>
                                          <td><img src="{{ URL::to('/') }}/public/photos/<?php echo $product->photoUrl; ?>" alt=""></td>
                                          <td><?php echo $product->productName; ?></td>
                                          <td><?php echo $product->cost; ?></td>
                                          <td><?php echo $product->categoryName; ?> - <?php echo $product->brandName; ?></td>
                                          <td><?php echo $product->sizeName; ?></td>
                                          <td><?php echo $product->colorName; ?></td>
                                          <td><?php echo $product->description; ?></td>
                                          <td><?php echo $quantity; ?></td>
                                            <td>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php echo $product->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $product->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $product->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <form action="{{ route('editproduct') }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Product</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">

                                              <input type="hidden" name="id" value="<?php echo $product->id; ?>" class="form-control" required>

                                              <div class="row">

                                                <?php
                                                $categories = \App\Categories::getAll();
                                                $colors = \App\Colors::getAll();
                                                $sizes = \App\Sizes::getAll();
                                                ?>

                                                <div class="col-sm-6 form-group">
                                                    <label>Category</label>
                                                    <select class="form-control" name="categoryId" id="categoryIde" required>
                                                   <option value="<?php echo $product->categoryId; ?>"><?php echo $product->categoryName; ?></option>
                                                   <?php foreach ($categories as $keyf) { ?>
                                                     <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                   <?php } ?>
                                                 </select>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label>Brand / Sub Category</label>
                                                    <select class="form-control" name="brandId" id="brandIde" required>
                                                      <option value="<?php echo $product->brandId; ?>"><?php echo $product->brandName; ?></option>
                                                 </select>
                                                </div>

                                              </div>
                                              <div class="row">

                                                <div class="col-sm-6 form-group">
                                                    <label>Color</label>
                                                    <select class="form-control" name="colorId">
                                                   <option value="<?php echo $product->colorId; ?>"><?php echo $product->colorName; ?></option>
                                                   <?php foreach ($colors as $keyl) { ?>
                                                     <option value="<?php echo $keyl->id; ?>"><?php echo $keyl->colorName; ?></option>
                                                   <?php } ?>
                                                 </select>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label>Size</label>
                                                    <select class="form-control" name="sizeId">
                                                   <option value="<?php echo $product->sizeId; ?>"><?php echo $product->sizeName; ?></option>
                                                   <?php foreach ($sizes as $keyz) { ?>
                                                     <option value="<?php echo $keyz->id; ?>"><?php echo $keyz->sizeName; ?></option>
                                                   <?php } ?>
                                                 </select>
                                                </div>
                                              </div>
                                              <div class="row">

                                              <div class="col-sm-12 form-group">
                                                <label>Product Name</label>
                                                <input class="form-control" type="text" name="productName" value="<?php echo $product->productName; ?>" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                <label>Cost (Ksh.)</label>
                                                <input class="form-control" type="number" name="cost" value="<?php echo $product->cost; ?>" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                <label>Description</label>
                                                <textarea class="form-control" name="description" required><?php echo $product->description; ?></textarea>
                                              </div>

                                              </div>
                                              <div class="row">
                                              <div class="col-sm-6 form-group">
                                                <label>Photo</label>
                                              <input type="file" name="photoUrl" placeholder="Choose photo" id="photoUrl">
                                              </div>
                                              <div class="col-sm-6 form-group">
                                              <img id="preview-image-before-upload" src="{{ URL::to('/') }}/public/photos/<?php echo $product->photoUrl; ?>" alt="preview image" style="max-height: 150px;">
                                              </div>
                                              </div>

                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $product->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deleteproduct']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Product</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $product->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this product</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
