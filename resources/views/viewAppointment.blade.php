<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | View Patient Appointment</title>
    @include('headerlink')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Patient | <?php echo strtoupper($patientDetails->firstName); ?> <?php echo strtoupper($patientDetails->middleName); ?> <?php echo strtoupper($patientDetails->lastName); ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Location | <?php echo ucwords($patientDetails->location); ?></li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Appointment details</div>
                                <div class="ibox-tools">
                                    <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                                    <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                    <!-- <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item">option 1</a>
                                        <a class="dropdown-item">option 2</a>
                                    </div> -->
                                </div>
                            </div>
                            <div class="ibox-body">
                              @if (count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                             @endforeach
                                         </ul>
                                     </div>
                                    @endif

                                    @if ($message = Session::get('error'))
                                         <div class="alert alert-danger">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if ($message = Session::get('success'))
                                         <div class="alert alert-success">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if (session('status0'))
                                    <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status0') }}
                                    </div>
                                    @endif

                                    @if (session('status1'))
                                    <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status1') }}
                                    </div>
                                    @endif

                                   <div class="row">
                                   <div class="col-sm-3 form-group">
                                       <label>Appointment No.</label>
                                       <input class="form-control" value="<?php echo $details->appointmentNo; ?>" type="text" readonly>
                                   </div>
                                   <div class="col-sm-3 form-group">
                                       <label>Appointment Date</label>
                                       <input class="form-control" value="<?php echo $details->appointmentDate; ?>" type="text" readonly>
                                   </div>

                                   <div class="col-sm-3 form-group">
                                       <label>Doctor</label>
                                       <input class="form-control" value="<?php echo $details->firstName; ?> <?php echo $details->lastName; ?>" type="text" readonly>
                                   </div>

                                   <div class="col-sm-3 form-group">
                                       <label>Service</label>
                                       <input class="form-control" value="<?php echo $details->serviceName; ?>" type="text" readonly>
                                   </div>

                                 </div>

                                 <?php $admitted = \App\Admissions::checkPatient($patientDetails->id,$details->id); ?>
                                 <?php $referred = \App\Referrals::checkPatient($patientDetails->id,$details->id); ?>

                                 <div class="form-group">
                                     <a href="<?php $url = URL::to("/editPatient/".$patientDetails->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Edit Patient</a>
                                     <?php if(!$admitted) { ?>
                                     <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-admit"><i class="fa fa-bed"></i> Admit Patient</button>
                                   <?php } else { if($admitted->dischargeDate == null) { ?>
                                     <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-discharge"><i class="fa fa-bed"></i> Discharge Patient</button>
                                   <?php } } ?>
                                   <?php if(!$referred) { ?>
                                   <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-refer"><i class="fa fa-hospital"></i> Refer Patient</button>
                                 <?php } ?>

                                 </div>

                                 <!-- Modal -->
                                 <div class="modal fade text-left" id="modal-refer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                   <div class="modal-lg modal-dialog" role="document">
                                     {!! Form::open(['url' => 'referpatient']) !!}
                                   <div class="modal-content">
                                     <div class="modal-header">
                                     <h4 class="modal-title" id="myModalLabel1">Refer this patient</h4>
                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                     </button>
                                     </div>
                                     <div class="modal-body">
                                     <div class="row">
                                     <div class="col-xl-12 col-lg-12 col-md-12">
                                       <input type="hidden" name="appointmentId" value="<?php echo $details->id; ?>" class="form-control" required>
                                       <input type="hidden" name="patientId" value="<?php echo $patientDetails->id; ?>" class="form-control" required>
                                   </div>
                                   <div class="col-sm-6 form-group">
                                       <label>Referral Hospital</label>
                                       <select class="form-control" name="referralHospitalId" required>
                                         <option value=""></option>
                                         <?php $hospitals = \App\Referralhospitals::getAll(); foreach ($hospitals as $hosp) { ?>
                                           <option value="<?php echo $hosp->id; ?>"><?php echo $hosp->name; ?></option>
                                         <?php } ?>
                                       </select>
                                   </div>
                                   <div class="col-sm-12 form-group">
                                       <textarea row="6" style="min-height:100px;" class="form-control" placeholder="Referral reason..." name="reason" required></textarea>
                                   </div>
                                 </div>
                                     </div>
                                     <div class="modal-footer">
                                     <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                     <button type="submit" class="btn btn-primary">Submit</button>
                                     </div>
                                   </div>
                                   {!! Form::close() !!}
                                   </div>
                                 </div>

                                 <!-- Modal -->
                                 <div class="modal fade text-left" id="modal-admit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                   <div class="modal-lg modal-dialog" role="document">
                                     {!! Form::open(['url' => 'admitpatient']) !!}
                                   <div class="modal-content">
                                     <div class="modal-header">
                                     <h4 class="modal-title" id="myModalLabel1">Admit this patient</h4>
                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                     </button>
                                     </div>
                                     <div class="modal-body">
                                     <div class="row">
                                     <div class="col-xl-12 col-lg-12 col-md-12">
                                       <input type="hidden" name="appointmentId" value="<?php echo $details->id; ?>" class="form-control" required>
                                       <input type="hidden" name="patientId" value="<?php echo $patientDetails->id; ?>" class="form-control" required>
                                   </div>
                                   <div class="col-sm-6 form-group">
                                       <label>Ward</label>
                                       <select class="form-control" name="wardId" required>
                                         <option value=""></option>
                                         <?php $wards = \App\Wards::getAll(); foreach ($wards as $wd) { ?>
                                           <option value="<?php echo $wd->id; ?>"><?php echo $wd->wardName; ?></option>
                                         <?php } ?>
                                       </select>
                                   </div>
                                   <div class="col-sm-6 form-group">
                                       <label>Bed No.</label>
                                       <input class="form-control" type="text" name="bedNo" required>
                                   </div>
                                   <div class="col-sm-12 form-group">
                                       <textarea row="6" style="min-height:100px;" class="form-control" placeholder="Admission reason..." name="admitReason" required></textarea>
                                   </div>
                                 </div>
                                     </div>
                                     <div class="modal-footer">
                                     <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                     <button type="submit" class="btn btn-primary">Submit</button>
                                     </div>
                                   </div>
                                   {!! Form::close() !!}
                                   </div>
                                 </div>

                                 <!-- Modal -->
                                 <div class="modal fade text-left" id="modal-discharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                   <div class="modal-lg modal-dialog" role="document">
                                     {!! Form::open(['url' => 'dischargepatient']) !!}
                                   <div class="modal-content">
                                     <div class="modal-header">
                                     <h4 class="modal-title" id="myModalLabel1">Discharge this patient</h4>
                                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                     </button>
                                     </div>
                                     <div class="modal-body">
                                     <div class="row">
                                     <div class="col-xl-12 col-lg-12 col-md-12">
                                       <input type="hidden" name="appointmentId" value="<?php echo $details->id; ?>" class="form-control" required>
                                       <input type="hidden" name="patientId" value="<?php echo $patientDetails->id; ?>" class="form-control" required>
                                   </div>
                                   <div class="col-sm-12 form-group">
                                       <textarea row="6" style="min-height:100px;" class="form-control" placeholder="Discharge reason..." name="dischargeReason" required></textarea>
                                   </div>
                                 </div>
                                     </div>
                                     <div class="modal-footer">
                                     <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                     <button type="submit" class="btn btn-primary">Submit</button>
                                     </div>
                                   </div>
                                   {!! Form::close() !!}
                                   </div>
                                 </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Bp, Cholestrol, Blood Sugar, Temperature</div>
                                <div class="ibox-tools">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-edittreatment"><i class="fa fa-edit"></i> Update</button>
                                </div>
                            </div>
                            <div class="ibox-body">
                              <!-- Modal -->
                              <div class="modal fade text-left" id="modal-edittreatment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  {!! Form::open(['url' => 'edittreatment']) !!}
                                <div class="modal-content">
                                  <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Update Treatment</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                                  <div class="modal-body">
                                  <div class="row">
                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                    <input type="hidden" name="id" value="<?php echo $treatmentdetails->id; ?>" class="form-control" required>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>Temperature</label>
                                    <input class="form-control" value="<?php echo $treatmentdetails->temperature; ?>" type="number" name="temperature" placeholder="Temperature" required>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>Blood Pressure</label>
                                    <input class="form-control" value="<?php echo $treatmentdetails->bp; ?>" type="text" name="bp" placeholder="Blood Pressure" required>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>Cholestrol</label>
                                    <input class="form-control" value="<?php echo $treatmentdetails->cholestrol; ?>" type="text" name="cholestrol" placeholder="Cholestrol" required>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>Blood Sugar</label>
                                    <input class="form-control" value="<?php echo $treatmentdetails->bloodSugar; ?>" type="text" name="bloodSugar" placeholder="Blood Sugar" required>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>Give Medicine</label>
                                    <select class="form-control" name="giveMedicine" required>
                                      <option value=""></option>
                                      <option value="Yes">Yes</option>
                                      <option value="No">No</option>
                                    </select>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>Costs</label>
                                    <input class="form-control" type="number" name="treatmentCost" value="<?php echo $treatmentdetails->treatmentCost; ?>" placeholder="Costs Incurred..." required>
                                </div>
                              </div>
                                  </div>
                                  <div class="modal-footer">
                                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Update</button>
                                  </div>
                                </div>
                                {!! Form::close() !!}
                                </div>
                              </div>
                              <table class="table table-striped table-bordered table-hover" cellspacing="0" width="50%">
                                <thead>
                                <tr>
                                  <th>Item</th><th>Value</th>
                                </tr>
                              </thead>
                                <tr>
                                  <td>Temperature</td><td><?php echo $treatmentdetails->temperature; ?></td>
                                </tr>
                                <tr>
                                  <td>Blood Sugar</td><td><?php echo $treatmentdetails->bloodSugar; ?></td>
                                </tr>
                                <tr>
                                  <td>Blood Pressure</td><td><?php echo $treatmentdetails->bp; ?></td>
                                </tr>
                                <tr>
                                  <td>Cholestrol</td><td><?php echo $treatmentdetails->cholestrol; ?></td>
                                </tr>
                                <tr>
                                  <td>Give Medicine</td><td><?php echo $treatmentdetails->giveMedicine; ?></td>
                                </tr>
                                <tr>
                                  <td>Medicine</td><td><?php echo $treatmentdetails->medicines; ?></td>
                                </tr>
                                <tr>
                                  <td>Cost</td><td>Ksh. <?php echo $treatmentdetails->treatmentCost; ?></td>
                                </tr>
                              </table>
                            </div>
                          </div>
                        </div>

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Prescription</div>
                                <div class="ibox-tools">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-editprescription"><i class="fa fa-edit"></i> Update</button>
                                </div>
                            </div>
                            <div class="ibox-body">
                              <!-- Modal -->
                              <div class="modal fade text-left" id="modal-editprescription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                <div class="modal-lg modal-dialog" role="document">
                                  {!! Form::open(['url' => 'editprescription']) !!}
                                <div class="modal-content">
                                  <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Update Prescription</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                                  <div class="modal-body">
                                  <div class="row">
                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                    <input type="hidden" name="id" value="<?php echo $treatmentdetails->id; ?>" class="form-control" required>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>Costs</label>
                                    <input class="form-control" type="number" name="prescriptionsCost" value="<?php echo $treatmentdetails->prescriptionsCost; ?>" placeholder="Costs Incurred..." required>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <textarea row="6" style="min-height:300px;" class="form-control" name="prescriptions" required><?php echo $treatmentdetails->prescriptions; ?></textarea>
                                </div>
                              </div>
                                  </div>
                                  <div class="modal-footer">
                                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Update</button>
                                  </div>
                                </div>
                                {!! Form::close() !!}
                                </div>
                              </div>
                              <p>Ksh. <?php echo $treatmentdetails->prescriptionsCost; ?></p>
                              <?php echo $treatmentdetails->prescriptions; ?>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                            <div class="ibox">
                                <div class="ibox-head">
                                    <div class="ibox-title">Diagnosis</div>
                                    <div class="ibox-tools">
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-editdiagnosis"><i class="fa fa-edit"></i> Update</button>
                                    </div>
                                </div>
                                <div class="ibox-body">
                                  <!-- Modal -->
                                  <div class="modal fade text-left" id="modal-editdiagnosis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                    <div class="modal-lg modal-dialog" role="document">
                                      {!! Form::open(['url' => 'editdiagnosis']) !!}
                                    <div class="modal-content">
                                      <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel1">Update Diagnosis</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                      </div>
                                      <div class="modal-body">
                                      <div class="row">
                                      <div class="col-xl-12 col-lg-12 col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $treatmentdetails->id; ?>" class="form-control" required>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <label>Costs</label>
                                        <input class="form-control" type="number" name="diagnosisCost" value="<?php echo $treatmentdetails->diagnosisCost; ?>" placeholder="Costs Incurred..." required>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <textarea row="6" style="min-height:300px;" class="form-control" name="diagnosis" required><?php echo $treatmentdetails->diagnosis; ?></textarea>
                                    </div>
                                  </div>
                                      </div>
                                      <div class="modal-footer">
                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Update</button>
                                      </div>
                                    </div>
                                    {!! Form::close() !!}
                                    </div>
                                  </div>
                                  <p>Ksh. <?php echo $treatmentdetails->diagnosisCost; ?></p>
                                  <?php echo $treatmentdetails->diagnosis; ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-12">
                                <div class="ibox">
                                    <div class="ibox-head">
                                        <div class="ibox-title">Lab Tests</div>
                                        <div class="ibox-tools">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-editlabtest"><i class="fa fa-edit"></i> Update</button>
                                        </div>
                                    </div>
                                    <div class="ibox-body">
                                      <!-- Modal -->
                                      <div class="modal fade text-left" id="modal-editlabtest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                        <div class="modal-lg modal-dialog" role="document">
                                          {!! Form::open(['url' => 'editlabtest']) !!}
                                        <div class="modal-content">
                                          <div class="modal-header">
                                          <h4 class="modal-title" id="myModalLabel1">Update lab test</h4>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                          </div>
                                          <div class="modal-body">
                                          <div class="row">
                                          <div class="col-xl-12 col-lg-12 col-md-12">
                                            <input type="hidden" name="id" value="<?php echo $treatmentdetails->id; ?>" class="form-control" required>
                                        </div>
                                        <div class="col-sm-12 form-group">
                                            <label>Costs</label>
                                            <input class="form-control" type="number" name="labtestCost" value="<?php echo $treatmentdetails->labtestCost; ?>" placeholder="Costs Incurred..." required>
                                        </div>
                                        <div class="col-sm-12 form-group">
                                            <textarea row="6" style="min-height:300px;" class="form-control" name="labtest" required><?php echo $treatmentdetails->labtest; ?></textarea>
                                        </div>
                                      </div>
                                          </div>
                                          <div class="modal-footer">
                                          <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                          <button type="submit" class="btn btn-primary">Update</button>
                                          </div>
                                        </div>
                                        {!! Form::close() !!}
                                        </div>
                                      </div>
                                      <p>Ksh. <?php echo $treatmentdetails->labtestCost; ?></p>
                                      <?php echo $treatmentdetails->labtest; ?>
                                    </div>
                                  </div>
                                </div>

                                <?php if($treatmentdetails->giveMedicine == "Yes") { ?>
                                <div class="col-md-12">
                                    <div class="ibox">
                                        <div class="ibox-head">
                                            <div class="ibox-title">Medicines</div>
                                            <div class="ibox-tools">
                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-editmedicines"><i class="fa fa-edit"></i> Update</button>
                                            </div>
                                        </div>
                                        <div class="ibox-body">
                                          <!-- Modal -->
                                          <div class="modal fade text-left" id="modal-editmedicines" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                            <div class="modal-lg modal-dialog" role="document">
                                              {!! Form::open(['url' => 'editmedicines']) !!}
                                            <div class="modal-content">
                                              <div class="modal-header">
                                              <h4 class="modal-title" id="myModalLabel1">Update Medicines</h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                              </div>
                                              <div class="modal-body">
                                              <div class="row">
                                              <div class="col-xl-12 col-lg-12 col-md-12">
                                                <input type="hidden" name="id" value="<?php echo $treatmentdetails->id; ?>" class="form-control" required>
                                            </div>
                                            <div class="col-sm-12 form-group">
                                                <label>Costs</label>
                                                <input class="form-control" type="number" name="medicinesCost" value="<?php echo $treatmentdetails->medicinesCost; ?>" placeholder="Costs Incurred..." required>
                                            </div>
                                            <div class="col-sm-12 form-group">
                                                <textarea row="6" style="min-height:300px;" class="form-control" name="medicines" required><?php echo $treatmentdetails->medicines; ?></textarea>
                                            </div>
                                          </div>
                                              </div>
                                              <div class="modal-footer">
                                              <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                              <button type="submit" class="btn btn-primary">Update</button>
                                              </div>
                                            </div>
                                            {!! Form::close() !!}
                                            </div>
                                          </div>
                                          <p>Ksh. <?php echo $treatmentdetails->medicinesCost; ?></p>
                                          <?php echo $treatmentdetails->medicines; ?>
                                        </div>
                                      </div>
                                    </div>
                                  <?php } ?>

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Fees</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addfee"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addfee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addfee']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Fee</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">
                                                <div class="col-xl-12 col-lg-12 col-md-12">
                                                  <input type="hidden" name="patientId" value="<?php echo $patientDetails->id; ?>" class="form-control" required>
                                                  <input type="hidden" name="appointmentId" value="<?php echo $details->id; ?>" class="form-control" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                  <label>Purpose</label>
                                                  <input class="form-control" type="text" name="feeName" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                  <label>Amount</label>
                                                  <input class="form-control" type="number" name="amount" required>
                                              </div>

                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                    <?php $fees = \App\Fees::getAllForAppointment($details->id); ?>

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Purpose</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Purpose</th>
                                          <th>Amount</th>
                                          <th>Date</th>
                                          <th>Status</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($fees as $fee) {
                                        ?>
                                        <tr>
                                          <td><?php echo $fee->feeName; ?></td>
                                            <td><?php echo $fee->amount; ?></td>
                                            <td><?php echo $fee->created_at; ?></td>
                                            <td><?php echo $fee->status; ?></td>
                                            <td>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editfee<?php echo $fee->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deletefee<?php echo $fee->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editfee<?php echo $fee->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editfee']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Fee</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $fee->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Purpose</label>
                                              <input class="form-control" type="text" name="feeName" value="<?php echo $fee->feeName; ?>" required>
                                          </div>
                                          <div class="col-sm-12 form-group">
                                              <label>Amount</label>
                                              <input class="form-control" type="number" name="amount" value="<?php echo $fee->amount; ?>" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Status</label>
                                              <select class="form-control" name="status" required>
                                                <option value="<?php echo $fee->status; ?>"><?php echo $fee->status; ?></option>
                                                <option value="Pending">Pending</option>
                                                <option value="Paid">Paid</option>
                                              </select>
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deletefee<?php echo $fee->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletefee']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Fee</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $fee->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this fee</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-12">
              <div class="ibox">
                  <div class="ibox-head">
                      <div class="ibox-title">Admissions
                      </div>

                  </div>
                  <div class="ibox-body">

            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th>Ward</th>
                  <th>Bed No.</th>
                  <th>Admission Date</th>
                  <th>Admission Reason</th>
                  <th>Discharge Date</th>
                  <th>Discharge Reason</th>
                  <th>Date</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                  <th>Ward</th>
                  <th>Bed No.</th>
                  <th>Admission Date</th>
                  <th>Admission Reason</th>
                  <th>Discharge Date</th>
                  <th>Discharge Reason</th>
                  <th>Date</th>
                </tr>
            </tfoot>
            <tbody>
              <?php $admissions = \App\Admissions::getForAppointment($details->id); foreach ($admissions as $admi) {
                ?>
                <tr>
                    <td>
                      <?php if($admi->dischargeDate==null) { ?>
                      <?php echo $admi->wardName; ?><br><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-discharge<?php echo $admi->id; ?>"><i class="fa fa-bed"></i> Discharge Patient</button>
                    <?php } else { ?>
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-edit<?php echo $admi->id; ?>"><i class="fa fa-edit"></i><?php echo $admi->wardName; ?></button>
                    <?php } ?>
                      </td>
                    <td><?php echo $admi->bedNo; ?></td>
                    <td><?php echo $admi->admissionDate; ?></td>
                    <td><?php echo $admi->admitReason; ?></td>
                    <td><?php echo $admi->dischargeDate; ?></td>
                    <td><?php echo $admi->dischargeReason; ?></td>
                    <td><?php echo $admi->created_at; ?></td>
                </tr>

                <!-- Modal -->
                <div class="modal fade text-left" id="modal-edit<?php echo $admi->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                  <div class="modal-lg modal-dialog" role="document">
                    {!! Form::open(['url' => 'editadmission']) !!}
                  <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Edit admission</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <input type="hidden" name="id" value="<?php echo $admi->id; ?>" class="form-control" required>
                  </div>
                  <div class="col-sm-6 form-group">
                      <label>Bed No.</label>
                      <input class="form-control" type="text" value="<?php echo $admi->bedNo; ?>" name="bedNo" required>
                  </div>
                  <div class="col-sm-12 form-group">
                      <textarea row="6" style="min-height:100px;" class="form-control" placeholder="Admission reason..." name="admitReason" required><?php echo $admi->admitReason; ?></textarea>
                  </div>
                </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                  </div>
                  {!! Form::close() !!}
                  </div>
                </div>

                <!-- Modal -->
                <div class="modal fade text-left" id="modal-discharge<?php echo $admi->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                  <div class="modal-lg modal-dialog" role="document">
                    {!! Form::open(['url' => 'dischargepatient']) !!}
                  <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Discharge this patient</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <input type="hidden" name="appointmentId" value="<?php echo $admi->appointmentId; ?>" class="form-control" required>
                        <input type="hidden" name="patientId" value="<?php echo $admi->patientId; ?>" class="form-control" required>
                      </div>
                      <div class="col-sm-12 form-group">
                          <textarea row="6" style="min-height:100px;" class="form-control" placeholder="Discharge reason..." name="dischargeReason" required></textarea>
                      </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                  {!! Form::close() !!}
                  </div>
                </div>

              <?php } ?>
            </tbody>
          </table>

                  </div>
                </div>
              </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
  </body>

  </html>
