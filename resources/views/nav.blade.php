<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                <img src="{{ URL::to('/') }}/profiles/<?php echo Auth::user()->profilePic; ?>" width="50px" height="50px" style="border-radius:50%;" />
            </div>
            <div class="admin-info">
                <div class="font-strong">{{Auth::user()->firstName}} {{Auth::user()->lastName}}</div><small>{{Auth::user()->mobileNo}}</small></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="{{URL::to('/dashboard')}}"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="heading">NAVIGATION</li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-medkit"></i>
                    <span class="nav-label">Patients</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{URL::to('/addpatient')}}">Register Patient</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/patients')}}">Manage Patients</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{URL::to('/doctors')}}"><i class="sidebar-item-icon fa fa-stethoscope"></i>
                    <span class="nav-label">Doctors</span>
                </a>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-leaf"></i>
                    <span class="nav-label">Pharmacy</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                  <li>
                      <a href="{{URL::to('/sales')}}">Make Sales</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/products')}}">Products</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/batches')}}">Batches</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/salesReport')}}">Sales Report</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/pharmacy')}}">Manage Pharmacy</a>
                  </li>
                    <li>
                        <a href="{{URL::to('/pharmacyReport')}}">Pharmacy Report</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-calendar"></i>
                    <span class="nav-label">Appointments</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{URL::to('/appointments')}}">Manage Appointments</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/appointmentsReport')}}">Appointments Report</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-map"></i>
                    <span class="nav-label">Cashier</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{URL::to('/receiveCash')}}">Receive Cash</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/cashier')}}">Collections History</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/cashierReport')}}">Collections Report</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/payments')}}">Payments</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/expensetypes')}}">Expense Types</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/expenses')}}">Expenses</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/salesReport')}}">Sales Report</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{URL::to('/admissions')}}"><i class="sidebar-item-icon fa fa-file"></i>
                    <span class="nav-label">Admissions</span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('/referrals')}}"><i class="sidebar-item-icon fa fa-arrows"></i>
                    <span class="nav-label">Referrals</span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('/users')}}"><i class="sidebar-item-icon fa fa-users"></i>
                    <span class="nav-label">Staff</span>
                </a>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-cog"></i>
                    <span class="nav-label">Setup</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                  <li>
                      <a href="{{URL::to('/categories')}}">Categories</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/brands')}}">Brands</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/colors')}}">Colors</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/sizes')}}">Sizes</a>
                  </li>
                </ul>
            </li>
            <li class="heading">SETTINGS</li>
            <li>
                <a href="{{URL::to('/wards')}}"><i class="sidebar-item-icon fa fa-bed"></i>
                    <span class="nav-label">Wards</span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('/services')}}"><i class="sidebar-item-icon fa fa-wrench"></i>
                    <span class="nav-label">Services</span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('/referralhospitals')}}"><i class="sidebar-item-icon fa fa-hospital-o"></i>
                    <span class="nav-label">Referral Hospitals</span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('/roles')}}"><i class="sidebar-item-icon fa fa-tasks"></i>
                    <span class="nav-label">Roles</span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('/permissions')}}"><i class="sidebar-item-icon fa fa-shield"></i>
                    <span class="nav-label">Permissions</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
