@include('emails.header')
	<!-- Intro -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding-bottom: 20px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td background="{{ URL::to('/') }}/emails/images/t8_bg.jpg" bgcolor="#114490" valign="top" height="366" class="bg" style="background-size:cover !important; -webkit-background-size:cover !important; background-repeat:none;">
													<!--[if gte mso 9]>
													<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:650px; height: 366px">
														<v:fill type="frame" src="images/t8_bg.jpg" color="#114490" />
														<v:textbox inset="0,0,0,0">
													<![endif]-->
													<div>
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="content-spacing" width="30" height="366" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
																<td style="padding: 30px 0px;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="h1 center pb25"
																			style="color:#ffffff; font-family:'Noto Sans',
																			Arial,sans-serif; font-size:40px;
																			line-height:46px; text-align:left;
																			padding-bottom:25px;">
																			Account Activation
																			</td>
																		</tr>
																		<tr>
																		<td class="text-center"
																		style="color:#ffffff; font-family:'Noto Sans', Arial,sans-serif;
																		font-size:16px; line-height:30px; text-align:left;">
																		Dear {{ $userName}},
																		</td>
																		</tr>
																		<tr>
																		<td class="text-center"
																		style="color:#ffffff; font-family:'Noto Sans', Arial,sans-serif;
																		font-size:16px; line-height:30px; text-align:left;text-decoration:none;">
																		Email Address:  {{ $email }}
																		</td>
																		</tr>
																		<tr>
																		<td class="text-center"
																		style="color:#ffffff; font-family:'Noto Sans', Arial,sans-serif;
																		font-size:16px; line-height:30px; text-align:left;">
																		Attached is your invoice for {{$title}}
																		</td>
																		</tr>

																		<tr>
																		<td class="text-center"
																		style="color:#ffffff; font-family:'Noto Sans', Arial,sans-serif;
																		font-size:16px; line-height:30px; text-align:left;">
																		All emails are subject to AlternetSMS’s Terms & Conditions of use.
																		</td>
																		</tr>
																		<tr>
																		<td class="text-center"
																		style="color:#ffffff; font-family:'Noto Sans', Arial,sans-serif;
																		font-size:16px; line-height:30px; text-align:left;">
																		&copy; All rights reserved
																		</td>
																		</tr>
																	</table>
																</td>
																<td class="content-spacing" width="30" style="font-size:0pt; line-height:0pt; text-align:left;"></td>
															</tr>
														</table>
													</div>
													<!--[if gte mso 9]>
														</v:textbox>
														</v:rect>
													<![endif]-->
												</td>
											</tr>
											<tr>
												<td class="mp15" style="padding: 20px 30px;background: rgb(0,51,102);background: linear-gradient(90deg, rgba(0,51,102,1) 0%, rgba(167,69,36,1) 50%);" align="center">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="h5 white" style="font-family:'Noto Sans', Arial,sans-serif; font-size:16px; line-height:22px; text-align:left; font-weight:bold; color:#ffffff;">CREDIBLE PROVIDERS</td>
																	</tr>
																</table>
															</th>
															<th class="column" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
															<th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="h5 white" style="font-family:'Noto Sans', Arial,sans-serif; font-size:16px; line-height:22px; text-align:left; font-weight:bold; color:#ffffff;">AFFODABLE COSTS</td>
																	</tr>
																</table>
															</th>
															<th class="column" width="50" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;"></th>
															<th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="h5 white" style="font-family:'Noto Sans', Arial,sans-serif; font-size:16px; line-height:22px; text-align:left; font-weight:bold; color:#ffffff;">SAVE ON TIME</td>
																	</tr>
																</table>
															</th>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END Intro -->
@include('emails.footer')
