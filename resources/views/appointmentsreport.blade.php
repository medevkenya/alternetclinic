<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | View Appointments</title>
    @include('headerlink')
    <link href="assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Appointments | <?php echo count($list); ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All appointments</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Appointments
                                  <?php if(isset($fromdate)) { echo "From: ".$fromdate." To: ".$todate; } ?>
                                </div>
                            </div>
                            <div class="ibox-body">

                              @if (count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                             @endforeach
                                         </ul>
                                     </div>
                                    @endif

                                    @if ($message = Session::get('error'))
                                         <div class="alert alert-danger">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if ($message = Session::get('success'))
                                         <div class="alert alert-success">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if (session('status0'))
                                    <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status0') }}
                                    </div>
                                    @endif

                                    @if (session('status1'))
                                    <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status1') }}
                                    </div>
                                    @endif

{!! Form::open(['url' => 'postappointmentsreport']) !!}
{{ csrf_field() }}
<div class="row">

<div class="form-group col-md-3">
  <label>From Date</label>
  <input type="date" class="form-control" name="fromdate" required>
</div>
<div class="form-group col-md-3">
  <label>To Date</label>
  <input type="date" class="form-control" name="todate" required>
</div>
<div class="form-group col-md-3">
  <label>.</label>
  <button type="submit" class="btn btn-block btn-primary">Search</button>
</div>
</div>
{!! Form::close() !!}

                      <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Doctor</th>
                              <th>Service</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th>Actions</th>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                              <th>No.</th>
                              <th>Doctor</th>
                              <th>Service</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th>Actions</th>
                          </tr>
                      </tfoot>
                      <tbody>
                        <?php foreach ($list as $app) {
                          ?>
                          <tr>
                            <td><?php echo $app->appointmentNo; ?></td>
                              <td><?php echo $app->firstName; ?> <?php echo $app->lastName; ?></td>
                              <td><?php echo $app->serviceName; ?></td>
                              <td><?php echo $app->appointmentDate; ?></td>
                              <td><?php echo $app->status; ?></td>
                              <td>
                            <a href="<?php $url = URL::to("/viewAppointment/".$app->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
                          </td>
                          </tr>

                        <?php } ?>
                      </tbody>
                  </table>

                            </div>
                          </div>
                        </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    <script src="assets/vendors/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="assets/vendors/jquery-knob/dist/jquery.knob.min.js" type="text/javascript"></script>
    <script src="assets/vendors/moment/min/moment.min.js" type="text/javascript"></script>
  </body>

  </html>
