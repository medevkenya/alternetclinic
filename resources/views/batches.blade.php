<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Batches</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Batches</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Batches</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Batches</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addbatch']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Batch</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                                  <?php
                                                  $categories = \App\Categories::getAll();
                                                  ?>

                                                  <div class="col-sm-12 form-group">
                                                      <label>Category</label>
                                                      <select class="form-control" name="categoryId" id="categoryIdBatch" required>
                                                     <option></option>
                                                     <?php foreach ($categories as $keyf) { ?>
                                                       <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-12 form-group">
                                                      <label>Product</label>
                                                      <select class="form-control" name="productId" id="productId" required>
                                                   </select>
                                                  </div>

                                              <div class="col-sm-12 form-group">
                                                  <label>Batch No.</label>
                                                  <input class="form-control" type="text" name="batchNo" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                  <label>Expiry Date</label>
                                                  <input class="form-control" type="date" name="expiryDate" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                  <label>Quantity</label>
                                                  <input class="form-control" type="number" name="quantity" required>
                                              </div>

                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                     <li>{{ $error }}</li>
                                                     @endforeach
                                                 </ul>
                                             </div>
                                            @endif

                                            @if ($message = Session::get('error'))
                                                 <div class="alert alert-danger">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if ($message = Session::get('success'))
                                                 <div class="alert alert-success">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if (session('status0'))
                                            <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status0') }}
                                            </div>
                                            @endif

                                            @if (session('status1'))
                                            <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status1') }}
                                            </div>
                                            @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Batche No.</th>
                                            <th>Product</th>
                                            <th>Expiry Date</th>
                                            <th>Quantity</th>
                                            <th>In Stock</th>
                                            <th>Sold</th>
                                            <th>Created On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Batche No.</th>
                                          <th>Product</th>
                                          <th>Expiry Date</th>
                                          <th>Quantity</th>
                                          <th>In Stock</th>
                                          <th>Sold</th>
                                          <th>Created On</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $batch) {
                                        ?>
                                        <tr>
                                          <td><?php echo $batch->batchNo; ?></td>
                                          <td><?php echo $batch->productName; ?></td>
                                          <td>
                                            <?php
                                            if($batch->expiryDate < date('Y-m-d')) {
                                            ?>
                                            <?php echo $batch->expiryDate; ?>
                                          <?php } else { ?>
                                            <?php echo $batch->expiryDate; ?>
                                          <?php } ?>
                                          </td>
                                          <td><?php echo $batch->quantity; ?></td>
                                          <td><?php echo $batch->quantity - $batch->sold; ?></td>
                                          <td><?php echo $batch->sold; ?></td>
                                          <td><?php echo $batch->created_at; ?></td>
                                            <td>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php echo $batch->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $batch->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editbatch']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Batche</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Category</label>
                                              <select class="form-control" name="categoryId" id="categoryIdbe">
                                             <option value="<?php echo $batch->categoryId; ?>"><?php echo $batch->categoryName; ?></option>
                                             <?php foreach ($categories as $keyf) { ?>
                                               <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Product</label>
                                              <select class="form-control" name="productId" id="productIde" required>
                                                <option value="<?php echo $batch->productId; ?>"><?php echo $batch->productName; ?></option>
                                           </select>
                                          </div>

                                      <div class="col-sm-12 form-group">
                                          <label>Batch No.</label>
                                          <input class="form-control" type="text" name="batchNo" value="<?php echo $batch->batchNo; ?>" required>
                                      </div>

                                      <div class="col-sm-12 form-group">
                                          <label>Expiry Date</label>
                                          <input class="form-control" type="date" name="expiryDate" value="<?php echo $batch->expiryDate; ?>" required>
                                      </div>

                                      <div class="col-sm-12 form-group">
                                          <label>Quantity</label>
                                          <input class="form-control" type="number" name="quantity" value="<?php echo $batch->quantity; ?>" required>
                                      </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletebatch']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Batche</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this batch</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
