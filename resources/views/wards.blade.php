<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Wards</title>
    @include('headerlink')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Wards</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All wards</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Wards</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addward"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addward" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-lg modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addward']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Ward</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                              <div class="col-sm-6 form-group">
                                                  <label>Ward Name</label>
                                                  <input class="form-control" type="text" name="wardName" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Available Spaces</label>
                                                  <input class="form-control" type="number" name="spacesavailable" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Occupied Spaces</label>
                                                  <input class="form-control" type="number" name="spacesoccupied" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Status</label>
                                                  <select class="form-control" name="status" required>
                                                    <option value=""></option>
                                                    <option value="Available">Available</option>
                                                    <option value="Not Available">Not Available</option>
                                                  </select>
                                              </div>

                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                                   <div class="alert alert-danger">
                                                       <ul>
                                                           @foreach ($errors->all() as $error)
                                                           <li>{{ $error }}</li>
                                                           @endforeach
                                                       </ul>
                                                   </div>
                                                  @endif

                                                  @if ($message = Session::get('error'))
                                                       <div class="alert alert-danger">
                                                           {{ $message }}
                                                       </div>
                                                  @endif

                                                  @if ($message = Session::get('success'))
                                                       <div class="alert alert-success">
                                                           {{ $message }}
                                                       </div>
                                                  @endif

                                                  @if (session('status0'))
                                                  <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                  {{ session('status0') }}
                                                  </div>
                                                  @endif

                                                  @if (session('status1'))
                                                  <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                  {{ session('status1') }}
                                                  </div>
                                                  @endif


                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Ward Name</th>
                                            <th>Spaces Available</th>
                                            <th>Spaces Occupied</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Ward Name</th>
                                          <th>Spaces Available</th>
                                          <th>Spaces Occupied</th>
                                          <th>Status</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $ward) {
                                        ?>
                                        <tr>
                                          <td><?php echo $ward->wardName; ?></td>
                                          <td><?php echo $ward->spacesavailable; ?></td>
                                          <td><?php echo $ward->spacesoccupied; ?></td>
                                          <td><?php echo $ward->status; ?></td>
                                            <td>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editward<?php echo $ward->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleteward<?php echo $ward->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editward<?php echo $ward->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-lg modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editward']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Ward</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $ward->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Ward Name</label>
                                              <input class="form-control" type="text" name="wardName" value="<?php echo $ward->wardName; ?>" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Available Spaces</label>
                                              <input class="form-control" type="number" name="spacesavailable" value="<?php echo $ward->spacesavailable; ?>" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Occupied Spaces</label>
                                              <input class="form-control" type="number" name="spacesoccupied" value="<?php echo $ward->spacesoccupied; ?>" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Status</label>
                                              <select class="form-control" name="status" required>
                                                <option value="<?php echo $ward->status; ?>"><?php echo $ward->status; ?></option>
                                                <option value="Available">Available</option>
                                                <option value="Not Available">Not Available</option>
                                              </select>
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleteward<?php echo $ward->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deleteward']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Ward</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $ward->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this ward</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
  </body>

  </html>
