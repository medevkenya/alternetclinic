<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Referral Hospitals</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Referral Hospitals</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All referralhospitals</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Referral Hospitals</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addreferralhospital"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addreferralhospital" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-lg modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addreferralhospital']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Referral Hospital</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                              <div class="col-sm-12 form-group">
                                                  <label>Referral Hospital Name</label>
                                                  <input class="form-control" type="text" name="name" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Location</label>
                                                  <input class="form-control" type="text" name="location" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Contacts</label>
                                                  <input class="form-control" type="text" name="contacts" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                  <label>Specialities</label>
                                                  <input class="form-control" type="text" name="specialities" required>
                                              </div>

                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>


                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Referral Hospital Name</th>
                                            <th>Location</th>
                                            <th>Contacts</th>
                                            <th>Specialities</th>
                                            <th>Total Referrals</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Referral Hospital Name</th>
                                          <th>Location</th>
                                          <th>Contacts</th>
                                          <th>Specialities</th>
                                          <th>Total Referrals</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $referralhospital) {
                                        $totalReferrals = \App\Referrals::countForHospital($referralhospital->id);
                                        ?>
                                        <tr>
                                          <td><?php echo $referralhospital->name; ?></td>
                                          <td><?php echo $referralhospital->location; ?></td>
                                          <td><?php echo $referralhospital->contacts; ?></td>
                                          <td><?php echo $referralhospital->specialities; ?></td>
                                          <td><?php echo $totalReferrals; ?></td>
                                            <td>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editreferralhospital<?php echo $referralhospital->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deletereferralhospital<?php echo $referralhospital->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editreferralhospital<?php echo $referralhospital->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-lg modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editreferralhospital']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Referral Hospital</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $referralhospital->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Referral Hospital Name</label>
                                              <input class="form-control" type="text" name="name" value="<?php echo $referralhospital->name; ?>" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Location</label>
                                              <input class="form-control" type="text" name="location" value="<?php echo $referralhospital->location; ?>" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Contacts</label>
                                              <input class="form-control" type="text" name="contacts" value="<?php echo $referralhospital->contacts; ?>" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Specialities</label>
                                              <input class="form-control" type="text" name="specialities" value="<?php echo $referralhospital->specialities; ?>" required>
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deletereferralhospital<?php echo $referralhospital->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletereferralhospital']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Referral Hospital</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $referralhospital->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this referral hospital</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
