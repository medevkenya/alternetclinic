<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | View Patient</title>
    @include('headerlink')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Patient | <?php echo strtoupper($details->firstName); ?> <?php echo strtoupper($details->middleName); ?> <?php echo strtoupper($details->lastName); ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Location | <?php echo ucwords($details->location); ?></li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Personal details</div>
                                <div class="ibox-tools">
                                    <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                                    <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                    <!-- <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item">option 1</a>
                                        <a class="dropdown-item">option 2</a>
                                    </div> -->
                                </div>
                            </div>
                            <div class="ibox-body">
                              @if (count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                             @endforeach
                                         </ul>
                                     </div>
                                    @endif

                                    @if ($message = Session::get('error'))
                                         <div class="alert alert-danger">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if ($message = Session::get('success'))
                                         <div class="alert alert-success">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if (session('status0'))
                                    <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status0') }}
                                    </div>
                                    @endif

                                    @if (session('status1'))
                                    <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status1') }}
                                    </div>
                                    @endif


                                   <div class="row">
                                     <input class="form-control" value="<?php echo $details->id; ?>" type="hidden" name="id">
                                     <input class="form-control" value="<?php echo $gdetails->id; ?>" type="hidden" name="guardianId">

                                       <div class="col-sm-4 form-group">
                                           <label>Gender</label>
                                           <select class="form-control" name="gender" readonly>
                                             <option value="<?php echo $details->gender; ?>"><?php echo $details->gender; ?></option>
                                             <option value="Male">Male</option>
                                             <option value="Female">Female</option>
                                           </select>
                                       </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Email  (optional)</label>
                                       <input class="form-control" value="<?php echo $details->email; ?>" type="email" name="email" placeholder="Email address" readonly>
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Mobile No.  (optional)</label>
                                       <input class="form-control" value="<?php echo $details->mobileNo; ?>" type="number" name="mobileNo" placeholder="Mobile No." readonly>
                                   </div>

                                   <div class="col-sm-4 form-group">
                                       <label>Date of Birth</label>
                                       <input class="form-control" value="<?php echo $details->dob; ?>" type="date" name="dob" placeholder="Date of Birth" readonly>
                                   </div>

                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Name</label>
                                       <input class="form-control" value="<?php echo $gdetails->name; ?>" type="text" name="guardianName" placeholder="Name" readonly>
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Email  (optional)</label>
                                       <input class="form-control" value="<?php echo $gdetails->email; ?>" type="email" name="guardianEmail" placeholder="Email address" readonly>
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Telephone  (optional)</label>
                                       <input class="form-control" value="<?php echo $gdetails->phone; ?>" type="number" name="guardianPhone" placeholder="Telephone" readonly>
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Gender</label>
                                       <select class="form-control" name="guardianGender" readonly>
                                         <option value="<?php echo $gdetails->gender; ?>"><?php echo $gdetails->gender; ?></option>
                                         <option value="Male">Male</option>
                                         <option value="Female">Female</option>
                                       </select>
                                   </div>
                                   <div class="col-sm-4 form-group">
                                       <label>Guardian Location</label>
                                       <input class="form-control" value="<?php echo $gdetails->location; ?>" type="text" name="guardianLocation" placeholder="Location" readonly>
                                   </div>

                                 </div>

                                   <div class="form-group">
                                       <a href="<?php $url = URL::to("/editPatient/".$details->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Edit Patient</a>
                                       <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-printcard"><i class="fa fa-id-card"></i> Print Patient Card</button>
                                   </div>

                                   <!-- Modal -->
                                   <div class="modal fade text-left" id="modal-printcard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                     <div class="modal-lg modal-dialog" role="document">
                                       {!! Form::open(['url' => 'printCard']) !!}
                                     <div class="modal-content">
                                       <div class="modal-header">
                                       <h4 class="modal-title" id="myModalLabel1">Patient Card</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                       </button>
                                       </div>
                                       <div class="modal-body">
                                       <div class="row">
                                       <div class="col-xl-12 col-lg-12 col-md-12">
                                         <input type="hidden" name="patientId" value="<?php echo $details->id; ?>" class="form-control" required>
                                         @include('card')
                                     </div>

                                   </div>
                                       </div>
                                       <div class="modal-footer">
                                       <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                       <button type="button" class="btn btn-primary" onclick="PrintElem()">Print Now</button>
                                       </div>
                                     </div>
                                   </form>
                                     </div>
                                   </div>

                                   <script>
                                   function PrintElem()
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head>');
    mywindow.document.write('<link rel="stylesheet" href="http://127.0.0.1/ALTERNET/klinik/public/assets/css/card.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(document.getElementById('cardcard').innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/


    setTimeout(function () {
        mywindow.print();
    mywindow.close();

    }, 1000)
    return true;
}
                                   </script>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Appointments</div>
                                <div class="ibox-tools">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addappointment"><i class="fa fa-plus"></i> Create New</button>
                                </div>
                            </div>
                            <div class="ibox-body">

                              <!-- Modal -->
                              <div class="modal fade text-left" id="modal-addappointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  {!! Form::open(['url' => 'addappointment']) !!}
                                <div class="modal-content">
                                  <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Create New Appointment</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                                  <div class="modal-body">
                                  <div class="row">
                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                    <input type="hidden" name="patientId" value="<?php echo $details->id; ?>" class="form-control" required>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Doctor</label>
                                    <select class="form-control" name="doctorId" required>
                                      <option value=""></option>
                                      <?php foreach ($doctors as $keyf) { ?>
                                        <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->firstName; ?> <?php echo $keyf->lastName; ?></option>
                                      <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-6 form-group">
                                    <label>Service</label>
                                    <select class="form-control" name="serviceId" required>
                                      <option value=""></option>
                                      <?php foreach ($services as $keys) { ?>
                                        <option value="<?php echo $keys->id; ?>"><?php echo $keys->name; ?></option>
                                      <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-6 form-group">
                                    <label>Appointment date</label>
                                    <input class="form-control" type="date" name="appointmentDate" value="<?php echo date('Y-m-d'); ?>" required>
                                </div>

                              </div>
                                  </div>
                                  <div class="modal-footer">
                                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                </div>
                                {!! Form::close() !!}
                                </div>
                              </div>

                              <?php $appointments = \App\Appointments::getForPatient($details->id); ?>

                      <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Doctor</th>
                              <th>Service</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th>Actions</th>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                              <th>No.</th>
                              <th>Doctor</th>
                              <th>Service</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th>Actions</th>
                          </tr>
                      </tfoot>
                      <tbody>
                        <?php foreach ($appointments as $app) {
                          ?>
                          <tr>
                            <td><?php echo $app->appointmentNo; ?></td>
                              <td><?php echo $app->firstName; ?> <?php echo $app->lastName; ?></td>
                              <td><?php echo $app->serviceName; ?></td>
                              <td><?php echo $app->appointmentDate; ?></td>
                              <td><?php echo $app->status; ?></td>
                              <td>
                            <a href="<?php $url = URL::to("/viewAppointment/".$app->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editappointment<?php echo $app->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleteappointment<?php echo $app->id; ?>"><i class="fa fa-trash"></i> Delete</button>

                          </td>
                          </tr>

                          <!-- Modal -->
                          <div class="modal fade text-left" id="modal-editappointment<?php echo $app->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              {!! Form::open(['url' => 'editappointment']) !!}
                            <div class="modal-content">
                              <div class="modal-header">
                              <h4 class="modal-title" id="myModalLabel1">Edit Appointment</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                              </div>
                              <div class="modal-body">
                              <div class="row">
                              <div class="col-xl-12 col-lg-12 col-md-12">
                                <input type="hidden" name="id" value="<?php echo $app->id; ?>" class="form-control" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label>Doctor</label>
                                <select class="form-control" name="doctorId" required>
                                  <option value="<?php echo $app->doctorId; ?>"><?php echo $app->firstName; ?> <?php echo $app->lastName; ?></option>
                                  <?php foreach ($doctors as $keyf) { ?>
                                    <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->firstName; ?> <?php echo $keyf->lastName; ?></option>
                                  <?php } ?>
                                </select>
                            </div>

                            <div class="col-sm-6 form-group">
                                <label>Service</label>
                                <select class="form-control" name="serviceId" required>
                                  <option value="<?php echo $app->serviceId; ?>"><?php echo $app->serviceName; ?></option>
                                  <?php foreach ($services as $keys) { ?>
                                    <option value="<?php echo $keys->id; ?>"><?php echo $keys->name; ?></option>
                                  <?php } ?>
                                </select>
                            </div>

                            <div class="col-sm-6 form-group">
                                <label>Appointment date</label>
                                <input class="form-control" type="date" name="appointmentDate" value="<?php echo $app->appointmentDate; ?>" required>
                            </div>

                            <div class="col-sm-6 form-group">
                                <label>Status</label>
                                <select class="form-control" name="status" required>
                                  <option value="<?php echo $app->status; ?>"><?php echo $app->status; ?></option>
                                  <option value="Pending">Pending</option>
                                  <option value="Done">Done</option>
                                  <option value="Cancelled">Cancelled</option>
                                </select>
                            </div>

                          </div>
                              </div>
                              <div class="modal-footer">
                              <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save Changes</button>
                              </div>
                            </div>
                            {!! Form::close() !!}
                            </div>
                          </div>

                          <!-- Modal -->
                          <div class="modal fade text-left" id="modal-deleteappointment<?php echo $app->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              {!! Form::open(['url' => 'deleteappointment']) !!}
                            <div class="modal-content">
                              <div class="modal-header">
                              <h4 class="modal-title" id="myModalLabel1">Delete Appointment</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                              </div>
                              <div class="modal-body">
                              <div class="row">
                              <div class="col-xl-6 col-lg-6 col-md-6">
                                <input type="hidden" name="id" value="<?php echo $app->id; ?>" class="form-control" required>
                            </div>
                            <h5 style="margin-left:2%;">Confirm that you want to delete this appointment</h5>
                          </div>
                              </div>
                              <div class="modal-footer">
                              <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Delete</button>
                              </div>
                            </div>
                            {!! Form::close() !!}
                            </div>
                          </div>

                        <?php } ?>
                      </tbody>
                  </table>

                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                            <div class="ibox">
                                <div class="ibox-head">
                                    <div class="ibox-title">Costs</div>

                                </div>
                                <div class="ibox-body">

                                  <?php $costs = \App\Costs::getAllForPatient($details->id); ?>

                          <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>Appointment No.</th>
                                  <th>Treatment</th>
                                  <th>Prescription</th>
                                  <th>Diagnosis</th>
                                  <th>Lab Tests</th>
                                  <th>Medicines</th>
                                  <th>Other Fees</th>
                                  <th>Total</th>
                                  <th>Amount Paid</th>
                                  <th>Balance</th>
                                  <th>Date</th>
                                  <th>Status</th>
                                  <th>Actions</th>
                              </tr>
                          </thead>
                          <tfoot>
                              <tr>
                                <th>Appointment No.</th>
                                <th>Treatment</th>
                                <th>Prescription</th>
                                <th>Diagnosis</th>
                                <th>Lab Tests</th>
                                <th>Medicines</th>
                                <th>Other Fees</th>
                                <th>Total</th>
                                <th>Amount Paid</th>
                                <th>Balance</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Actions</th>
                              </tr>
                          </tfoot>
                          <tbody>
                            <?php foreach ($costs as $app) {
                              $otherFees = \App\Fees::totalForAppointment($app->appointmentId);
                              $totalCosts = $otherFees + $app->treatmentCost + $app->prescriptionsCost + $app->diagnosisCost + $app->labtestCost + $app->medicinesCost;
                              $balance = $totalCosts - $app->amountPaid;
                              ?>
                              <tr>
                                <td><a href="<?php $url = URL::to("/viewAppointment/".$app->appointmentId); print_r($url); ?>" class="btn btn-primary"><?php echo $app->appointmentNo; ?></a></td>

                                  <td><?php echo $app->treatmentCost; ?></td>
                                  <td><?php echo $app->prescriptionsCost; ?></td>
                                  <td><?php echo $app->diagnosisCost; ?></td>
                                  <td><?php echo $app->labtestCost; ?></td>
                                  <td><?php echo $app->medicinesCost; ?></td>
                                  <td><?php echo $otherFees; ?></td>
                                  <td><?php echo $totalCosts; ?></td>
                                  <td><?php echo $app->amountPaid; ?></td>
                                  <td><?php echo $balance; ?></td>
                                  <td><?php echo $app->created_at; ?></td>
                                  <td><?php echo $app->status; ?></td>
                                  <td>
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editcost<?php echo $app->id; ?>"><i class="fa fa-edit"></i> Update</button>
                              </td>
                              </tr>

                              <!-- Modal -->
                              <div class="modal fade text-left" id="modal-editcost<?php echo $app->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  {!! Form::open(['url' => 'editcost']) !!}
                                <div class="modal-content">
                                  <div class="modal-header">
                                  <h4 class="modal-title" id="myModalLabel1">Edit Costs</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                                  <div class="modal-body">
                                  <div class="row">
                                  <div class="col-xl-12 col-lg-12 col-md-12">
                                    <input type="hidden" name="id" value="<?php echo $app->id; ?>" class="form-control" required>
                                </div>

                                <div class="col-sm-6 form-group">
                                    <label>Amount Paid</label>
                                    <input class="form-control" type="number" name="amountPaid" value="<?php echo $app->amountPaid; ?>" required>
                                </div>

                                <div class="col-sm-6 form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status" required>
                                      <option value="<?php echo $app->status; ?>"><?php echo $app->status; ?></option>
                                      <option value="Pending">Pending</option>
                                      <option value="Paid">Paid</option>
                                    </select>
                                </div>

                              </div>
                                  </div>
                                  <div class="modal-footer">
                                  <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Save Changes</button>
                                  </div>
                                </div>
                                {!! Form::close() !!}
                                </div>
                              </div>

                            <?php } ?>
                          </tbody>
                      </table>

                                </div>
                              </div>
                            </div>

                            <div class="col-md-12">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Admissions
            </div>

        </div>
        <div class="ibox-body">

  <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
  <thead>
      <tr>
        <th>Appointment No.</th>
        <th>Ward</th>
        <th>Bed No.</th>
        <th>Admission Date</th>
        <th>Admission Reason</th>
        <th>Discharge Date</th>
        <th>Discharge Reason</th>
        <th>Date</th>
      </tr>
  </thead>
  <tfoot>
      <tr>
        <th>Appointment No.</th>
        <th>Ward</th>
        <th>Bed No.</th>
        <th>Admission Date</th>
        <th>Admission Reason</th>
        <th>Discharge Date</th>
        <th>Discharge Reason</th>
        <th>Date</th>
      </tr>
  </tfoot>
  <tbody>
    <?php $admissions = \App\Admissions::getForPatient($details->id); foreach ($admissions as $admi) {
      ?>
      <tr>
        <td><a href="<?php $url = URL::to("/viewAppointment/".$admi->appointmentId); print_r($url); ?>" class="btn btn-primary"><?php echo $admi->appointmentNo; ?></a></td>
          <td>
            <?php if($admi->dischargeDate==null) { ?>
            <?php echo $admi->wardName; ?><br><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-discharge<?php echo $admi->id; ?>"><i class="fa fa-bed"></i> Discharge Patient</button>
          <?php } else { ?>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-edit<?php echo $admi->id; ?>"><i class="fa fa-edit"></i><?php echo $admi->wardName; ?></button>
          <?php } ?>
            </td>
          <td><?php echo $admi->bedNo; ?></td>
          <td><?php echo $admi->admissionDate; ?></td>
          <td><?php echo $admi->admitReason; ?></td>
          <td><?php echo $admi->dischargeDate; ?></td>
          <td><?php echo $admi->dischargeReason; ?></td>
          <td><?php echo $admi->created_at; ?></td>
      </tr>

      <!-- Modal -->
      <div class="modal fade text-left" id="modal-edit<?php echo $admi->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-lg modal-dialog" role="document">
          {!! Form::open(['url' => 'editadmission']) !!}
        <div class="modal-content">
          <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel1">Edit admission</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body">
          <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12">
            <input type="hidden" name="id" value="<?php echo $admi->id; ?>" class="form-control" required>
        </div>
        <div class="col-sm-6 form-group">
            <label>Bed No.</label>
            <input class="form-control" type="text" value="<?php echo $admi->bedNo; ?>" name="bedNo" required>
        </div>
        <div class="col-sm-12 form-group">
            <textarea row="6" style="min-height:100px;" class="form-control" placeholder="Admission reason..." name="admitReason" required><?php echo $admi->admitReason; ?></textarea>
        </div>
      </div>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save Changes</button>
          </div>
        </div>
        {!! Form::close() !!}
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade text-left" id="modal-discharge<?php echo $admi->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-lg modal-dialog" role="document">
          {!! Form::open(['url' => 'dischargepatient']) !!}
        <div class="modal-content">
          <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel1">Discharge this patient</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body">
          <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12">
              <input type="hidden" name="appointmentId" value="<?php echo $admi->appointmentId; ?>" class="form-control" required>
              <input type="hidden" name="patientId" value="<?php echo $admi->patientId; ?>" class="form-control" required>
            </div>
            <div class="col-sm-12 form-group">
                <textarea row="6" style="min-height:100px;" class="form-control" placeholder="Discharge reason..." name="dischargeReason" required></textarea>
            </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
        {!! Form::close() !!}
        </div>
      </div>

    <?php } ?>
  </tbody>
</table>

        </div>
      </div>
    </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
  </body>

  </html>
