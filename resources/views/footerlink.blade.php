<!-- CORE PLUGINS-->
<script src="{{ URL::asset('assets/vendors/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/popper.js/dist/umd/popper.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/metisMenu/dist/metisMenu.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL PLUGINS-->
<script src="{{ URL::asset('assets/vendors/chart.js/dist/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-us-aea-en.js')}}" type="text/javascript"></script>
<!-- CORE SCRIPTS-->
<script src="{{ URL::asset('assets/js/app.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL SCRIPTS-->
<script src="{{ URL::asset('assets/js/scripts/dashboard_1_demo.js')}}" type="text/javascript"></script>
<script>
$("#categoryId").change(function(){
  $.ajax({
      url: "{{ route('getBrands') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        console.log("js models--"+data.models);
          $('#brandId').html(data.models);
      }
  });
});

$("#categoryIde").change(function(){
  $.ajax({
      url: "{{ route('getBrands') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        console.log("js models--"+data.models);
          $('#brandIde').html(data.models);
      }
  });
});

$("#categoryIdBatch").change(function(){
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        console.log("js products--"+data.products);
          $('#productId').html(data.products);
      }
  });
});

$("#categoryIdbe").change(function(){
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        console.log("js products--"+data.products);
          $('#productIde').html(data.products);
      }
  });
});

$(document).ready(function (e) {

$('#photoUrl').change(function(){

let reader = new FileReader();

reader.onload = (e) => {

  $('#preview-image-before-upload').attr('src', e.target.result);
}

reader.readAsDataURL(this.files[0]);

});

});
        </script>
