-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2021 at 07:46 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alternet_clinic`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `adminId`, `description`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Updated group [ Chama Moja ]', 0, 0, '2021-02-06 17:07:22', '2021-02-06 17:07:22'),
(2, 1, 'Added new role [Accounts2]', 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(3, 1, 'Added new role [HR]', 1, 0, '2021-02-12 08:18:47', '2021-02-12 08:18:47'),
(4, 1, 'Added new role [HR2]', 1, 0, '2021-02-12 08:19:32', '2021-02-12 08:19:32'),
(5, 1, 'Added new role [HR23]', 1, 0, '2021-02-12 08:20:00', '2021-02-12 08:20:00'),
(6, 1, 'Added new role [HR233]', 1, 0, '2021-02-12 08:21:07', '2021-02-12 08:21:07'),
(7, 1, 'Added new role [HR2337]', 1, 0, '2021-02-12 08:22:31', '2021-02-12 08:22:31'),
(8, 1, 'Added new role [gigi]', 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(9, 1, 'Added new role [mm]', 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(10, 1, 'Added new role [vvvvvv]', 1, 0, '2021-02-12 08:47:37', '2021-02-12 08:47:37'),
(11, 1, 'Added new role [vvvvvfv]', 1, 0, '2021-02-12 08:47:54', '2021-02-12 08:47:54'),
(12, 1, 'Added new role [vvvvvfvfdfdfd]', 1, 0, '2021-02-12 08:48:12', '2021-02-12 08:48:12'),
(13, 1, 'Added new role [kvd]', 1, 0, '2021-02-12 08:52:00', '2021-02-12 08:52:00'),
(14, 1, 'Added new role [kvd777]', 1, 0, '2021-02-12 08:53:01', '2021-02-12 08:53:01'),
(15, 1, 'Added new role [vbvbvbv]', 1, 0, '2021-02-12 08:55:07', '2021-02-12 08:55:07'),
(16, 1, 'Added new role [vbvbvbv444]', 1, 0, '2021-02-12 08:56:23', '2021-02-12 08:56:23'),
(17, 1, 'Added new role [cvcvcv]', 1, 0, '2021-02-12 09:17:24', '2021-02-12 09:17:24'),
(18, 1, 'Added new role [cvddfdfdf]', 1, 0, '2021-02-12 09:19:04', '2021-02-12 09:19:04'),
(19, 1, 'Added new role [trtrtrt]', 1, 0, '2021-02-12 09:22:48', '2021-02-12 09:22:48'),
(20, 1, 'Edited SMS batch [14]', 1, 0, '2021-02-16 11:30:16', '2021-02-16 11:30:16'),
(21, 1, 'Deleted SMS batch [14]', 1, 0, '2021-02-16 11:30:26', '2021-02-16 11:30:26'),
(22, 1, 'Added user [Patient 1 - 0702150260]', 1, 0, '2021-03-03 06:32:05', '2021-03-03 06:32:05'),
(23, 1, 'Added user [Patient 1 - 0702150260]', 1, 0, '2021-03-03 06:32:44', '2021-03-03 06:32:44'),
(24, 1, 'Added guaradian [maimuna Muna@gmail.com - 0725142514]', 1, 0, '2021-03-03 06:32:44', '2021-03-03 06:32:44'),
(25, 1, 'Added user [Patient 1 - 0702150260]', 1, 0, '2021-03-03 06:34:10', '2021-03-03 06:34:10'),
(26, 1, 'Added guaradian [maimuna Muna@gmail.com - 0725142514]', 1, 0, '2021-03-03 06:34:10', '2021-03-03 06:34:10'),
(27, 1, 'Added user [Patient 1 - 0702150260]', 1, 0, '2021-03-03 06:39:00', '2021-03-03 06:39:00'),
(28, 1, 'Added guaradian [maimuna Muna@gmail.com - 0725142514]', 1, 0, '2021-03-03 06:39:00', '2021-03-03 06:39:00'),
(29, 1, 'Added user [Patient 1 - 0702150260]', 1, 0, '2021-03-03 06:39:28', '2021-03-03 06:39:28'),
(30, 1, 'Added guaradian [maimuna Muna@gmail.com - 0725142514]', 1, 0, '2021-03-03 06:39:28', '2021-03-03 06:39:28'),
(31, 1, 'Added appointment for patient [34, appointment no. - 1APT/2021-03-03]', 1, 0, '2021-03-03 06:39:28', '2021-03-03 06:39:28'),
(32, 1, 'Added user [kevo gd - 0114254785]', 1, 0, '2021-03-03 06:40:44', '2021-03-03 06:40:44'),
(33, 1, 'Added guaradian [gdfg Mundfgdfa@gmail.com - 53534543]', 1, 0, '2021-03-03 06:40:44', '2021-03-03 06:40:44'),
(34, 1, 'Added appointment for patient [35, appointment no. - 1APT/2021-03-03]', 1, 0, '2021-03-03 06:40:44', '2021-03-03 06:40:44'),
(35, 1, 'Edited patient [kevob gd - 53453453]', 1, 0, '2021-03-03 09:06:41', '2021-03-03 09:06:41'),
(36, 1, 'Edited patient [kevob gd - 53453453]', 1, 0, '2021-03-03 09:07:10', '2021-03-03 09:07:10'),
(37, 1, 'Updated guaradian [gdfg Mundfgdfa@gmail.com - 53534543]', 1, 0, '2021-03-03 09:07:10', '2021-03-03 09:07:10'),
(38, 1, 'Added appointment for patient [35, appointment no. - 3APT/20210303]', 1, 0, '2021-03-03 10:16:51', '2021-03-03 10:16:51'),
(39, 1, 'Edited appointment 2, No. [1APT/2021-03-03]', 1, 0, '2021-03-03 10:17:43', '2021-03-03 10:17:43'),
(40, 1, 'Edited treatment 3', 1, 0, '2021-03-03 14:41:20', '2021-03-03 14:41:20'),
(41, 1, 'Edited Prescription 3', 1, 0, '2021-03-03 14:43:00', '2021-03-03 14:43:00'),
(42, 1, 'Edited diagnosis 3', 1, 0, '2021-03-03 14:43:05', '2021-03-03 14:43:05'),
(43, 1, 'Edited Prescription 3', 1, 0, '2021-03-03 14:43:11', '2021-03-03 14:43:11'),
(44, 1, 'Edited labtest 3', 1, 0, '2021-03-03 14:43:17', '2021-03-03 14:43:17'),
(45, 1, 'Edited Prescription 3', 1, 0, '2021-03-03 14:44:16', '2021-03-03 14:44:16'),
(46, 1, 'Edited appointment 3, No. [3APT/20210303]', 1, 0, '2021-03-03 15:47:35', '2021-03-03 15:47:35'),
(47, 1, 'Edited treatment 1', 1, 0, '2021-03-04 01:32:30', '2021-03-04 01:32:30'),
(48, 1, 'Edited labtest 1', 1, 0, '2021-03-04 01:32:39', '2021-03-04 01:32:39'),
(49, 1, 'Edited labtest 1', 1, 0, '2021-03-04 01:33:31', '2021-03-04 01:33:31'),
(50, 1, 'Edited medicines 1', 1, 0, '2021-03-04 01:33:36', '2021-03-04 01:33:36'),
(51, 1, 'Added cost for patient [35, appointment id. - 5]', 1, 0, '2021-03-04 01:51:00', '2021-03-04 01:51:00'),
(52, 1, 'Added appointment for patient [35, appointment no. - 2APT/20210304]', 1, 0, '2021-03-04 01:51:00', '2021-03-04 01:51:00'),
(53, 1, 'Added new fee [fff]', 1, 0, '2021-03-04 05:07:01', '2021-03-04 05:07:01'),
(54, 1, 'Edited fee [fff]', 1, 0, '2021-03-04 05:27:24', '2021-03-04 05:27:24'),
(55, 1, 'Added new fee [vvvv]', 1, 0, '2021-03-04 05:36:48', '2021-03-04 05:36:48'),
(56, 1, 'Edited fee [vvvv]', 1, 0, '2021-03-04 05:37:43', '2021-03-04 05:37:43'),
(57, 1, 'Deleted fee [2]', 1, 0, '2021-03-04 05:37:55', '2021-03-04 05:37:55'),
(58, 1, 'Updated cost for patient [35, appointment id. - 5]', 1, 0, '2021-03-04 05:50:24', '2021-03-04 05:50:24'),
(59, 1, 'Updated cost for patient [35, appointment id. - 5]', 1, 0, '2021-03-04 05:50:39', '2021-03-04 05:50:39'),
(60, 1, 'Added new expense type [uuuuu]', 1, 0, '2021-03-04 08:04:35', '2021-03-04 08:04:35'),
(61, 1, 'Added new expense type [mmmm]', 1, 0, '2021-03-04 08:04:41', '2021-03-04 08:04:41'),
(62, 1, 'Edited expense type [mmmm99]', 1, 0, '2021-03-04 08:04:47', '2021-03-04 08:04:47'),
(63, 1, 'Deleted expense type [2]', 1, 0, '2021-03-04 08:04:51', '2021-03-04 08:04:51'),
(64, 1, 'Added new expense [1]', 1, 0, '2021-03-04 08:12:54', '2021-03-04 08:12:54'),
(65, 1, 'Added new expense [1]', 1, 0, '2021-03-04 08:13:08', '2021-03-04 08:13:08'),
(66, 1, 'Edited expense [1]', 1, 0, '2021-03-04 08:13:16', '2021-03-04 08:13:16'),
(67, 1, 'Deleted expense [2]', 1, 0, '2021-03-04 08:13:40', '2021-03-04 08:13:40'),
(68, 1, 'Added user [ggggg fgddfg - medevkfgdgenya@gmail.com]', 1, 0, '2021-03-04 08:25:06', '2021-03-04 08:25:06'),
(69, 1, 'Edited user [Mary Wanjiru - medevkfgdgenya@gmail.com]', 1, 0, '2021-03-04 08:25:20', '2021-03-04 08:25:20'),
(70, 1, 'Deleted user [36]', 1, 0, '2021-03-04 08:25:30', '2021-03-04 08:25:30'),
(71, 1, 'Added new ward [mmmmm]', 1, 0, '2021-03-04 08:37:37', '2021-03-04 08:37:37'),
(72, 1, 'Added new ward [bbbb]', 1, 0, '2021-03-04 08:37:46', '2021-03-04 08:37:46'),
(73, 1, 'Edited ward [mmmmm]', 1, 0, '2021-03-04 08:37:51', '2021-03-04 08:37:51'),
(74, 1, 'Deleted ward [1]', 1, 0, '2021-03-04 08:37:54', '2021-03-04 08:37:54'),
(75, 1, 'Added new service [gdfgdfg]', 1, 0, '2021-03-04 09:11:33', '2021-03-04 09:11:33'),
(76, 1, 'Edited service [33333]', 1, 0, '2021-03-04 09:11:41', '2021-03-04 09:11:41'),
(77, 1, 'Added new referral hospital [fhfh]', 1, 0, '2021-03-04 10:50:45', '2021-03-04 10:50:45'),
(78, 1, 'Added new referral hospital [hgfhgf]', 1, 0, '2021-03-04 10:50:51', '2021-03-04 10:50:51'),
(79, 1, 'Added new role [Ojijo]', 1, 0, '2021-03-04 10:54:22', '2021-03-04 10:54:22'),
(80, 1, 'Edited role [Ojijo 66]', 1, 0, '2021-03-04 10:54:30', '2021-03-04 10:54:30'),
(81, 1, 'Deleted role [6]', 1, 0, '2021-03-04 10:54:33', '2021-03-04 10:54:33'),
(82, 1, 'Admitted new patient [35], ward [2]', 1, 0, '2021-03-04 16:28:43', '2021-03-04 16:28:43'),
(83, 1, 'Added new ward [fsdfds]', 1, 0, '2021-03-13 13:47:25', '2021-03-13 13:47:25'),
(84, 1, 'Added new ward [dasdsadasd3]', 1, 0, '2021-03-13 13:47:59', '2021-03-13 13:47:59'),
(85, 1, 'Added new category [Panadol]', 1, 0, '2021-04-04 04:35:27', '2021-04-04 04:35:27'),
(86, 1, 'Edited category [Piriton]', 1, 0, '2021-04-04 04:35:36', '2021-04-04 04:35:36'),
(87, 1, 'Deleted category [5]', 1, 0, '2021-04-04 04:35:39', '2021-04-04 04:35:39'),
(88, 1, 'Added new color [Blue]', 1, 0, '2021-04-04 04:37:28', '2021-04-04 04:37:28'),
(89, 1, 'Added new color [Red]', 1, 0, '2021-04-04 04:37:40', '2021-04-04 04:37:40'),
(90, 1, 'Added new size [XL5]', 1, 0, '2021-04-04 04:38:20', '2021-04-04 04:38:20'),
(91, 1, 'Added new size [Small]', 1, 0, '2021-04-04 04:38:25', '2021-04-04 04:38:25'),
(92, 1, 'Added new brand [Wwew]', 1, 0, '2021-04-04 04:43:36', '2021-04-04 04:43:36'),
(93, 1, 'Edited brand [Wwew]', 1, 0, '2021-04-04 04:43:42', '2021-04-04 04:43:42'),
(94, 1, 'Added new product [cool dawa]', 1, 0, '2021-04-04 06:39:40', '2021-04-04 06:39:40'),
(95, 1, 'Edited product [cool dawa 1]', 1, 0, '2021-04-04 08:55:32', '2021-04-04 08:55:32'),
(96, 1, 'Added new batch [5501]', 1, 0, '2021-04-04 09:15:57', '2021-04-04 09:15:57'),
(97, 1, 'Edited batch [5501]', 1, 0, '2021-04-04 09:18:04', '2021-04-04 09:18:04'),
(98, 1, 'Edited batch [5501]', 1, 0, '2021-04-04 09:19:11', '2021-04-04 09:19:11'),
(99, 1, 'Added new category [Pain Killers]', 1, 0, '2021-04-04 11:54:14', '2021-04-04 11:54:14'),
(100, 1, 'Added new category [Food Poisoning]', 1, 0, '2021-04-04 11:54:23', '2021-04-04 11:54:23'),
(101, 1, 'Added new batch [1001]', 1, 0, '2021-04-06 02:22:37', '2021-04-06 02:22:37'),
(102, 1, 'Added new batch [1002]', 1, 0, '2021-04-06 02:23:35', '2021-04-06 02:23:35'),
(103, 1, 'Added new batch [1003]', 1, 0, '2021-04-06 02:23:50', '2021-04-06 02:23:50'),
(104, 1, 'Added new batch [1005]', 1, 0, '2021-04-06 02:24:04', '2021-04-06 02:24:04'),
(105, 1, 'Added new batch [1008]', 1, 0, '2021-04-06 02:24:28', '2021-04-06 02:24:28');

-- --------------------------------------------------------

--
-- Table structure for table `admissions`
--

CREATE TABLE `admissions` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `appointmentId` int(11) NOT NULL,
  `wardId` int(11) NOT NULL,
  `bedNo` varchar(10) NOT NULL,
  `admissionDate` date NOT NULL,
  `dischargeDate` date DEFAULT NULL,
  `admitReason` text,
  `dischargeReason` text,
  `isPaid` enum('Paid','Pending','','') NOT NULL DEFAULT 'Pending',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admissions`
--

INSERT INTO `admissions` (`id`, `adminId`, `created_by`, `patientId`, `appointmentId`, `wardId`, `bedNo`, `admissionDate`, `dischargeDate`, `admitReason`, `dischargeReason`, `isPaid`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 35, 5, 2, '1', '2021-03-04', '2021-03-05', 'fdsfsdfsd', NULL, 'Pending', 0, '2021-03-04 16:28:43', '2021-03-05 06:20:14');

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `doctorId` int(11) NOT NULL,
  `serviceId` int(11) NOT NULL,
  `appointmentDate` date NOT NULL,
  `appointmentNo` varchar(20) NOT NULL,
  `giveMedicine` varchar(3) NOT NULL DEFAULT 'No',
  `status` enum('Pending','Done','Cancelled','') NOT NULL DEFAULT 'Pending',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `adminId`, `created_by`, `patientId`, `doctorId`, `serviceId`, `appointmentDate`, `appointmentNo`, `giveMedicine`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 34, 29, 1, '2021-03-03', '1APT/2021-03-03', 'Yes', 'Pending', 0, '2021-03-03 06:39:28', '2021-03-04 01:32:30'),
(2, 1, 1, 35, 29, 1, '2021-03-11', '1APT/2021-03-03', 'No', 'Pending', 0, '2021-03-03 06:40:44', '2021-03-03 10:17:43'),
(3, 1, 1, 35, 29, 1, '2021-03-05', '3APT/20210303', 'No', 'Pending', 0, '2021-03-03 10:16:51', '2021-03-03 15:47:35'),
(4, 1, 1, 35, 29, 1, '2021-03-04', '1APT/20210304', 'No', 'Pending', 0, '2021-03-04 01:50:34', '2021-03-04 01:50:34'),
(5, 1, 1, 35, 29, 1, '2021-03-04', '2APT/20210304', 'No', 'Pending', 0, '2021-03-04 01:51:00', '2021-03-04 01:51:00');

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE `batches` (
  `id` int(11) NOT NULL,
  `batchNo` varchar(50) NOT NULL,
  `expiryDate` date NOT NULL,
  `quantity` double NOT NULL,
  `sold` double NOT NULL DEFAULT '0',
  `productId` int(11) NOT NULL,
  `status` enum('In Stock','Out of Stock','Expired','Inactive') NOT NULL DEFAULT 'In Stock',
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`id`, `batchNo`, `expiryDate`, `quantity`, `sold`, `productId`, `status`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, '5501', '2021-04-04', 20, 7, 10, 'In Stock', 1, 1, 0, '2021-04-04 09:15:57', '2021-04-06 08:16:40'),
(2, '1001', '2021-04-13', 50, 2, 1, 'In Stock', 1, 1, 0, '2021-04-06 02:22:37', '2021-04-06 08:16:41'),
(3, '1002', '2021-04-29', 30, 0, 9, 'In Stock', 1, 1, 0, '2021-04-06 02:23:35', '2021-04-06 02:23:35'),
(4, '1003', '2021-04-21', 40, 2, 4, 'In Stock', 1, 1, 0, '2021-04-06 02:23:50', '2021-04-06 08:16:40'),
(5, '1005', '2021-04-15', 60, 0, 5, 'In Stock', 1, 1, 0, '2021-04-06 02:24:04', '2021-04-06 02:24:04'),
(6, '1008', '2021-04-21', 22, 2, 7, 'In Stock', 1, 1, 0, '2021-04-06 02:24:28', '2021-04-06 08:16:41');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `brandName` varchar(40) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `categoryId`, `brandName`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Toyota', 0, 0, 0, '2021-03-24 16:06:01', '2021-04-04 06:23:55'),
(2, 1, 'Mercedece', 0, 0, 0, '2021-03-24 16:06:01', '2021-04-04 06:23:58'),
(3, 6, 'Wwew', 1, 1, 0, '2021-04-04 04:43:36', '2021-04-04 04:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `itemName` varchar(100) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(2) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `orderId` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `totalCost` decimal(10,2) NOT NULL,
  `color` varchar(20) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  `completed` tinyint(3) NOT NULL DEFAULT '0',
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `itemName`, `productId`, `quantity`, `sessionId`, `userId`, `orderId`, `cost`, `totalCost`, `color`, `size`, `completed`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, '', 2, 1, 'a3oGFzqFTh3CiUdo8AArdxmiWqNqJjh6ncDiHqZT', NULL, 1, '900.00', '900.00', '0', '0', 0, 0, 0, 0, '2021-03-29 05:40:33', '2021-04-04 13:10:24'),
(2, '', 4, 1, 'a3oGFzqFTh3CiUdo8AArdxmiWqNqJjh6ncDiHqZT', NULL, 2, '900.00', '900.00', '', '', 0, 0, 0, 0, '2021-03-29 05:41:59', '2021-04-04 13:10:28'),
(3, '', 1, 1, 'a3oGFzqFTh3CiUdo8AArdxmiWqNqJjh6ncDiHqZT', NULL, 2, '400.00', '400.00', '', '', 0, 0, 0, 0, '2021-03-29 05:41:59', '2021-04-04 13:10:31'),
(4, '', 2, 1, 'a3oGFzqFTh3CiUdo8AArdxmiWqNqJjh6ncDiHqZT', 0, 3, '900.00', '900.00', '0', '0', 0, 0, 0, 0, '2021-03-29 06:26:58', '2021-04-04 13:10:34'),
(5, 'Hisense TV', 2, 1, 'a3oGFzqFTh3CiUdo8AArdxmiWqNqJjh6ncDiHqZT', 0, 4, '900.00', '900.00', '0', '0', 0, 0, 0, 0, '2021-03-29 06:27:22', '2021-04-04 13:10:21'),
(6, 'EcoTank M3180 Inkjet Printer', 4, 1, '7TVUsTlV36BXw8D9m0TmWVlnJ91LOdXXA5B2LAUq', 0, 5, '900.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-29 12:48:50', '2021-03-29 12:48:50'),
(7, 'HP 250 G7 Notebook', 5, 1, '7TVUsTlV36BXw8D9m0TmWVlnJ91LOdXXA5B2LAUq', 0, 5, '400.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-29 12:48:50', '2021-03-29 12:48:50'),
(8, '55 inch Hisense Smart TV', 6, 1, '7TVUsTlV36BXw8D9m0TmWVlnJ91LOdXXA5B2LAUq', 0, 5, '900.00', '1500.00', '', '', 0, 0, 0, 0, '2021-03-29 12:48:50', '2021-04-04 13:10:39'),
(9, 'HP 250 G7 Notebook', 5, 1, 'ld4mstUYFHyWK5FTFACC5bArZS6e088zKtkydkCp', 0, 6, '400.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-30 01:17:39', '2021-03-30 01:17:39'),
(10, 'EcoTank M3180 Inkjet Printer', 4, 1, 'ld4mstUYFHyWK5FTFACC5bArZS6e088zKtkydkCp', 0, 6, '900.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-30 01:17:39', '2021-03-30 01:17:39'),
(11, 'EcoTank M3180 Inkjet Printer', 8, 1, 'SHjjt9AK6lKTUvT3c4vPRDbwbjG4e5Iud6adjHTq', 0, 7, '900.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-30 04:54:24', '2021-03-30 04:54:24'),
(12, 'Hisense 43 UHD SMART', 7, 1, 'SHjjt9AK6lKTUvT3c4vPRDbwbjG4e5Iud6adjHTq', 0, 7, '400.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-30 04:54:24', '2021-03-30 04:54:24'),
(13, 'Hisense 43 UHD SMART', 2, 1, 'aTSmsoM8Eqa48hJM8UH8furhZxqaaz0TUCb6pdSP', 0, 8, '900.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-30 10:41:39', '2021-03-30 10:41:39'),
(14, '', 3, 1, 'aTSmsoM8Eqa48hJM8UH8furhZxqaaz0TUCb6pdSP', 0, 8, '400.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-30 10:41:39', '2021-03-30 10:41:39'),
(15, 'EcoTank M3180 Inkjet Printer', 4, 1, 'JRSv36uVqs84d4BPqO8lDOuW1UljqJ8D8lw4pkIP', 0, 9, '900.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-31 00:48:08', '2021-03-31 00:48:08'),
(16, '', 3, 1, 'JRSv36uVqs84d4BPqO8lDOuW1UljqJ8D8lw4pkIP', 0, 9, '400.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-31 00:48:08', '2021-03-31 00:48:08'),
(17, 'Hisense 43 UHD SMART', 2, 1, 'OtTEUtzQiVa3pCTojWE4QpQByHQhLtIzV1wK3JZy', 0, 10, '900.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-31 08:02:40', '2021-03-31 08:02:40'),
(18, '', 3, 1, 'OtTEUtzQiVa3pCTojWE4QpQByHQhLtIzV1wK3JZy', 0, 10, '400.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-31 08:02:40', '2021-03-31 08:02:40'),
(19, 'EcoTank M3180 Inkjet Printer', 4, 1, 'OtTEUtzQiVa3pCTojWE4QpQByHQhLtIzV1wK3JZy', 0, 10, '900.00', '0.00', '', '', 0, 0, 0, 0, '2021-03-31 08:02:40', '2021-03-31 08:02:40'),
(20, 'Hisense 43 UHD SMART', 7, 1, 'UukQEDv4BWZl9SRRJqgc1Xd7viP60iiPGkMkkMic', 0, 11, '400.00', '400.00', '', '', 0, 0, 0, 0, '2021-03-31 12:43:02', '2021-03-31 12:43:02'),
(21, '55 inch Hisense Smart TV', 6, 1, 'UukQEDv4BWZl9SRRJqgc1Xd7viP60iiPGkMkkMic', 0, 11, '900.00', '900.00', '', '', 0, 0, 0, 0, '2021-03-31 12:43:02', '2021-03-31 12:43:02'),
(22, 'Hisense 43 UHD SMART', 2, 3, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '300.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 09:39:11', '2021-04-05 12:55:00'),
(23, 'cool dawa 1 cool dawa 1 cool dawa 1 cool dawa 1', 10, 11, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '11000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 10:30:21', '2021-04-05 12:55:00'),
(24, 'EcoTank M3180 Inkjet Printer', 8, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '400.00', '400.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 10:30:24', '2021-04-05 12:55:00'),
(25, 'EcoTank M3180 Inkjet Printer', 4, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '230.00', '230.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 10:30:33', '2021-04-05 12:55:00'),
(26, 'HP 250 G7 Notebook', 5, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '210.00', '210.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 10:30:34', '2021-04-05 12:55:00'),
(27, 'HP 250 G7 Notebook', 9, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '850.00', '850.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 10:30:36', '2021-04-05 12:55:00'),
(28, 'Hisense 43 UHD SMART', 7, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '150.00', '150.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 10:30:40', '2021-04-05 12:55:00'),
(29, 'EcoTank M3180 Inkjet Printer', 4, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '230.00', '230.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:14:51', '2021-04-05 12:55:00'),
(30, 'HP 250 G7 Notebook', 9, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '850.00', '850.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:14:58', '2021-04-05 12:55:00'),
(31, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:19:37', '2021-04-05 12:55:00'),
(32, 'HP 250 G7 Notebook', 5, 2, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '210.00', '420.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:20:03', '2021-04-05 12:55:00'),
(33, 'EcoTank M3180 Inkjet Printer', 4, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '230.00', '230.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:20:37', '2021-04-05 12:55:00'),
(34, '55 inch Hisense Smart TV', 1, 3, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '50.00', '150.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:34:47', '2021-04-05 12:55:00'),
(35, 'HP 250 G7 Notebook', 5, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '210.00', '210.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:35:54', '2021-04-05 12:55:00'),
(36, 'HP 250 G7 Notebook', 5, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '210.00', '210.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:36:14', '2021-04-05 12:55:00'),
(37, 'Hisense 43 UHD SMART', 7, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '150.00', '150.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:37:39', '2021-04-05 12:55:00'),
(38, 'Hisense 43 UHD SMART', 7, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '150.00', '150.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:38:06', '2021-04-05 12:55:00'),
(39, 'Hisense 43 UHD SMART', 7, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '150.00', '150.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:39:28', '2021-04-05 12:55:00'),
(40, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:39:57', '2021-04-05 12:55:00'),
(41, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:40:15', '2021-04-05 12:55:00'),
(42, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:41:00', '2021-04-05 12:55:00'),
(43, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:41:09', '2021-04-05 12:55:00'),
(44, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:41:21', '2021-04-05 12:55:00'),
(45, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:41:31', '2021-04-05 12:55:00'),
(46, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:42:28', '2021-04-05 12:55:00'),
(47, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:43:10', '2021-04-05 12:55:00'),
(48, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:43:50', '2021-04-05 12:55:00'),
(49, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:44:15', '2021-04-05 12:55:00'),
(50, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:44:50', '2021-04-05 12:55:00'),
(51, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:45:45', '2021-04-05 12:55:00'),
(52, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:46:07', '2021-04-05 12:55:00'),
(53, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:54:09', '2021-04-05 12:55:00'),
(54, 'Hisense 43 UHD SMART', 2, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '100.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:54:43', '2021-04-05 12:55:00'),
(55, 'Hisense 43 UHD SMART', 2, 2, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '100.00', '200.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:55:04', '2021-04-05 12:55:00'),
(56, 'EcoTank M3180 Inkjet Printer', 4, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '230.00', '230.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:55:08', '2021-04-05 12:55:00'),
(57, 'HP 250 G7 Notebook', 5, 2, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '210.00', '420.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:55:09', '2021-04-05 12:55:00'),
(58, 'Hisense 43 UHD SMART', 7, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '150.00', '150.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 11:55:09', '2021-04-05 12:55:00'),
(59, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:13:41', '2021-04-05 12:55:00'),
(60, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:24:52', '2021-04-05 12:55:00'),
(61, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:25:31', '2021-04-05 12:55:00'),
(62, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:26:10', '2021-04-05 12:55:00'),
(63, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:28:11', '2021-04-05 12:55:00'),
(64, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:28:28', '2021-04-05 12:55:00'),
(65, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:28:59', '2021-04-05 12:55:00'),
(66, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:31:08', '2021-04-05 12:55:00'),
(67, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:31:50', '2021-04-05 12:55:00'),
(68, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:32:09', '2021-04-05 12:55:00'),
(69, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:35:11', '2021-04-05 12:55:00'),
(70, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:36:20', '2021-04-05 12:55:00'),
(71, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:37:34', '2021-04-05 12:55:00'),
(72, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:38:23', '2021-04-05 12:55:00'),
(73, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:39:15', '2021-04-05 12:55:00'),
(74, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:51:10', '2021-04-05 12:55:00'),
(75, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:51:44', '2021-04-05 12:55:00'),
(76, 'cool dawa 1', 10, 1, 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 1, 26, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-05 12:54:58', '2021-04-05 12:55:00'),
(77, 'cool dawa 1', 10, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 31, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-06 01:29:27', '2021-04-06 02:04:19'),
(78, 'cool dawa 1', 10, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 31, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-06 01:33:23', '2021-04-06 02:04:19'),
(79, 'cool dawa 1', 10, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 31, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-06 01:33:40', '2021-04-06 02:04:19'),
(80, 'cool dawa 1', 10, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 31, '1000.00', '1000.00', NULL, NULL, 1, 0, 0, 0, '2021-04-06 01:35:52', '2021-04-06 02:04:19'),
(81, 'cool dawa 1', 10, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 31, '1000.00', '1000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 01:40:37', '2021-04-06 02:04:19'),
(82, 'cool dawa 1', 10, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 32, '1000.00', '1000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 01:44:05', '2021-04-06 02:04:19'),
(83, 'cool dawa 1', 10, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 33, '1000.00', '1000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 01:45:38', '2021-04-06 02:04:19'),
(84, 'cool dawa 1', 10, 2, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 34, '1000.00', '2000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 01:46:05', '2021-04-06 02:04:19'),
(85, 'cool dawa 1', 10, 2, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 0, '1000.00', '2000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 01:58:49', '2021-04-06 02:04:19'),
(86, 'cool dawa 1', 10, 2, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 0, '1000.00', '2000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 01:59:42', '2021-04-06 02:04:19'),
(87, 'cool dawa 1', 10, 2, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 0, '1000.00', '2000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 02:00:48', '2021-04-06 02:04:19'),
(88, 'cool dawa 1', 10, 3, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 0, '1000.00', '3000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 02:01:14', '2021-04-06 02:04:19'),
(89, 'cool dawa 1', 10, 2, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 0, '1000.00', '2000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 02:01:49', '2021-04-06 02:04:19'),
(90, 'cool dawa 1', 10, 2, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 0, '1000.00', '2000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 02:02:18', '2021-04-06 02:04:19'),
(91, 'cool dawa 1', 10, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 0, '1000.00', '1000.00', NULL, NULL, 0, 1, 1, 0, '2021-04-06 02:15:58', '2021-04-06 02:15:58'),
(92, 'HP 250 G7 Notebook', 5, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 0, '210.00', '210.00', NULL, NULL, 0, 1, 1, 0, '2021-04-06 02:28:14', '2021-04-06 02:28:14'),
(93, '55 inch Hisense Smart TV', 1, 1, 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 1, 0, '50.00', '50.00', NULL, NULL, 0, 1, 1, 0, '2021-04-06 02:28:17', '2021-04-06 02:28:17'),
(94, 'cool dawa 1', 10, 3, 'aD4sHjp92SaAvMTTu81pfsdOEA6HuvXEXmvnZprf', 1, 35, '1000.00', '3000.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 08:01:42', '2021-04-06 08:16:40'),
(95, 'EcoTank M3180 Inkjet Printer', 4, 2, 'aD4sHjp92SaAvMTTu81pfsdOEA6HuvXEXmvnZprf', 1, 35, '230.00', '460.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 08:01:49', '2021-04-06 08:16:40'),
(96, '55 inch Hisense Smart TV', 1, 2, 'aD4sHjp92SaAvMTTu81pfsdOEA6HuvXEXmvnZprf', 1, 35, '50.00', '100.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 08:01:52', '2021-04-06 08:16:40'),
(97, 'Hisense 43 UHD SMART', 7, 2, 'aD4sHjp92SaAvMTTu81pfsdOEA6HuvXEXmvnZprf', 1, 35, '150.00', '300.00', NULL, NULL, 1, 1, 1, 0, '2021-04-06 08:01:53', '2021-04-06 08:16:40'),
(98, 'cool dawa 1', 10, 1, 'JUfm33bUw8o7hPI2WDogbxfQSPzg5jNn6EWvgZPs', 1, 0, '1000.00', '1000.00', NULL, NULL, 0, 1, 1, 0, '2021-04-06 14:29:55', '2021-04-06 14:29:55'),
(99, 'HP 250 G7 Notebook', 5, 1, 'JUfm33bUw8o7hPI2WDogbxfQSPzg5jNn6EWvgZPs', 1, 0, '210.00', '210.00', NULL, NULL, 0, 1, 1, 0, '2021-04-06 14:30:00', '2021-04-06 14:30:00'),
(100, 'Hisense 43 UHD SMART', 7, 1, 'JUfm33bUw8o7hPI2WDogbxfQSPzg5jNn6EWvgZPs', 1, 0, '150.00', '150.00', NULL, NULL, 0, 1, 1, 0, '2021-04-06 14:30:01', '2021-04-06 14:30:01'),
(101, '55 inch Hisense Smart TV', 1, 1, 'JUfm33bUw8o7hPI2WDogbxfQSPzg5jNn6EWvgZPs', 1, 0, '50.00', '50.00', NULL, NULL, 0, 1, 1, 0, '2021-04-06 14:30:02', '2021-04-06 14:30:02');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `categoryName` varchar(50) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `photoUrl` text,
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `adminId`, `categoryName`, `slug`, `photoUrl`, `status`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Home Appliances', 'home-appliances', 'homeappliances.png', 1, 0, 0, '2021-03-23 12:19:13', '2021-04-04 07:32:52'),
(2, 1, 'Electronics', 'electronics', 'electronics.png', 1, 0, 0, '2021-03-23 12:19:13', '2021-04-04 07:32:55'),
(3, 1, 'Women Cothes', 'women-clothes', 'women-clothes.png', 1, 0, 0, '2021-03-23 12:20:04', '2021-04-04 07:32:57'),
(4, 1, 'Furniture', 'furniture', 'electronics.png', 1, 0, 0, '2021-03-23 12:20:04', '2021-04-04 07:32:59'),
(5, 1, 'Cameras', 'cameras', 'homeappliances.png', 1, 0, 1, '2021-03-23 12:20:04', '2021-04-04 04:35:39'),
(6, 1, 'Piriton', 'phones', 'women-clothes.png', 1, 0, 0, '2021-03-23 12:20:04', '2021-04-04 04:35:36'),
(7, 1, 'Panadol', NULL, NULL, 1, 1, 0, '2021-04-04 04:35:27', '2021-04-04 04:35:27'),
(8, 1, 'Pain Killers', NULL, NULL, 1, 1, 0, '2021-04-04 11:54:14', '2021-04-04 11:54:14'),
(9, 1, 'Food Poisoning', NULL, NULL, 1, 1, 0, '2021-04-04 11:54:23', '2021-04-04 11:54:23');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(11) NOT NULL,
  `colorName` varchar(20) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `colorName`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Red', 0, 0, 0, '2021-03-24 16:06:19', '2021-03-24 16:06:19'),
(2, 'Blue', 0, 0, 0, '2021-03-24 16:06:19', '2021-03-24 16:06:19'),
(3, 'Green', 0, 0, 0, '2021-03-24 16:06:26', '2021-03-24 16:06:26'),
(4, 'Black', 0, 0, 0, '2021-03-24 16:06:26', '2021-03-24 16:06:26'),
(5, 'Blue', 1, 1, 0, '2021-04-04 04:37:28', '2021-04-04 04:37:28'),
(6, 'Red', 1, 1, 0, '2021-04-04 04:37:40', '2021-04-04 04:37:40');

-- --------------------------------------------------------

--
-- Table structure for table `costs`
--

CREATE TABLE `costs` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `appointmentId` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `prescriptionsCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `diagnosisCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `labtestCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `medicinesCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `treatmentCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amountPaid` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` enum('Paid','Pending','','') NOT NULL DEFAULT 'Pending',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `costs`
--

INSERT INTO `costs` (`id`, `adminId`, `created_by`, `patientId`, `appointmentId`, `description`, `amount`, `prescriptionsCost`, `diagnosisCost`, `labtestCost`, `medicinesCost`, `treatmentCost`, `amountPaid`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 35, 5, NULL, '60.00', '0.00', '0.00', '0.00', '0.00', '0.00', '20.00', 'Pending', 0, '2021-03-04 01:51:00', '2021-03-04 05:50:39');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `expenseTypeId` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `expenseTypeId`, `amount`, `description`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, '500.00', 'ttrtrtrt', 1, 1, 0, '2021-03-04 08:12:54', '2021-03-04 08:13:16'),
(2, 1, '70.00', 'uyyty ytyty', 1, 1, 1, '2021-03-04 08:13:08', '2021-03-04 08:13:40');

-- --------------------------------------------------------

--
-- Table structure for table `expensetypes`
--

CREATE TABLE `expensetypes` (
  `id` int(11) NOT NULL,
  `expensetypeName` varchar(100) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` smallint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expensetypes`
--

INSERT INTO `expensetypes` (`id`, `expensetypeName`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'uuuuu', 1, 1, 0, '2021-03-04 08:04:35', '2021-03-04 08:04:35'),
(2, 'mmmm99', 1, 1, 1, '2021-03-04 08:04:41', '2021-03-04 08:04:51');

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `appointmentId` int(11) NOT NULL,
  `feeName` varchar(50) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `adminId` int(11) NOT NULL,
  `status` enum('Pending','Paid','','') NOT NULL DEFAULT 'Pending',
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`id`, `patientId`, `appointmentId`, `feeName`, `amount`, `adminId`, `status`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 35, 5, 'fff', '55.00', 1, 'Pending', 1, 0, '2021-03-04 05:07:01', '2021-03-04 05:27:24'),
(2, 35, 5, 'vvvv', '100.00', 1, 'Paid', 1, 1, '2021-03-04 05:36:48', '2021-03-04 05:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `guardians`
--

CREATE TABLE `guardians` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `gender` varchar(8) NOT NULL,
  `location` varchar(100) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guardians`
--

INSERT INTO `guardians` (`id`, `patientId`, `adminId`, `created_by`, `name`, `phone`, `email`, `gender`, `location`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 30, 1, 1, 'maimuna', '0725142514', 'Muna@gmail.com', 'Female', 'Kenya', 0, '2021-03-03 06:32:05', '2021-03-03 06:32:05'),
(2, 31, 1, 1, 'maimuna', '0725142514', 'Muna@gmail.com', 'Female', 'Kenya', 0, '2021-03-03 06:32:44', '2021-03-03 06:32:44'),
(3, 32, 1, 1, 'maimuna', '0725142514', 'Muna@gmail.com', 'Female', 'Kenya', 0, '2021-03-03 06:34:10', '2021-03-03 06:34:10'),
(4, 33, 1, 1, 'maimuna', '0725142514', 'Muna@gmail.com', 'Female', 'Kenya', 0, '2021-03-03 06:39:00', '2021-03-03 06:39:00'),
(5, 34, 1, 1, 'maimuna', '0725142514', 'Muna@gmail.com', 'Female', 'Kenya', 0, '2021-03-03 06:39:28', '2021-03-03 06:39:28'),
(6, 35, 1, 1, 'gdfg', '53534543', 'Mundfgdfa@gmail.com', 'Male', 'dfgfdgdf', 0, '2021-03-03 06:40:44', '2021-03-03 06:40:44');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `senderId` int(11) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `message` varchar(600) NOT NULL,
  `isRead` tinyint(3) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `senderId`, `userId`, `adminId`, `message`, `isRead`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'Test one 2', 0, 0, '2021-03-04 17:50:35', '2021-03-04 18:00:20'),
(2, 1, 2, 1, 'Cool for all', 0, 0, '2021-03-04 17:50:35', '2021-03-04 18:00:24'),
(3, NULL, 1, 1, 'sema je', 0, 0, '2021-03-04 16:53:28', '2021-03-04 16:53:28');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `paymentMethod` varchar(20) DEFAULT NULL,
  `status` enum('pending','packaging','shipping','delivered','cancelled') NOT NULL DEFAULT 'pending',
  `orderNo` varchar(20) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `paid` varchar(10) NOT NULL DEFAULT 'pending',
  `paidOn` datetime DEFAULT NULL,
  `total` double NOT NULL DEFAULT '0',
  `subtotal` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL DEFAULT '0',
  `taxamount` double NOT NULL DEFAULT '0',
  `discount` double NOT NULL DEFAULT '0',
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `userId`, `paymentMethod`, `status`, `orderNo`, `sessionId`, `paid`, `paidOn`, `total`, `subtotal`, `tax`, `taxamount`, `discount`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'pending', 'XM1A76-0', 'a3oGFzqFTh3CiUdo8AArdxmiWqNqJjh6ncDiHqZT', 'pending', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-29 05:40:33', '2021-04-04 12:46:13'),
(2, NULL, NULL, 'pending', '9H17CM-2', 'a3oGFzqFTh3CiUdo8AArdxmiWqNqJjh6ncDiHqZT', 'pending', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-29 05:41:59', '2021-04-04 12:46:16'),
(3, 0, NULL, 'pending', '6W87S1-3', 'a3oGFzqFTh3CiUdo8AArdxmiWqNqJjh6ncDiHqZT', 'paid', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-29 06:26:58', '2021-04-04 12:54:30'),
(4, 0, NULL, 'pending', 'K2C0AI-4', 'a3oGFzqFTh3CiUdo8AArdxmiWqNqJjh6ncDiHqZT', 'pending', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-29 06:27:22', '2021-04-04 12:46:35'),
(5, 0, NULL, 'pending', '2NH3E3-5', '7TVUsTlV36BXw8D9m0TmWVlnJ91LOdXXA5B2LAUq', 'pending', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-29 12:48:50', '2021-04-04 12:46:33'),
(6, 0, NULL, 'pending', 'L9M7H5-1', 'ld4mstUYFHyWK5FTFACC5bArZS6e088zKtkydkCp', 'paid', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-30 01:17:39', '2021-04-04 12:54:35'),
(7, 0, 'cashondelivery', 'packaging', '1RF9G9-7', 'SHjjt9AK6lKTUvT3c4vPRDbwbjG4e5Iud6adjHTq', 'pending', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-30 04:54:24', '2021-04-04 12:46:29'),
(8, 0, 'mpesa', 'pending', 'EYZC91-8', 'aTSmsoM8Eqa48hJM8UH8furhZxqaaz0TUCb6pdSP', 'paid', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-30 10:41:39', '2021-04-04 12:54:39'),
(9, 0, NULL, 'pending', 'IR68WL-1', 'JRSv36uVqs84d4BPqO8lDOuW1UljqJ8D8lw4pkIP', 'pending', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-31 00:48:08', '2021-04-04 12:46:25'),
(10, 0, NULL, 'pending', 'IWDG1L-10', 'OtTEUtzQiVa3pCTojWE4QpQByHQhLtIzV1wK3JZy', 'pending', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-31 08:02:40', '2021-04-04 12:46:22'),
(11, 0, NULL, 'pending', 'R551EV-11', 'UukQEDv4BWZl9SRRJqgc1Xd7viP60iiPGkMkkMic', 'pending', NULL, 0, 0, 0, 0, 0, 1, 0, 0, '2021-03-31 12:43:02', '2021-04-04 12:46:20'),
(12, 1, NULL, 'pending', '6FG6P2-1', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:26:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-05 12:26:12', '2021-04-05 12:26:12'),
(13, 1, NULL, 'pending', '91R673-13', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:28:00', 1100, 1000, 0, 100, 0, 1, 0, 0, '2021-04-05 12:28:13', '2021-04-06 11:49:57'),
(14, 1, NULL, 'pending', '0X6BWU-14', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:28:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-05 12:28:30', '2021-04-05 12:28:30'),
(15, 1, NULL, 'pending', '3P1ZB7-15', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:29:00', 1100, 1000, 0, 100, 0, 1, 0, 0, '2021-04-05 12:29:01', '2021-04-06 11:49:59'),
(16, 1, NULL, 'pending', 'BA6TZ3-16', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:31:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-05 12:31:10', '2021-04-05 12:31:10'),
(17, 1, NULL, 'pending', 'AW61KY-17', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:31:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-05 12:31:51', '2021-04-05 12:31:51'),
(18, 1, NULL, 'pending', '07FB3Z-18', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:32:00', 1100, 1000, 0, 100, 0, 1, 0, 0, '2021-04-05 12:32:10', '2021-04-06 11:50:01'),
(19, 1, NULL, 'pending', '39936D-19', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:35:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-05 12:35:13', '2021-04-05 12:35:13'),
(20, 1, NULL, 'pending', 'RK6A81-20', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:36:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-05 12:36:22', '2021-04-05 12:36:22'),
(21, 1, NULL, 'pending', 'UFZE1T-21', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:37:00', 1100, 1000, 0, 100, 0, 1, 0, 0, '2021-04-05 12:37:36', '2021-04-06 11:50:03'),
(22, 1, NULL, 'pending', 'DCPX1L-22', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:38:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-05 12:38:24', '2021-04-05 12:38:24'),
(23, 1, NULL, 'pending', 'F6D3JI-23', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:39:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-05 12:39:18', '2021-04-05 12:39:18'),
(24, 1, NULL, 'pending', 'WH8D71-24', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:51:00', 1100, 1000, 0, 100, 0, 1, 0, 0, '2021-04-05 12:51:12', '2021-04-06 11:50:44'),
(25, 1, NULL, 'pending', '77COJ6-25', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:51:00', 1100, 1000, 0, 100, 0, 1, 0, 0, '2021-04-05 12:51:46', '2021-04-06 11:50:46'),
(26, 1, NULL, 'pending', 'E1H6IV-26', 'PmJkEmjo5CdtrKFJSRvFaHcVxSyQ1CYaf2DRCr9P', 'paid', '2021-04-05 15:55:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-05 12:55:00', '2021-04-05 12:55:00'),
(27, 1, NULL, 'pending', 'PDC618-1', 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 'paid', '2021-04-06 04:29:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-06 01:29:46', '2021-04-06 01:29:46'),
(28, 1, NULL, 'pending', 'TVIC66-28', 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 'paid', '2021-04-06 04:33:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-06 01:33:25', '2021-04-06 01:33:25'),
(29, 1, NULL, 'pending', '86N7PR-29', 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 'paid', '2021-04-06 04:33:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-06 01:33:42', '2021-04-06 01:33:42'),
(30, 1, NULL, 'pending', 'UC36Y1-30', 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 'paid', '2021-04-06 04:35:00', 1100, 1000, 0, 100, 0, 0, 0, 0, '2021-04-06 01:35:54', '2021-04-06 01:35:54'),
(31, 1, NULL, 'pending', 'HU61BA-31', 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 'paid', '2021-04-06 04:41:00', 1100, 1000, 0, 100, 0, 1, 1, 0, '2021-04-06 01:41:24', '2021-04-06 01:41:24'),
(32, 1, NULL, 'pending', '18TO6X-32', 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 'paid', '2021-04-06 04:44:00', 1100, 1000, 0, 100, 0, 1, 1, 0, '2021-04-06 01:44:07', '2021-04-06 01:44:07'),
(33, 1, NULL, 'pending', 'M1BW74-33', 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 'paid', '2021-04-06 04:45:00', 1100, 1000, 0, 100, 0, 1, 1, 0, '2021-04-06 01:45:41', '2021-04-06 01:45:41'),
(34, 1, NULL, 'pending', '7J3VWM-34', 'TcwAoIpIx64v1LIeV2kjwOvQ5zJmECbGVyuD23Co', 'paid', '2021-04-06 04:46:00', 2200, 2000, 0, 200, 0, 1, 1, 0, '2021-04-06 01:46:13', '2021-04-06 01:46:13'),
(35, 1, NULL, 'pending', 'QJ0R81-35', 'aD4sHjp92SaAvMTTu81pfsdOEA6HuvXEXmvnZprf', 'paid', '2021-04-06 11:16:00', 4246, 3860, 0, 386, 0, 1, 1, 0, '2021-04-06 08:16:40', '2021-04-06 08:16:40');

-- --------------------------------------------------------

--
-- Table structure for table `paymentmethods`
--

CREATE TABLE `paymentmethods` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  `adminId` int(11) DEFAULT NULL,
  `patientId` int(11) DEFAULT NULL,
  `appointmentId` int(11) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `fee` decimal(10,2) DEFAULT NULL,
  `method` varchar(20) DEFAULT NULL,
  `currency` varchar(5) NOT NULL,
  `MSISDN` varchar(20) DEFAULT NULL,
  `paidBy` varchar(50) DEFAULT NULL,
  `details` text,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `userId`, `packageId`, `adminId`, `patientId`, `appointmentId`, `transaction_id`, `reference`, `amount`, `fee`, `method`, `currency`, `MSISDN`, `paidBy`, `details`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 1, 22, 1, NULL, 'NM70AG', '100.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2021-03-04 07:21:14', '2021-03-04 09:40:15'),
(2, NULL, NULL, 1, 22, 1, NULL, '265QOX', '100.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2020-11-21 07:22:30', '2020-11-21 07:22:30'),
(3, NULL, NULL, 1, 22, 2, NULL, '20924P', '200.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2021-03-04 07:21:14', '2021-03-04 09:40:18'),
(4, NULL, NULL, 1, 22, 3, NULL, '229UK5', '500.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2020-11-21 07:32:45', '2020-11-21 07:32:45'),
(5, NULL, NULL, 1, 22, 2, NULL, 'RO99Q1', '200.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2020-11-21 07:36:23', '2020-11-21 07:36:23'),
(6, NULL, NULL, 1, 22, 1, NULL, 'VUFG9B', '100.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2020-11-21 07:38:01', '2020-11-21 07:38:01'),
(7, NULL, NULL, 1, 22, 1, NULL, 'EZ2I02', '100.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2020-11-21 07:39:02', '2020-11-21 07:39:02'),
(8, NULL, NULL, 1, 22, 1, NULL, 'XM5D51', '100.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2020-11-21 07:41:18', '2020-11-21 07:41:18'),
(9, NULL, NULL, 1, 22, 1, NULL, 'I121CJ', '100.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2020-11-21 08:45:53', '2020-11-21 08:45:53'),
(10, NULL, NULL, 1, 22, 2, NULL, 'GXSVOE', '200.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2020-11-21 08:49:36', '2020-11-21 08:49:36'),
(11, NULL, NULL, 1, 22, 2, '1714726', 'M2VE9J', '200.00', '5.80', NULL, 'KES', '', '', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1714726,\"txref\":\"M2VE9J\",\"flwref\":\"8502220835\",\"devicefingerprint\":\"e287a57eb0b652f6570943ddc1b14f03\",\"cycle\":\"one-time\",\"amount\":200,\"currency\":\"KES\",\"chargedamount\":205.8,\"appfee\":5.8,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"105.163.87.93\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":6,\"createddayname\":\"SATURDAY\",\"createdweek\":47,\"createdmonth\":10,\"createdmonthname\":\"NOVEMBER\",\"createdquarter\":4,\"createdyear\":2020,\"createdyearisleap\":true,\"createddayispublicholiday\":0,\"createdhour\":11,\"createdminute\":56,\"createdpmam\":\"am\",\"created\":\"2020-11-21T11:56:29.000Z\",\"customerid\":531404,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Brian Medev\",\"custemail\":\"medevkenya@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2020-11-21T10:28:54.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"8502220835\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":200,\"meta\":[]}}', 1, 0, '2020-11-21 08:54:18', '2020-11-21 08:54:48'),
(12, NULL, NULL, 1, 22, 5, '1714755', 'MTXYLR', '1000.00', '29.00', NULL, 'KES', '', '', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1714755,\"txref\":\"MTXYLR\",\"flwref\":\"2597613808\",\"devicefingerprint\":\"e287a57eb0b652f6570943ddc1b14f03\",\"cycle\":\"one-time\",\"amount\":1000,\"currency\":\"KES\",\"chargedamount\":1029,\"appfee\":29,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"105.163.87.93\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":6,\"createddayname\":\"SATURDAY\",\"createdweek\":47,\"createdmonth\":10,\"createdmonthname\":\"NOVEMBER\",\"createdquarter\":4,\"createdyear\":2020,\"createdyearisleap\":true,\"createddayispublicholiday\":0,\"createdhour\":12,\"createdminute\":17,\"createdpmam\":\"pm\",\"created\":\"2020-11-21T12:17:24.000Z\",\"customerid\":531404,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Brian Medev\",\"custemail\":\"medevkenya@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2020-11-21T10:28:54.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"2597613808\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":1000,\"meta\":[]}}', 1, 0, '2020-11-21 09:15:16', '2020-11-21 09:15:45'),
(13, NULL, NULL, 1, 22, 6, '1714762', '0EMG0F', '2500.00', '72.50', NULL, 'KES', '', '', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1714762,\"txref\":\"0EMG0F\",\"flwref\":\"4329379027\",\"devicefingerprint\":\"e287a57eb0b652f6570943ddc1b14f03\",\"cycle\":\"one-time\",\"amount\":2500,\"currency\":\"KES\",\"chargedamount\":2572.5,\"appfee\":72.5,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"105.163.87.93\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":6,\"createddayname\":\"SATURDAY\",\"createdweek\":47,\"createdmonth\":10,\"createdmonthname\":\"NOVEMBER\",\"createdquarter\":4,\"createdyear\":2020,\"createdyearisleap\":true,\"createddayispublicholiday\":0,\"createdhour\":12,\"createdminute\":20,\"createdpmam\":\"pm\",\"created\":\"2020-11-21T12:20:20.000Z\",\"customerid\":531404,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Brian Medev\",\"custemail\":\"medevkenya@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2020-11-21T10:28:54.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"4329379027\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":2500,\"meta\":[]}}', 1, 0, '2020-11-21 09:18:14', '2020-11-21 09:18:39'),
(14, NULL, NULL, 1, 22, 4, '1714767', '6M25X6', '500.00', '14.50', NULL, 'KES', '', '', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1714767,\"txref\":\"6M25X6\",\"flwref\":\"2386281338\",\"devicefingerprint\":\"e287a57eb0b652f6570943ddc1b14f03\",\"cycle\":\"one-time\",\"amount\":500,\"currency\":\"KES\",\"chargedamount\":514.5,\"appfee\":14.5,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"105.163.87.93\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":6,\"createddayname\":\"SATURDAY\",\"createdweek\":47,\"createdmonth\":10,\"createdmonthname\":\"NOVEMBER\",\"createdquarter\":4,\"createdyear\":2020,\"createdyearisleap\":true,\"createddayispublicholiday\":0,\"createdhour\":12,\"createdminute\":23,\"createdpmam\":\"pm\",\"created\":\"2020-11-21T12:23:13.000Z\",\"customerid\":531404,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Brian Medev\",\"custemail\":\"medevkenya@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2020-11-21T10:28:54.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"2386281338\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":500,\"meta\":[]}}', 1, 0, '2020-11-21 09:21:07', '2020-11-21 09:21:34'),
(15, NULL, NULL, 1, 1, 3, NULL, 'HGQOMA', '2500.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2021-02-12 10:43:32', '2021-02-12 10:43:32'),
(16, NULL, NULL, 1, 1, 2, NULL, '7J26F6', '1000.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2021-02-12 10:47:17', '2021-02-12 10:47:17'),
(17, NULL, NULL, 1, 1, 5, NULL, 'VEKLRD', '10000.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2021-02-12 10:47:22', '2021-02-12 10:47:22'),
(18, NULL, NULL, 1, 1, 4, NULL, 'DK3R4G', '6000.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2021-02-12 10:47:23', '2021-02-12 10:47:23'),
(19, NULL, NULL, 1, 1, 3, NULL, 'OEGA11', '2500.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2021-02-12 10:47:24', '2021-02-12 10:47:24'),
(20, NULL, NULL, 1, 1, 5, '1904540', '756HGT', '10000.00', '290.00', NULL, 'KES', '', '', '{\"status\":\"success\",\"message\":\"Tx Fetched\",\"data\":{\"txid\":1904540,\"txref\":\"756HGT\",\"flwref\":\"9632118379\",\"devicefingerprint\":\"8c7fab8cd27b8d566ea73de128aef9f0\",\"cycle\":\"one-time\",\"amount\":10000,\"currency\":\"KES\",\"chargedamount\":10290,\"appfee\":290,\"merchantfee\":0,\"merchantbearsfee\":0,\"chargecode\":\"00\",\"chargemessage\":\"Successful\",\"authmodel\":\"LIPA_MPESA\",\"ip\":\"197.232.84.223\",\"narration\":\"FLW-PBF MPESA Transaction \",\"status\":\"successful\",\"vbvcode\":\"N/A\",\"vbvmessage\":\"N/A\",\"authurl\":\"N/A\",\"acctcode\":\"00\",\"acctmessage\":\"Successful\",\"paymenttype\":\"mpesa\",\"paymentid\":\"N/A\",\"fraudstatus\":\"ok\",\"chargetype\":\"normal\",\"createdday\":5,\"createddayname\":\"FRIDAY\",\"createdweek\":6,\"createdmonth\":1,\"createdmonthname\":\"FEBRUARY\",\"createdquarter\":1,\"createdyear\":2021,\"createdyearisleap\":false,\"createddayispublicholiday\":0,\"createdhour\":13,\"createdminute\":48,\"createdpmam\":\"pm\",\"created\":\"2021-02-12T13:48:44.000Z\",\"customerid\":590581,\"custphone\":\"254702066508\",\"custnetworkprovider\":\"UNKNOWN PROVIDER\",\"custname\":\"Global12 Services\",\"custemail\":\"globaltempingservices@gmail.com\",\"custemailprovider\":\"GMAIL\",\"custcreated\":\"2021-02-12T13:48:43.000Z\",\"accountid\":220897,\"acctbusinessname\":\"Brian Mulunda\",\"acctcontactperson\":\"Brian Mulunda\",\"acctcountry\":\"KE\",\"acctbearsfeeattransactiontime\":0,\"acctparent\":2410,\"acctvpcmerchant\":\"N/A\",\"acctalias\":null,\"acctisliveapproved\":0,\"orderref\":\"9632118379\",\"paymentplan\":null,\"paymentpage\":null,\"raveref\":null,\"amountsettledforthistransaction\":10000,\"meta\":[]}}', 1, 0, '2021-02-12 10:47:54', '2021-02-12 10:48:32'),
(21, NULL, NULL, 1, 1, 2, NULL, 'QRTW8J', '1000.00', NULL, NULL, 'KES', '', '', NULL, 0, 0, '2021-02-12 11:02:37', '2021-02-12 11:02:37'),
(22, 1, 1, NULL, NULL, NULL, NULL, '0ES1F1', '15.00', NULL, NULL, 'KES', NULL, NULL, NULL, 0, 0, '2021-03-05 10:24:32', '2021-03-05 10:24:32'),
(23, 1, 1, NULL, NULL, NULL, NULL, 'C51TEM', '50.00', NULL, NULL, 'KES', NULL, NULL, NULL, 0, 0, '2021-03-05 10:27:35', '2021-03-05 10:27:35');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `permissionName` varchar(30) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permissionName`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'SMS Single Recipient', 0, '2021-02-10 13:32:56', '2021-02-10 13:32:56'),
(2, 'SMS Group', 0, '2021-02-10 13:32:56', '2021-02-10 13:32:56'),
(3, 'SMS Excel Contacts', 0, '2021-02-10 13:33:40', '2021-02-12 12:21:02'),
(4, 'Create Group', 0, '2021-02-10 13:33:40', '2021-02-12 12:21:05'),
(5, 'Edit Group', 0, '2021-02-10 13:33:54', '2021-02-10 13:33:54'),
(6, 'Delete Group', 0, '2021-02-10 13:33:54', '2021-02-10 13:33:54'),
(7, 'Create Contact', 0, '2021-02-10 13:34:11', '2021-02-10 13:34:11'),
(8, 'Edit Contact', 0, '2021-02-10 13:34:11', '2021-02-10 13:34:11'),
(9, 'Delete Contact', 0, '2021-02-10 13:35:31', '2021-02-10 13:35:31'),
(10, 'Outbox', 0, '2021-02-10 13:35:31', '2021-02-10 13:35:31'),
(11, 'Subscriptions', 0, '2021-02-10 13:36:01', '2021-02-10 13:36:01'),
(12, 'Settings', 0, '2021-02-10 13:36:01', '2021-02-10 13:36:01'),
(13, 'Users', 0, '2021-02-10 13:36:17', '2021-02-10 13:36:17'),
(14, 'Roles', 0, '2021-02-10 13:36:17', '2021-02-10 13:36:17'),
(15, 'Permissions', 0, '2021-02-10 13:36:24', '2021-02-10 13:36:24'),
(16, 'View Group', 0, '2021-02-10 13:36:24', '2021-02-10 13:36:24'),
(17, 'View Contact', 0, '2021-02-10 13:36:24', '2021-02-10 13:36:24');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `productName` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  `manufacturer` varchar(100) DEFAULT NULL,
  `categoryId` int(11) NOT NULL,
  `sizeId` int(11) DEFAULT NULL,
  `colorId` int(11) DEFAULT NULL,
  `brandId` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `photoUrl` varchar(50) NOT NULL DEFAULT 'medicine.png',
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `productName`, `cost`, `manufacturer`, `categoryId`, `sizeId`, `colorId`, `brandId`, `description`, `photoUrl`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, '55 inch Hisense Smart TV', 50, NULL, 1, 1, 1, 1, '55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV', 'medicine.png', 1, 0, 0, '2021-03-24 15:38:46', '2021-04-05 12:37:28'),
(2, 'Hisense 43 UHD SMART', 100, NULL, 1, 1, 1, 1, 'Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART', 'medicine.png', 1, 0, 0, '2021-03-24 15:38:46', '2021-04-05 12:37:31'),
(4, 'EcoTank M3180 Inkjet Printer', 230, NULL, 1, 1, 1, 1, 'EcoTank M3180 Inkjet Printer EcoTank M3180 Inkjet Printer EcoTank M3180 Inkjet Printer EcoTank M3180 Inkjet Printer EcoTank M3180 Inkjet Printer', 'medicine.png', 1, 0, 0, '2021-03-24 15:38:46', '2021-04-05 12:37:33'),
(5, 'HP 250 G7 Notebook', 210, NULL, 1, 1, 1, 1, 'HP 250 G7 Notebook HP 250 G7 Notebook HP 250 G7 Notebook HP 250 G7 NotebookHP 250 G7 Notebook HP 250 G7 Notebook HP 250 G7 NotebookHP 250 G7 NotebookHP 250 G7 Notebook HP 250 G7 Notebook', 'medicine.png', 1, 0, 0, '2021-03-24 15:38:46', '2021-04-05 12:37:37'),
(6, '55 inch Hisense Smart TV', 80, NULL, 1, 1, 1, 1, '55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV 55 inch Hisense Smart TV', 'medicine.png', 1, 0, 0, '2021-03-24 15:38:46', '2021-04-05 12:37:39'),
(7, 'Hisense 43 UHD SMART', 150, NULL, 1, 1, 1, 1, 'Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART Hisense 43 UHD SMART', 'medicine.png', 1, 0, 0, '2021-03-24 15:38:46', '2021-04-05 12:37:43'),
(8, 'EcoTank M3180 Inkjet Printer', 400, NULL, 1, 1, 1, 1, 'EcoTank M3180 Inkjet Printer EcoTank M3180 Inkjet Printer EcoTank M3180 Inkjet Printer EcoTank M3180 Inkjet Printer EcoTank M3180 Inkjet Printer', 'medicine.png', 1, 0, 0, '2021-03-24 15:38:46', '2021-04-05 12:37:45'),
(9, 'HP 250 G7 Notebook', 850, NULL, 1, 1, 1, 1, 'HP 250 G7 Notebook HP 250 G7 Notebook HP 250 G7 Notebook HP 250 G7 NotebookHP 250 G7 Notebook HP 250 G7 Notebook HP 250 G7 NotebookHP 250 G7 NotebookHP 250 G7 Notebook HP 250 G7 Notebook', 'medicine.png', 1, 0, 0, '2021-03-24 15:38:46', '2021-04-05 12:37:50'),
(10, 'cool dawa 1', 1000, NULL, 6, 4, 6, 3, 'iko sawa', 'logo-2-1024-by-500_1617529179.png', 1, 1, 0, '2021-04-04 06:39:40', '2021-04-05 12:37:52');

-- --------------------------------------------------------

--
-- Table structure for table `referralhospitals`
--

CREATE TABLE `referralhospitals` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `name` text NOT NULL,
  `location` text NOT NULL,
  `contacts` varchar(100) NOT NULL,
  `specialities` text NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referralhospitals`
--

INSERT INTO `referralhospitals` (`id`, `adminId`, `created_by`, `name`, `location`, `contacts`, `specialities`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'fhfh', 'hhfgh', 'fhfgh', 'ghgfhgfh', 0, '2021-03-04 10:50:45', '2021-03-04 10:50:45'),
(2, 1, 1, 'hgfhgf', 'hgfhg', 'hgfhg', 'hfghgfh', 0, '2021-03-04 10:50:51', '2021-03-04 10:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `appointmentId` int(11) NOT NULL,
  `referralHospitalId` int(11) NOT NULL,
  `reason` text NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referrals`
--

INSERT INTO `referrals` (`id`, `adminId`, `created_by`, `patientId`, `appointmentId`, `referralHospitalId`, `reason`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 'Cool one', 0, '2021-03-05 07:02:38', '2021-03-05 07:02:38');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `roleName` varchar(30) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roleName`, `description`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', NULL, 1, 1, 0, '2021-03-02 19:55:14', '2021-03-02 19:55:14'),
(2, 'Doctor', NULL, 1, 1, 0, '2021-03-02 19:55:14', '2021-03-02 19:55:14'),
(3, 'Pharmacist', NULL, 1, 1, 0, '2021-03-02 19:55:52', '2021-03-02 19:55:52'),
(4, 'Receptionist', NULL, 1, 1, 0, '2021-03-02 19:55:52', '2021-03-02 19:55:52'),
(5, 'Cashier', NULL, 1, 1, 0, '2021-03-02 19:56:10', '2021-03-02 19:56:10'),
(6, 'Ojijo 66', NULL, 1, 1, 1, '2021-03-04 10:54:22', '2021-03-04 10:54:33');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `appointmentFee` decimal(10,2) NOT NULL DEFAULT '0.00',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `adminId`, `created_by`, `name`, `description`, `appointmentFee`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'ENT', 'Cool stuff', '60.00', 0, '2021-03-03 09:29:06', '2021-03-04 04:50:12'),
(2, 1, 1, '33333', '77777', '45.00', 0, '2021-03-04 09:11:33', '2021-03-04 09:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `companyPhoto` varchar(50) NOT NULL DEFAULT 'company.jpg',
  `companyName` varchar(50) NOT NULL DEFAULT 'Our Awesome Health Centre',
  `companyTelephone` varchar(100) DEFAULT NULL,
  `companyEmail` varchar(100) DEFAULT NULL,
  `companyLocation` varchar(100) DEFAULT NULL,
  `balanceLimit` int(11) NOT NULL DEFAULT '10',
  `tax` double NOT NULL DEFAULT '0',
  `discount` double NOT NULL DEFAULT '0',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `adminId`, `companyPhoto`, `companyName`, `companyTelephone`, `companyEmail`, `companyLocation`, `balanceLimit`, `tax`, `discount`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, '73920.png', 'Springs Health Services', '7567567567', 'm@gfdgfd.gh', 'hfghfgh', 10, 10, 100, 0, '2021-02-08 12:23:05', '2021-04-06 14:45:31');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `sizeName` varchar(20) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `sizeName`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'XL', 0, 0, 0, '2021-03-24 16:05:41', '2021-03-24 16:05:41'),
(2, 'L', 0, 0, 0, '2021-03-24 16:05:41', '2021-03-24 16:05:41'),
(3, 'XL5', 1, 1, 0, '2021-04-04 04:38:19', '2021-04-04 04:38:19'),
(4, 'Small', 1, 1, 0, '2021-04-04 04:38:25', '2021-04-04 04:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `subscriptionDate` date NOT NULL,
  `expiryDate` date NOT NULL,
  `status` enum('pending','active','expired','') NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `adminId`, `packageId`, `subscriptionDate`, `expiryDate`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-02-09', '2021-02-28', 'active', 0, '2021-02-09 13:07:15', '2021-02-09 13:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `treatments`
--

CREATE TABLE `treatments` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `prescriptions` text,
  `bp` text,
  `cholestrol` text,
  `bloodSugar` text,
  `diagnosis` text,
  `labtest` text,
  `temperature` double DEFAULT NULL,
  `giveMedicine` varchar(3) NOT NULL DEFAULT '0',
  `medicines` text,
  `appointmentId` int(11) NOT NULL,
  `prescriptionsCost` decimal(10,2) DEFAULT '0.00',
  `diagnosisCost` decimal(10,2) DEFAULT '0.00',
  `labtestCost` decimal(10,2) DEFAULT '0.00',
  `medicinesCost` decimal(10,2) DEFAULT '0.00',
  `treatmentCost` decimal(10,2) DEFAULT '0.00',
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `treatments`
--

INSERT INTO `treatments` (`id`, `adminId`, `patientId`, `prescriptions`, `bp`, `cholestrol`, `bloodSugar`, `diagnosis`, `labtest`, `temperature`, `giveMedicine`, `medicines`, `appointmentId`, `prescriptionsCost`, `diagnosisCost`, `labtestCost`, `medicinesCost`, `treatmentCost`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 34, NULL, '3', '4', 'r', NULL, 'all ava 656565', 2, 'Yes', 'gdgdgd', 1, '0.00', '0.00', '0.00', '0.00', '0.00', 1, 0, '2021-03-03 13:25:41', '2021-03-04 01:33:36'),
(2, 1, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, 2, '0.00', '0.00', '0.00', '0.00', '0.00', 1, 0, '2021-03-03 13:25:41', '2021-03-04 04:31:56'),
(3, 1, 35, 'sdfsd 453534', 'eee', 'ffff', 'dddd', 'dsfsdfdsf', 'sdvvcxvxc', 20, 'No', NULL, 3, '0.00', '0.00', '0.00', '0.00', '0.00', 1, 0, '2021-03-03 13:25:41', '2021-03-04 04:32:00'),
(4, 1, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, 4, '0.00', '0.00', '0.00', '0.00', '0.00', 1, 0, '2021-03-04 01:50:34', '2021-03-04 01:50:34'),
(5, 1, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, 5, '0.00', '0.00', '0.00', '0.00', '0.00', 1, 0, '2021-03-04 01:51:00', '2021-03-04 01:51:00');

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions`
--

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userpermissions`
--

INSERT INTO `userpermissions` (`id`, `roleId`, `permissionId`, `adminId`, `created_by`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(2, 1, 2, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(3, 1, 3, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(4, 1, 4, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-14 09:27:17'),
(5, 1, 5, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(6, 1, 6, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-14 09:27:20'),
(7, 1, 7, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(8, 1, 8, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(9, 1, 9, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(10, 1, 10, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(11, 1, 11, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(12, 1, 12, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(13, 1, 13, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(14, 1, 14, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(15, 1, 15, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(16, 2, 1, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:19:24'),
(17, 2, 2, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(18, 2, 3, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:18:28'),
(19, 2, 4, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:20:52'),
(20, 2, 5, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(21, 2, 6, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:21:01'),
(22, 2, 7, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(23, 2, 8, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:20:54'),
(24, 2, 9, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(25, 2, 10, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(26, 2, 11, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(27, 2, 12, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(28, 2, 13, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:20:25'),
(29, 2, 14, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(30, 2, 15, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(31, 3, 1, 1, 1, 0, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(32, 3, 2, 1, 1, 0, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(33, 3, 3, 1, 1, 0, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(34, 7, 1, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(35, 7, 2, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(36, 7, 5, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(37, 7, 6, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(38, 7, 7, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(39, 12, 0, 1, 1, 1, 0, '2021-02-12 08:22:31', '2021-02-12 08:22:31'),
(40, 13, 1, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(41, 13, 2, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(42, 13, 5, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(43, 13, 6, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(44, 13, 7, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(45, 13, 8, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(46, 13, 9, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(47, 13, 10, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(48, 13, 11, 1, 1, 1, 0, '2021-02-12 08:33:56', '2021-02-12 08:33:56'),
(49, 13, 12, 1, 1, 1, 0, '2021-02-12 08:33:56', '2021-02-12 08:33:56'),
(50, 13, 13, 1, 1, 1, 0, '2021-02-12 08:33:56', '2021-02-12 08:33:56'),
(51, 14, 1, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(52, 14, 2, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(53, 14, 5, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(54, 14, 6, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(55, 14, 7, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(56, 14, 8, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(57, 14, 9, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(58, 14, 10, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(59, 14, 11, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(60, 14, 12, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(61, 14, 13, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(62, 14, 14, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(63, 17, 1, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(64, 17, 2, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(65, 17, 5, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(66, 17, 6, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(67, 17, 7, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(68, 17, 8, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(69, 17, 9, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(70, 17, 10, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(71, 17, 11, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(72, 17, 12, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(73, 17, 13, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(74, 17, 14, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(75, 17, 15, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(76, 17, 16, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(77, 17, 17, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(78, 19, 1, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(79, 19, 2, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(80, 19, 5, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(81, 19, 6, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(82, 19, 7, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(83, 19, 8, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(84, 19, 9, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(85, 19, 10, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(86, 19, 11, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(87, 19, 12, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(88, 19, 13, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(89, 19, 14, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(90, 19, 15, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(91, 19, 16, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(92, 19, 17, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(93, 19, 18, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(94, 19, 19, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(95, 21, 1, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(96, 21, 2, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(97, 21, 5, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(98, 21, 6, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(99, 21, 7, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(100, 21, 8, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(101, 21, 9, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(102, 21, 10, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(103, 21, 11, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(104, 21, 12, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(105, 21, 13, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(106, 21, 14, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(107, 21, 15, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(108, 21, 16, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(109, 21, 17, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(110, 21, 18, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(111, 21, 19, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(112, 21, 20, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(113, 21, 21, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(114, 22, 1, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(115, 22, 2, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(116, 22, 5, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(117, 22, 6, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(118, 22, 7, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(119, 22, 8, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(120, 22, 9, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(121, 22, 10, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(122, 22, 11, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(123, 22, 12, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(124, 22, 13, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(125, 22, 14, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(126, 22, 15, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(127, 22, 16, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(128, 22, 17, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(129, 22, 18, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(130, 22, 19, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(131, 22, 20, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(132, 22, 21, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(133, 22, 22, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(134, 23, 1, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(135, 23, 2, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(136, 23, 5, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(137, 23, 6, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(138, 23, 7, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(139, 23, 8, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(140, 23, 9, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(141, 23, 10, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(142, 23, 11, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(143, 23, 12, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(144, 23, 13, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(145, 23, 14, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(146, 23, 15, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(147, 23, 16, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(148, 23, 17, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(149, 23, 18, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(150, 23, 19, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(151, 23, 20, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(152, 23, 21, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(153, 23, 22, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(154, 23, 23, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(155, 1, 16, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(156, 1, 17, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(157, 1, 18, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(158, 6, 1, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(159, 6, 2, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(160, 6, 3, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(161, 6, 4, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(162, 6, 5, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(163, 6, 6, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(164, 6, 7, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(165, 6, 8, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(166, 6, 9, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(167, 6, 10, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(168, 6, 11, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(169, 6, 12, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(170, 6, 13, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(171, 6, 14, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(172, 6, 15, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(173, 6, 16, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(174, 6, 17, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(175, 5, 1, 1, 1, 1, 0, '2021-03-04 11:04:32', '2021-03-04 11:04:32'),
(176, 5, 5, 1, 1, 1, 0, '2021-03-04 11:04:52', '2021-03-04 11:04:52'),
(177, 5, 7, 1, 1, 1, 0, '2021-03-04 11:04:55', '2021-03-04 11:04:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `adminId` int(11) DEFAULT NULL,
  `firstName` varchar(20) NOT NULL,
  `middleName` varchar(20) DEFAULT NULL,
  `lastName` varchar(20) NOT NULL,
  `mobileNo` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `resetCode` varchar(100) DEFAULT NULL,
  `isVerified` tinyint(3) NOT NULL DEFAULT '0',
  `isVetted` tinyint(3) NOT NULL DEFAULT '0',
  `isDisabled` tinyint(3) NOT NULL DEFAULT '0',
  `isActive` tinyint(3) NOT NULL DEFAULT '0',
  `wardId` int(11) DEFAULT NULL,
  `userTypeId` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `roleId` tinyint(3) NOT NULL DEFAULT '1',
  `location` varchar(200) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `patientNo` varchar(11) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `profilePic` varchar(30) NOT NULL DEFAULT 'profile.png',
  `countryId` int(11) NOT NULL DEFAULT '1',
  `townId` int(11) NOT NULL DEFAULT '1',
  `about` varchar(255) DEFAULT NULL,
  `isOnline` tinyint(3) NOT NULL DEFAULT '0',
  `companyId` int(11) DEFAULT NULL,
  `confirmpassword` varchar(100) DEFAULT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `adminId`, `firstName`, `middleName`, `lastName`, `mobileNo`, `email`, `password`, `resetCode`, `isVerified`, `isVetted`, `isDisabled`, `isActive`, `wardId`, `userTypeId`, `created_by`, `roleId`, `location`, `dob`, `gender`, `patientNo`, `latitude`, `longitude`, `profilePic`, `countryId`, `townId`, `about`, `isOnline`, `companyId`, `confirmpassword`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Brian', NULL, 'Mulunda', '254724619842', 'globaltempingservices@gmail.com', '$2y$10$HxtagHZ0g0dbQPZ4yaVs9euua7vekftIEiG2rFpQNy76LzPB92gw.', '5a0917b6e81dc7dd29be2cf266c860f1', 1, 0, 0, 1, 0, 'Employer', NULL, 1, 'Nairobi, Kenya', '2021-03-03', NULL, NULL, -1.2920659, 36.8219462, '93599.jpg', 1, 1, 'Advertisement of all job categories\r\nA jobs advertisement company', 1, NULL, NULL, 0, '2020-08-09 05:47:37', '2021-04-06 14:09:55'),
(28, NULL, 'Merve', NULL, 'Manager', '254702150260', 'admin@mervecabs.com', '$2y$10$e3yIsKNGMzjJIKxbYaW8Mu8gwAMMW8repsLkMaYSklzeJxX22oYLW', 'ceccbe5dd13f5dab9cda265b5eafb2a0', 0, 0, 0, 0, NULL, NULL, NULL, 2, NULL, '2021-03-03', NULL, NULL, NULL, NULL, 'profile.jpg', 1, 1, NULL, 0, NULL, NULL, 0, '2021-02-05 05:31:41', '2021-03-03 09:55:28'),
(29, 1, 'Juma', NULL, 'josphine@gmail.com', NULL, 'Anderson', '123456', NULL, 0, 0, 0, 0, NULL, NULL, 1, 2, NULL, '2021-03-03', NULL, NULL, NULL, NULL, 'profile.jpg', 1, 1, NULL, 0, NULL, NULL, 0, '2021-02-10 10:08:26', '2021-03-03 09:55:26'),
(30, 1, 'Patient5', 'Zero6', '1', '0702150260', 'medevkenfrya11@gmail.com', 'Patient', NULL, 0, 0, 0, 0, NULL, NULL, 1, 0, 'Nairobi Kenya', '2021-03-03', 'Male', NULL, NULL, NULL, 'profile.jpg', 1, 1, NULL, 0, NULL, NULL, 0, '2021-03-03 06:32:05', '2021-03-03 09:32:41'),
(31, 1, 'PatientY', 'Zero', '1', '0702150260', 'medevkenfrya11@gmail.com', 'Patient', NULL, 0, 0, 0, 0, NULL, NULL, 1, 0, 'Nairobi Kenya', '2021-03-03', 'Male', NULL, NULL, NULL, 'profile.jpg', 1, 1, NULL, 0, NULL, NULL, 0, '2021-03-03 06:32:44', '2021-03-03 09:34:03'),
(32, 1, 'Patientd', 'Zerov', '1', '0702150260', 'medevkenfrya11@gmail.com', 'Patient', NULL, 0, 0, 0, 0, NULL, NULL, 1, 0, 'Nairobi Kenya', '2021-03-03', 'Male', NULL, NULL, NULL, 'profile.jpg', 1, 1, NULL, 0, NULL, NULL, 0, '2021-03-03 06:34:10', '2021-03-03 09:37:10'),
(33, 1, 'Patienty', 'Zerok', '1', '0702150260', 'medevkenfrya11@gmail.com', 'Patient', NULL, 0, 0, 0, 0, NULL, NULL, 1, 0, 'Nairobi Kenya', '2021-03-03', 'Male', NULL, NULL, NULL, 'profile.jpg', 1, 1, NULL, 0, NULL, NULL, 0, '2021-03-03 06:39:00', '2021-03-03 09:39:14'),
(34, 1, 'Patient', 'Zero', '1', '0702150260', 'medevkenfrya11@gmail.com', 'Patient', NULL, 0, 0, 0, 0, NULL, NULL, 1, 0, 'Nairobi Kenya', '2021-03-03', 'Male', NULL, NULL, NULL, 'profile.jpg', 1, 1, NULL, 0, NULL, NULL, 0, '2021-03-03 06:39:28', '2021-03-03 06:39:28'),
(35, 1, 'Emmanuel', 'Makelele', 'Wanjiku', '1PT20210301', 'booksforlifeke@gmail.com', 'kevo', NULL, 0, 0, 0, 0, NULL, NULL, 1, 0, 'dfgfdgdb', '2021-01-03', 'Male', NULL, NULL, NULL, 'profile.jpg', 1, 1, NULL, 0, NULL, NULL, 0, '2021-03-03 06:40:44', '2021-03-05 07:40:11'),
(36, 1, 'Mary', NULL, 'Wanjiru', NULL, 'medevkfgdgenya@gmail.com', '123456', NULL, 0, 0, 0, 0, NULL, NULL, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'profile.jpg', 1, 1, NULL, 0, NULL, NULL, 1, '2021-03-04 08:25:06', '2021-03-04 08:25:30');

-- --------------------------------------------------------

--
-- Table structure for table `wards`
--

CREATE TABLE `wards` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `wardName` varchar(50) NOT NULL,
  `spacesavailable` int(5) NOT NULL DEFAULT '1',
  `spacesoccupied` int(5) NOT NULL DEFAULT '0',
  `status` enum('Available','Not Available','','') NOT NULL DEFAULT 'Available',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wards`
--

INSERT INTO `wards` (`id`, `adminId`, `created_by`, `wardName`, `spacesavailable`, `spacesoccupied`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'mmmmm', 3, 2, 'Available', 1, '2021-03-04 08:37:37', '2021-03-04 08:37:54'),
(2, 1, 1, 'bbbb', 4, 2, 'Available', 0, '2021-03-04 08:37:46', '2021-03-04 08:37:46'),
(3, 1, 1, 'fsdfds', 5, 3, 'Available', 0, '2021-03-13 13:47:25', '2021-03-13 13:47:25'),
(4, 1, 1, 'dasdsadasd3', 5, 3, 'Available', 0, '2021-03-13 13:47:59', '2021-03-13 13:47:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admissions`
--
ALTER TABLE `admissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batches`
--
ALTER TABLE `batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `costs`
--
ALTER TABLE `costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expensetypes`
--
ALTER TABLE `expensetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guardians`
--
ALTER TABLE `guardians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paymentmethods`
--
ALTER TABLE `paymentmethods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referralhospitals`
--
ALTER TABLE `referralhospitals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `treatments`
--
ALTER TABLE `treatments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userpermissions`
--
ALTER TABLE `userpermissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wards`
--
ALTER TABLE `wards`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `admissions`
--
ALTER TABLE `admissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `batches`
--
ALTER TABLE `batches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `costs`
--
ALTER TABLE `costs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `expensetypes`
--
ALTER TABLE `expensetypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `guardians`
--
ALTER TABLE `guardians`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `paymentmethods`
--
ALTER TABLE `paymentmethods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `referralhospitals`
--
ALTER TABLE `referralhospitals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `treatments`
--
ALTER TABLE `treatments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `userpermissions`
--
ALTER TABLE `userpermissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `wards`
--
ALTER TABLE `wards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
